package fr.itris.glips.svgeditor.shape.customshapes;

import fr.itris.glips.library.geom.Polygon2D;
import fr.itris.glips.svgeditor.ColorManager;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.shape.CustomShape;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by Radek Frydrysek on 9.3.2015.
 */
public class ArrowShape extends CustomShape {
    /**
     * the constructor of the class
     *
     * @param editor the editor
     */
    public ArrowShape(Editor editor) {
        super(editor, "ArrowShape");
    }

    @Override
    public Element createElement(SVGHandle handle, Rectangle2D bounds) {
        //the edited document
        Document doc = handle.getScrollPane().getSVGCanvas().getDocument();

        //normalizing the bounds of the element
        if (bounds.getWidth() < 1) {

            bounds.setRect(bounds.getX(), bounds.getY(), 1, bounds.getHeight());
        }

        if (bounds.getHeight() < 1) {

            bounds.setRect(bounds.getX(), bounds.getY(), bounds.getWidth(), 1);
        }


        final Element element = doc.createElementNS
                (doc.getDocumentElement().getNamespaceURI(), handledElementTagName);

        Shape shape = createShape(bounds);

        element.setAttributeNS(null, dAtt, shapeToPath(handle, element, shape));

        //getting the last color that has been used by the user
        String colorString = Editor.getColorChooser().getColorString(ColorManager.getCurrentColor());
        element.setAttributeNS(null, "style", "fill:" + colorString + ";stroke:#000000;");

        //inserting the element in the document and handling the undo/redo support
        insertShapeElement(handle, element);

        return element;
    }

    @Override
    public Shape createShape(Rectangle2D rect) {
        Polygon2D arrow = new Polygon2D();

        //A
        arrow.addPoint(new Point2D.Double(
                rect.getX(),
                rect.getCenterY()
        ));

        //B
        arrow.addPoint(new Point2D.Double(
                rect.getCenterX(),
                rect.getY()
        ));

        //C
        arrow.addPoint(new Point2D.Double(
                rect.getCenterX(),
                rect.getY() + rect.getHeight() / 3
        ));

        //D
        arrow.addPoint(new Point2D.Double(
                rect.getX() + rect.getWidth(),
                rect.getY() + rect.getHeight() / 3
        ));

        //E
        arrow.addPoint(new Point2D.Double(
                rect.getX() + rect.getWidth(),
                rect.getY() + (rect.getHeight() * (2.0 / 3.0))
        ));

        //F
        arrow.addPoint(new Point2D.Double(
                rect.getCenterX(),
                rect.getY() + (rect.getHeight() * (2.0 / 3.0))
        ));

        //G
        arrow.addPoint(new Point2D.Double(
                rect.getCenterX(),
                rect.getY() + rect.getHeight()
        ));

        return arrow;
    }
}
