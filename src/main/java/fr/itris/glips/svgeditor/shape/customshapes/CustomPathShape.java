package fr.itris.glips.svgeditor.shape.customshapes;

import fr.itris.glips.library.geom.path.Path;
import fr.itris.glips.svgeditor.ColorManager;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.shape.CustomShape;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by Radek Frydrysek on 02.01.2016.
 */
public class CustomPathShape extends CustomShape {
    private Path loadedShape;

    /**
     * the constructor of the class
     *
     * @param editor        the editor
     * @param shapeModuleId
     */
    public CustomPathShape(Editor editor, Path loadedShape) {
        super(editor, "CustomPathShape");
        this.loadedShape = loadedShape;
    }

    @Override
    public Element createElement(SVGHandle handle, Rectangle2D bounds) {
        Document doc = handle.getScrollPane().getSVGCanvas().getDocument();

        //normalizing the bounds of the element
        if (bounds.getWidth() < 1) {

            bounds.setRect(bounds.getX(), bounds.getY(), 1, bounds.getHeight());
        }

        if (bounds.getHeight() < 1) {

            bounds.setRect(bounds.getX(), bounds.getY(), bounds.getWidth(), 1);
        }


        final Element element = doc.createElementNS
                (doc.getDocumentElement().getNamespaceURI(), handledElementTagName);

        //Star2D shape = new Star2D(bounds.getCenterX(), bounds.getCenterY(), bounds.getWidth() / 2 / 3, bounds.getWidth() / 2, 5);
        Shape shape = createShape(bounds);

        element.setAttributeNS(null, dAtt, shapeToPath(handle, element, shape));

        //getting the last color that has been used by the user
        String colorString = Editor.getColorChooser().getColorString(ColorManager.getCurrentColor());
        element.setAttributeNS(null, "style", "fill:" + colorString + ";stroke:#000000;");

        //inserting the element in the document and handling the undo/redo support
        insertShapeElement(handle, element);

        return element;
    }

    @Override
    public Shape createShape(Rectangle2D bounds) {
        Point currentPoint = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(currentPoint, Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas());

        Point2D diff = new Point2D.Double(currentPoint.getX() - loadedShape.getBounds().getX(),
                currentPoint.getY() - loadedShape.getBounds().getY());

        AffineTransform af = new AffineTransform();
        af.translate(diff.getX(), diff.getY());

        return af.createTransformedShape(loadedShape);
    }
}
