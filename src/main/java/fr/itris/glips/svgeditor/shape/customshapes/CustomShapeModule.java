package fr.itris.glips.svgeditor.shape.customshapes;

import fr.itris.glips.library.PreferencesStore;
import fr.itris.glips.library.geom.path.Path;
import fr.itris.glips.svgeditor.ColorManager;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.ModuleAdapter;
import fr.itris.glips.svgeditor.actions.toolbar.ToolsFrame;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DialogHeader;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.PreviewElement;
import fr.itris.glips.svgeditor.display.handle.HandlesListener;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.display.selection.Selection;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMPathElement;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Created by Radek Frydrysek on 31.12.2015.
 */
public class CustomShapeModule extends ModuleAdapter {

    public static final int INFORMATION_TYPE = 0;
    public static final int WARNING_TYPE = 1;
    public static final int ERROR_TYPE = 2;

    private static String headerTitle, description, frameTitle, invalidNameWarning, shapeNameLbl, noPathSelectedWarning;

    static {
        headerTitle = ResourcesManager.bundle.getString("CustomShapesHeaderTitle");
        description = ResourcesManager.bundle.getString("CustomShapesHeaderDescription");
        frameTitle = ResourcesManager.bundle.getString("CustomShapesFrameTitle");
        invalidNameWarning = ResourcesManager.bundle.getString("CustomShapesNameWarning");
        shapeNameLbl = ResourcesManager.bundle.getString("CustomShapesNameLbl");
        noPathSelectedWarning = ResourcesManager.bundle.getString("CustomShapesNoPathSelectedWarning");
    }

    private DefaultListModel shapesListModel;
    private Map<String, String> currentCustomShapes = new HashMap<>();

    private static String preferencesNodeID = "CustomShapes";
    private static String moduleId = "CustomShapeModule";

    private JList<String> listOfShapes;
    private JScrollPane listOfShapesWrapper;
    private JTextField shapeNameEdit;
    private JLabel shapeNameLabel;
    private JButton addShape;
    private JButton removeShape;
    //private PreviewPanel previewPanel;
    private PreviewElement previewElement;

    private ToolsFrame moduleFrame;
    private JPanel controlsPanel = new JPanel();
    private DialogHeader dialogHeader;


    public CustomShapeModule(final Editor editor) {
        frameTitle = ResourcesManager.bundle.getString("label_" + moduleId.toLowerCase());

        initGuiItems();
        composeGui();

        moduleFrame = new ToolsFrame(editor, moduleId, frameTitle, controlsPanel);
        moduleFrame.getMenuItem().setEnabled(false);
        moduleFrame.getToolBarButton().setEnabled(false);

        final HandlesListener svgHandleListener = new HandlesListener() {
            @Override
            public void handleChanged(SVGHandle currentHandle, Set<SVGHandle> handles) {
                if (handles.size() != 0) {
                    moduleFrame.getMenuItem().setEnabled(true);
                    moduleFrame.getToolBarButton().setEnabled(true);
                } else {
                    moduleFrame.getMenuItem().setEnabled(false);
                    moduleFrame.getToolBarButton().setEnabled(false);
                }
            }
        };
        editor.getHandlesManager().addHandlesListener(svgHandleListener);

        moduleFrame.setVisibilityChangedRunnable(() -> {
            if (moduleFrame.isVisible()) {
                fillShapesList();
            }
        });
    }

    private void composeGui() {
        //JPanel headersPanel = createHeader();
        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        GridBagConstraints cons = new GridBagConstraints();
        //AttributeSettings
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        cons.weighty = 0;
        cons.weightx = 0;
        controlsPanel.add(shapeNameLabel, cons);

        cons.gridx = 1;
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.weightx = 1;
        controlsPanel.add(shapeNameEdit, cons);

        cons.fill = GridBagConstraints.NONE;
        cons.gridx = 2;
        cons.weightx = 0;
        controlsPanel.add(addShape, cons);

        cons.gridx = 3;
        controlsPanel.add(removeShape, cons);

        JPanel listAndPreview = new JPanel(new GridLayout(0, 2));
        listOfShapesWrapper = new JScrollPane(listOfShapes);
        listOfShapesWrapper.setPreferredSize(new Dimension(105, 220));
        listAndPreview.add(listOfShapesWrapper);

        //previewPanelWrapper.setPreferredSize(new Dimension(105, 220));

        //listAndPreview.add(new JScrollPane(previewPanel));

        listAndPreview.add(new JScrollPane(previewElement));
        cons.gridy = 1;
        cons.gridx = 0;
        cons.gridwidth = 4;
        cons.fill = GridBagConstraints.BOTH;
        controlsPanel.add(listAndPreview, cons);

        this.controlsPanel.setLayout(new BorderLayout());
        this.controlsPanel.add(dialogHeader, BorderLayout.NORTH);
        this.controlsPanel.add(controlsPanel, BorderLayout.SOUTH);
    }

    private void initGuiItems() {
        dialogHeader = new DialogHeader(headerTitle, description);
        shapesListModel = new DefaultListModel();

        shapeNameLabel = new JLabel(shapeNameLbl);
        shapeNameEdit = new JTextField();
        shapeNameEdit.setPreferredSize(new Dimension(100, 23));

        //previewPanel = new PreviewPanel();
        previewElement = new PreviewElement(false);
        listOfShapes = new JList<>();
        listOfShapes.setModel(shapesListModel);
        addShape = new JButton("+");
        addShape.setPreferredSize(new Dimension(23, 23));
        addShape.addActionListener(e -> addShapeAction());
        removeShape = new JButton("-");
        removeShape.setPreferredSize(new Dimension(23, 23));
        removeShape.addActionListener(e -> {
            String selectedShape = listOfShapes.getSelectedValue();
            if (selectedShape != null && !selectedShape.isEmpty()) {
                PreferencesStore.removePreference(preferencesNodeID, selectedShape);
                fillShapesList();
            }
        });

        listOfShapes.addListSelectionListener(e -> drawShape());


    }

    private void drawShape() {
        String selectedShape = listOfShapes.getSelectedValue();
        try {
            String selectedPath = currentCustomShapes.get(selectedShape);
            Path path = new Path(selectedPath);
            //previewPanel.drawShape(path);
            //previewPanel.updateUI();
            //previewPanel.repaint();
            previewElement.updateCanvas(path);
            CustomPathShape test = new CustomPathShape(Editor.getEditor(), path);
            test.setModuleMode(0);
            test.notifyDrawingMode();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addShapeAction() {
        SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
        if (currentHandle != null) {

            Selection currentSelection = currentHandle.getSelection();

            if (currentSelection != null && currentSelection.getSelectedElements().size() == 1) {
                Element selectedELement = currentSelection.getSelectedElements().iterator().next();
                if (selectedELement instanceof SVGOMPathElement) {
                    String shapeName = shapeNameEdit.getText();
                    if (shapeName != null && !shapeName.isEmpty() && !currentCustomShapes.containsKey(shapeName)) {

                        SVGOMPathElement path = (SVGOMPathElement) selectedELement;
                        String pathString = path.getAttribute("d");

                        //pokud cesta neni uzavrena
                        Path pathShape = new Path(pathString);
                        if (!pathString.contains("Z")) {
                            pathShape.closePath();
                            selectedELement.setAttribute("d", pathShape.toString());
                        }
                        PreferencesStore.setPreference(preferencesNodeID, shapeName, pathShape.toString());
                        String colorString = Editor.getColorChooser().getColorString(ColorManager.getCurrentColor());
                        selectedELement.setAttributeNS(null, "style", "fill:" + colorString + ";stroke:#000000;");
                        fillShapesList();
                        setMessage(description, INFORMATION_TYPE);
                    } else {
                        setMessage(invalidNameWarning, WARNING_TYPE);
                    }
                } else {
                    setMessage(noPathSelectedWarning, WARNING_TYPE);
                }
            } else {
                setMessage(noPathSelectedWarning, WARNING_TYPE);
            }
        }

    }

    private void fillShapesList() {
        shapesListModel.clear();
        currentCustomShapes.clear();

        Preferences settings = PreferencesStore.getPreferenceNode(preferencesNodeID);
        String[] keys = null;
        try {
            keys = settings.keys();
        } catch (BackingStoreException e1) {
            e1.printStackTrace();
        }
        if (keys != null && keys.length > 0) {
            for (int i = 0; i < keys.length; i++) {
                String customShape = settings.get(keys[i], "");
                shapesListModel.addElement(keys[i]);
                currentCustomShapes.put(keys[i], customShape);
            }
        }
    }

    @Override
    public HashMap<String, JMenuItem> getMenuItems() {
        HashMap<String, JMenuItem> menuItems = new HashMap<>();
        menuItems.put("Menu_" + moduleId, moduleFrame.getMenuItem());
        return menuItems;
    }

    @Override
    public HashMap<String, AbstractButton> getToolItems() {
        HashMap<String, AbstractButton> map = new HashMap<>();
        map.put("Tool_" + moduleId, moduleFrame.getToolBarButton());
        return map;
    }

    public void setMessage(String message, int type) {
        switch (type) {

            case INFORMATION_TYPE:

                dialogHeader.setLabels(headerTitle, "", message);
                break;

            case WARNING_TYPE:
                dialogHeader.showWarning("", message);
                break;

            case ERROR_TYPE:
                dialogHeader.showError("", message);
                break;
        }
    }
}
