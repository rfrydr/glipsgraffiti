package fr.itris.glips.svgeditor.shape.customshapes;

import fr.itris.glips.svgeditor.ColorManager;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.EditorToolkit;
import fr.itris.glips.svgeditor.display.canvas.SVGCanvas;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.shape.CustomShape;
import org.jdesktop.swingx.geom.Star2D;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 * Created by Radek Frydrysek on 31.10.2014.
 */
public class StarShape extends CustomShape {

    /**
     * the constructor of the class
     *
     * @param editor the editor
     */
    public StarShape(Editor editor) {
        super(editor,"StarShape");
    }


    @Override
    public Element createElement(SVGHandle handle, Rectangle2D bounds){

        //the edited document
        Document doc=handle.getScrollPane().getSVGCanvas().getDocument();

        //normalizing the bounds of the element
        if(bounds.getWidth()<1){

            bounds.setRect(bounds.getX(), bounds.getY(), 1, bounds.getHeight());
        }

        if(bounds.getHeight()<1){

            bounds.setRect(bounds.getX(), bounds.getY(), bounds.getWidth(), 1);
        }


        final Element element=doc.createElementNS
                (doc.getDocumentElement().getNamespaceURI(), handledElementTagName);

        //Star2D shape = new Star2D(bounds.getCenterX(), bounds.getCenterY(), bounds.getWidth() / 2 / 3, bounds.getWidth() / 2, 5);
        Star2D shape= (Star2D) createShape(bounds);

        element.setAttributeNS(null, dAtt, shapeToPath(handle, element, shape));

        //getting the last color that has been used by the user
        String colorString=Editor.getColorChooser().getColorString(ColorManager.getCurrentColor());
        element.setAttributeNS(null, "style", "fill:"+colorString+";stroke:#000000;");

        //inserting the element in the document and handling the undo/redo support
        insertShapeElement(handle, element);

        return element;
    }

    @Override
    public Shape createShape(Rectangle2D rect) {
        return new Star2D(rect.getX(), rect.getY(), rect.getWidth() / 2 / 3, rect.getWidth() / 2, 5);
    }


}
