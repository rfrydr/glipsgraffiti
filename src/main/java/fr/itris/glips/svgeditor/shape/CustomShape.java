package fr.itris.glips.svgeditor.shape;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.EditorToolkit;
import fr.itris.glips.svgeditor.display.canvas.CanvasPainter;
import fr.itris.glips.svgeditor.display.canvas.SVGCanvas;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.shape.path.PathShape;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 9.3.2015.
 */
public abstract class CustomShape extends PathShape {
    /**
     * the element attributes names
     */
    protected static String xAtt = "x", yAtt = "y";
    /**
     * the first point of the drawing action
     */
    protected Point2D firstDrawingPoint;

    /**
     * the painter drawing ghost rectangles on the canvas
     */
    protected GhostShapeCanvasPainter ghostCanvasPainter = new GhostShapeCanvasPainter();

    /**
     * the constructor of the class
     *
     * @param editor the editor
     */
    public CustomShape(Editor editor, String shapeModuleId) {
        super(editor, shapeModuleId);
        retrieveLabels();
        createMenuAndToolItems();
    }

    public abstract Element createElement(SVGHandle handle, Rectangle2D bounds);

    @Override
    public void notifyDrawingAction(
            SVGHandle handle, Point2D point, int modifier, int type) {

        //getting the canvas
        SVGCanvas canvas = handle.getScrollPane().getSVGCanvas();

        // according to type of the event for the drawing action
        switch (type) {

            case DRAWING_MOUSE_PRESSED:

                if (firstDrawingPoint == null) {

                    firstDrawingPoint = point;
                }

                break;

            case DRAWING_MOUSE_RELEASED:

                //computing the star (polygon) corresponding the first and current clicked points
                Rectangle2D rect = null;

                if (Editor.getEditor().getSquareModeManager().isSquareMode()) {

                    rect = EditorToolkit.getComputedSquare(firstDrawingPoint, point);

                } else {

                    rect = EditorToolkit.getComputedRectangle(firstDrawingPoint, point);
                }

                //computing the base scaled rectangle
                Rectangle2D rect2D = handle.getTransformsManager().
                        getScaledRectangle(rect, true);

                //creating the svg shape
                createElement(handle, rect2D);

                //reinitializing the data
                firstDrawingPoint = null;
                canvas.removePaintListener(ghostCanvasPainter, true);
                ghostCanvasPainter.reinitialize();
                resetDrawing();
                break;

            case DRAWING_MOUSE_DRAGGED:

                //removing the ghost canvas painter
                if (ghostCanvasPainter.getClip() != null) {

                    canvas.removePaintListener(ghostCanvasPainter, false);
                }

                //computing the rectangle corresponding the first and current clicked points
                if (Editor.getEditor().getSquareModeManager().isSquareMode()) {

                    rect = EditorToolkit.getComputedSquare(firstDrawingPoint, point);

                } else {

                    rect = EditorToolkit.getComputedRectangle(firstDrawingPoint, point);
                }

                if (rect.getWidth() > 0.0) {

                    Shape shape = createShape(rect);

                    //setting the new shape to the ghost painter
                    ghostCanvasPainter.setShape(shape);
                    canvas.addLayerPaintListener(SVGCanvas.DRAW_LAYER, ghostCanvasPainter, true);
                }
                break;
        }
    }

    public abstract Shape createShape(Rectangle2D rect);

    protected String shapeToPath(SVGHandle handle, Element element, Shape shape) {
        AffineTransform transform = handle.getSvgElementsManager().getTransform(element);
        PathIterator pathIterator = shape.getPathIterator(transform);
        String points = "M";
        java.util.List<Point2D> pointsList = new ArrayList<>();

        while (!pathIterator.isDone()) {
            final double[] coords = {-1, -1};

            pathIterator.currentSegment(coords);
            if (coords[0] > -1.0 && coords[0] > -1.0) {
                pointsList.add(new Point2D.Double(coords[0], coords[1]));
            }
            pathIterator.next();
        }
        for (int i = 0; i < pointsList.size(); i++) {
            if (i == 0) {
                points = points + String.valueOf(pointsList.get(i).getX()) + " " + String.valueOf(pointsList.get(i).getY()) + " ";
            } else {
                points = points + "L" + String.valueOf(pointsList.get(i).getX()) + " " + String.valueOf(pointsList.get(i).getY()) + " ";
            }
        }
        points = points + "Z";
        return points;
    }

    /**
     * the painter used to draw ghost shapes on a canvas
     *
     * @author ITRIS, Jordi SUC
     */
    protected class GhostShapeCanvasPainter extends CanvasPainter {

        /**
         * the shape to draw
         */
        private Shape shape;

        /**
         * the set of the clip rectangles
         */
        private Set<Rectangle2D> clips = new HashSet<>();

        @Override
        public void paintToBeDone(Graphics2D g) {
            if (shape != null) {

                g = (Graphics2D) g.create();
                g.setColor(Color.black);
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);

                g.draw(shape);
                g.dispose();
            }
        }

        @Override
        public Set<Rectangle2D> getClip() {
            return clips;
        }

        /**
         * sets the shape to paint
         *
         * @param shape a shape
         */
        public void setShape(
                Shape shape) {
            this.shape = shape;
            clips.clear();

            if (shape != null) {
                clips.add(shape.getBounds2D());
            }
        }

        /**
         * reinitializing the painter
         */
        public void reinitialize() {
            shape = null;
        }
    }
}
