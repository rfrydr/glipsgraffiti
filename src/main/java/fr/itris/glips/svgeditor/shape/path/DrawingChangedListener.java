package fr.itris.glips.svgeditor.shape.path;

import fr.itris.glips.svgeditor.shape.AbstractShape;
import org.w3c.dom.Element;

/**
 * Created by Radek Frydrysek on 4.4.2015.
 */
public abstract class DrawingChangedListener {
    public abstract void drawingEnded(AbstractShape shape, Element element);
}
