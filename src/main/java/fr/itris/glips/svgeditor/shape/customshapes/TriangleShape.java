package fr.itris.glips.svgeditor.shape.customshapes;

import fr.itris.glips.library.geom.Polygon2D;
import fr.itris.glips.svgeditor.ColorManager;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.shape.CustomShape;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by Radek Frydrysek on 9.3.2015.
 */
public class TriangleShape extends CustomShape {
    /**
     * the constructor of the class
     *
     * @param editor        the editor
     */
    public TriangleShape(Editor editor) {
        super(editor, "TriangleShape");
    }

    @Override
    public Element createElement(SVGHandle handle, Rectangle2D bounds) {

        //the edited document
        Document doc=handle.getScrollPane().getSVGCanvas().getDocument();

        //normalizing the bounds of the element
        if(bounds.getWidth()<1){

            bounds.setRect(bounds.getX(), bounds.getY(), 1, bounds.getHeight());
        }

        if(bounds.getHeight()<1){

            bounds.setRect(bounds.getX(), bounds.getY(), bounds.getWidth(), 1);
        }


        final Element element=doc.createElementNS
                (doc.getDocumentElement().getNamespaceURI(), handledElementTagName);

        Shape shape = createShape(bounds);

        element.setAttributeNS(null, dAtt, shapeToPath(handle, element, shape));

        //getting the last color that has been used by the user
        String colorString=Editor.getColorChooser().getColorString(ColorManager.getCurrentColor());
        element.setAttributeNS(null, "style", "fill:"+colorString+";stroke:#000000;");

        //inserting the element in the document and handling the undo/redo support
        insertShapeElement(handle, element);

        return element;
    }

    @Override
    public Shape createShape(Rectangle2D rect) {
        Polygon2D triangle = new Polygon2D();
        triangle.addPoint(new Point2D.Double(rect.getX(),rect.getY()));
        triangle.addPoint(new Point2D.Double(rect.getX()+rect.getWidth()/2,rect.getY()+rect.getHeight()));
        triangle.addPoint(new Point2D.Double(rect.getX()+rect.getWidth(),rect.getY()));

        return triangle;
    }
}
