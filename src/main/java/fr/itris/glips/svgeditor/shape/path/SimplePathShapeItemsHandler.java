package fr.itris.glips.svgeditor.shape.path;

import fr.itris.glips.library.display.ArrowIcon;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.HandlesListener;
import fr.itris.glips.svgeditor.display.handle.HandlesManager;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 20.12.2015.
 */
public class SimplePathShapeItemsHandler implements IPathShapeHandler {
    /**
     * the path shape module
     */
    private PathShape pathShapeModule;

    /**
     * the button used for selecting the type of the action
     */
    protected JToggleButton toolButton = new JToggleButton();

    /**
     * the icons
     */
    protected ImageIcon shapeCreatorIcon, shapeCreatorDisabledIcon;

    /**
     * the labels
     */
    protected String itemLabel, itemTooltip;

    /**
     * the menu items that will be added to the menu bar
     */
    protected JMenuItem shapeCreatorMenuItem;

    /**
     * the current mode
     */
    protected int currentMode = PathShape.CREATION_MODE;

    /**
     * the constructor of the class
     *
     * @param pathShapeModule the path shape module
     */
    public SimplePathShapeItemsHandler(PathShape pathShapeModule) {

        this.pathShapeModule = pathShapeModule;
        createItems();
    }

    /**
     * creates all the items for the menubar and toolbar
     */
    protected void createItems() {

        //getting the id of the path module
        String shapeModuleId = pathShapeModule.getId();

        //getting the icons
        shapeCreatorIcon = ResourcesManager.getIcon(shapeModuleId, false);
        shapeCreatorDisabledIcon = ResourcesManager.getIcon(shapeModuleId, true);

        //getting the labels
        itemLabel = ResourcesManager.bundle.getString(shapeModuleId + "ItemLabel");
        itemTooltip = ResourcesManager.bundle.getString(shapeModuleId + "ItemToolTip");

        //creating the menu items for the menu bar
        shapeCreatorMenuItem = new JMenuItem(itemLabel, shapeCreatorIcon);
        shapeCreatorMenuItem.setDisabledIcon(shapeCreatorDisabledIcon);
        shapeCreatorMenuItem.setEnabled(false);

        //creating the menu items for the tool bar
        final JMenuItem shapeCreatorMenuItemTool = new JMenuItem(itemLabel, shapeCreatorIcon);
        shapeCreatorMenuItemTool.setDisabledIcon(shapeCreatorDisabledIcon);
        shapeCreatorMenuItemTool.setToolTipText(itemTooltip);

        //creating the listener to the menu and tool items
        ActionListener listener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                if (e.getSource().equals(shapeCreatorMenuItem)) {

                    pathShapeModule.setModuleMode(PathShape.CREATION_MODE);
                    pathShapeModule.notifyDrawingMode();

                } else if (e.getSource().equals(shapeCreatorMenuItemTool)) {

                    pathShapeModule.setModuleMode(PathShape.CREATION_MODE);
                    setCurrentTool(PathShape.CREATION_MODE);
                    pathShapeModule.notifyDrawingMode();

                }

                toolButton.setSelected(true);
            }
        };

        //adding the listener to the tool and menu items
        shapeCreatorMenuItem.addActionListener(listener);
        shapeCreatorMenuItemTool.addActionListener(listener);

        //creating the tool item that will be displayed in the tool bar
        toolButton = new JToggleButton();
        toolButton.setEnabled(false);

        //computing the area where the arrow can be found
        final Rectangle area = new Rectangle(ArrowIcon.arrowIconBounds);
        area.x += toolButton.getMargin().left;
        area.width += toolButton.getMargin().right;

        //adding a listener to the tool button
        toolButton.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent evt) {

                //setting the global module mode to the current mode of the tool
                pathShapeModule.setModuleMode(currentMode);

                if (currentMode == PathShape.CREATION_MODE) {

                    pathShapeModule.notifyDrawingMode();

                } else {

                    pathShapeModule.notifyItemsActionMode();
                }
            }
        });


        setCurrentTool(currentMode);

        //adding the listener to the switches between the svg handles
        final HandlesManager svgHandleManager =
                Editor.getEditor().getHandlesManager();

        svgHandleManager.addHandlesListener(new HandlesListener() {

            @Override
            public void handleChanged(SVGHandle currentHandle, Set<SVGHandle> handles) {

                boolean existsHandle = svgHandleManager.getHandlesNumber() > 0;
                shapeCreatorMenuItem.setEnabled(existsHandle);
                toolButton.setEnabled(existsHandle);
            }
        });
    }

    /**
     * sets the current tool item in the toolbar according
     * to the provided mode
     *
     * @param mode
     */
    protected void setCurrentTool(int mode) {
        this.currentMode = mode;

        //getting the new icons and the tooltip for the tool
        ImageIcon icon = shapeCreatorIcon;
        String tooltip = itemTooltip;

        //setting the new properties for the tool
        toolButton.setToolTipText(tooltip);
        toolButton.setIcon(icon);
        toolButton.setDisabledIcon(shapeCreatorDisabledIcon);
    }

    /**
     * @return the menu items for the path shape module
     */
    public HashMap<String, JMenuItem> getMenuItems() {

        HashMap<String, JMenuItem> map = new HashMap<>();
        map.put(pathShapeModule.getId(), shapeCreatorMenuItem);

        return map;
    }

    /**
     * @return the tool item containing the menu items
     * for the path shape module
     */
    public HashMap<String, AbstractButton> getTools() {

        HashMap<String, AbstractButton> map = new HashMap<>();
        map.put(pathShapeModule.getId(), toolButton);

        return map;
    }
}
