package fr.itris.glips.svgeditor.shape.path;

import fr.itris.glips.library.display.ArrowIcon;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;

import javax.swing.*;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 19.12.2015.
 */
public class SimplePathShape extends PathShape {

    private Set<DrawingChangedListener> drawingChangedListeners = new HashSet<>();

    public SimplePathShape(Editor editor) {
        super(editor, "SimplePathShape");
    }

    public SimplePathShape(Editor editor, boolean createElement) {
        super(editor, createElement);
    }

    @Override
    public void notifyDrawingAction(
            SVGHandle handle, Point2D point, int modifier, int type) {
        //according to the type of the event for the drawing action
        switch (type) {

            case DRAWING_MOUSE_PRESSED:
                drawingHandler.mousePressed(handle, point);
                break;

            case DRAWING_MOUSE_RELEASED:

                drawingHandler.mouseReleased(handle, point);
                break;

            case DRAWING_MOUSE_MOVED:

                drawingHandler.mouseMoved(handle, point);
                break;

            case DRAWING_MOUSE_DOUBLE_CLICK:

                drawingHandler.mouseDoubleClicked(handle, point);
                resetDrawing();
                for (DrawingChangedListener listener : drawingChangedListeners) {
                    listener.drawingEnded(this, handle.getSelection().getSelectedElements().iterator().next());
                }
                break;

            case DRAWING_END:

                drawingHandler.reset(handle);
                for (DrawingChangedListener listener : drawingChangedListeners) {
                    listener.drawingEnded(this, handle.getSelection().getSelectedElements().iterator().next());
                }
                break;
        }
    }

    /**
     * adds a new drawing changed listener
     *
     * @param listener a drawing changed listener
     */
    public void addDrawingChangedListener(DrawingChangedListener listener) {

        if (drawingChangedListeners != null) {

            drawingChangedListeners.add(listener);
        }
    }

    /**
     * removes a drawing changed listener
     *
     * @param listener a drawing changed listener
     */
    public void removeDrawingChangedListener(DrawingChangedListener listener) {

        if (drawingChangedListeners != null) {

            drawingChangedListeners.remove(listener);
        }
    }
}
