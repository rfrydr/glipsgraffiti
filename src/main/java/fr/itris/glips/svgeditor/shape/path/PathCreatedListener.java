package fr.itris.glips.svgeditor.shape.path;

import org.apache.batik.ext.awt.geom.ExtendedGeneralPath;

/**
 * Created by Radek on 06.01.2016.
 */
public abstract class PathCreatedListener {
    public abstract void pathCreated(ExtendedGeneralPath pathShape, String pathString);
}
