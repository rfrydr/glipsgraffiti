package fr.itris.glips.svgeditor.shape.path;

import javax.swing.*;
import java.util.HashMap;

/**
 * Created by Radek Frydrysek on 20.12.2015.
 */
public interface IPathShapeHandler {
    public HashMap<String, JMenuItem> getMenuItems();

    public HashMap<String, AbstractButton> getTools();
}
