package fr.itris.glips.svgeditor.display.canvas.animationsHelperLines;

import fr.itris.glips.library.geom.path.Path;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.AnimationController;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.display.canvas.CanvasPainter;
import fr.itris.glips.svgeditor.display.canvas.SVGCanvas;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.jdesktop.swingx.geom.Star2D;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimateMotionElement;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Radek on 21.12.2015.
 */
public class AnimationsHelper {
    /**
     * the canvas
     */
    private SVGCanvas canvas;

    /**
     * the constructor of the class
     *
     * @param canvas the canvas
     */
    public AnimationsHelper(SVGCanvas canvas) {

        this.canvas = canvas;
        initializeHelpers();
    }

    /**
     * initializes the grid
     */
    protected void initializeHelpers() {
        CanvasPainter paintListener = new CanvasPainter() {

            @Override
            public void paintToBeDone(Graphics2D g) {
                AnimationsHelperParametersManager handler = Editor.getEditor().
                        getHandlesManager().getAnimationsHelperParametersManager();
                boolean isHelpersEnabled = handler.isHelpersEnabled();

                if (isHelpersEnabled) {
                    Color gridColor = handler.getGridColor();

                    BasicStroke gridStroke = handler.getHelpersStroke();

                    Rectangle canvasBounds = canvas.getScrollPane().getCanvasBounds();
                    Rectangle viewportRectangle = canvas.getScrollPane().getViewPortBounds();

                    Rectangle2D scaledCanvasBounds = canvas.getSVGHandle().
                            getTransformsManager().getScaledRectangle(
                            new Rectangle2D.Double(0, 0, canvasBounds.width, canvasBounds.height), true);

                    Rectangle2D innerRectangle = canvas.getSVGHandle().
                            getTransformsManager().getScaledRectangle(
                            new Rectangle2D.Double(-canvasBounds.x, -canvasBounds.y,
                                    viewportRectangle.width, viewportRectangle.height), true);

                    Rectangle2D.Double resultRect = new Rectangle2D.Double();

                    if (canvasBounds.x >= 0) {

                        resultRect.x = scaledCanvasBounds.getX();
                        resultRect.width = scaledCanvasBounds.getWidth();

                    } else {

                        resultRect.x = innerRectangle.getX();
                        resultRect.width = innerRectangle.getWidth();
                    }

                    if (canvasBounds.y >= 0) {

                        resultRect.y = scaledCanvasBounds.getY();
                        resultRect.height = scaledCanvasBounds.getHeight();

                    } else {

                        resultRect.y = innerRectangle.getY();
                        resultRect.height = innerRectangle.getHeight();
                    }

                    Graphics2D g2 = (Graphics2D) g.create();

                    g2.setColor(gridColor);
                    g2.setXORMode(Color.white);

                    if (gridStroke != null) {
                        g2.setStroke(gridStroke);
                    } else {//set width
                        g2.setStroke(new BasicStroke(handler.getStrokeWidth()));
                    }
                    Rectangle clip = g2.getClip().getBounds();

                    if (clip != null) {

                        final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
                        final SVGOMDocument currentDocument = (SVGOMDocument) currentHandle.getCanvas().getDocument();
                        final BridgeContext currentCtx = currentHandle.getCanvas().getBridgeContext();
                        Map<Element, List<IAnimated>> animatedElements = AnimationController.getAnimations(currentDocument, currentCtx);

                        for (Element key : animatedElements.keySet()) {
                            List<IAnimated> elementAnimations = animatedElements.get(key);


                            renderMovementCurve(getMovementAnimations(elementAnimations), g2);
                            //if is not rendered any helper, then show star as general animation
                            if (!renderSpiral(key, getRotateAnimations(elementAnimations), g2)) {
                                renderStar(key, g2);
                            }
                        }
                    }

                    g2.dispose();
                }
            }
        };

        canvas.addLayerPaintListener(SVGCanvas.GRID_LAYER, paintListener, false);
    }

    /**
     * renders star in center of animatedElement
     *
     * @param animatedElement element which is animated
     * @param g2              Graphics2D object to draw shape
     */
    private void renderStar(Element animatedElement, Graphics2D g2) {
        Shape shape = Editor.getEditor().getHandlesManager().getCurrentHandle().getSvgElementsManager().getGeometryOutline(animatedElement);
        double centerX = shape.getBounds2D().getCenterX();
        double centerY = shape.getBounds2D().getCenterY();

        Star2D star = new Star2D(0, 0, 10, 30, 5);
        Rectangle starBounds = star.getBounds();
        star.setX(centerX - starBounds.getWidth() / 2.0);
        star.setY(centerY - starBounds.getWidth() / 2.0);
        g2.draw(star);
    }

    /**
     * renders spiral in center of animatedElement
     *
     * @param animatedElement   element which is animated
     * @param elementAnimations list of animations which animates element
     * @param g2                Graphics2D object to draw shape
     * @return true if drawing was successful otherwise false
     */
    private boolean renderSpiral(Element animatedElement, List<SVGOMAnimationElement> elementAnimations, Graphics2D g2) {
        if (elementAnimations.size() > 0) {
            GeneralPath p = new GeneralPath();
            Shape shape = Editor.getEditor().getHandlesManager().getCurrentHandle().getSvgElementsManager().getGeometryOutline(animatedElement);

            double centerX = shape.getBounds2D().getCenterX();
            double centerY = shape.getBounds2D().getCenterY();
            final double mp8 = Math.PI / 180; // 1 Radian


            double end = 450 * mp8;
            float a = 3;

            double x = 0;
            double y = 0;
            p.moveTo(centerX, centerY);
            for (double theta = 0; theta < end; theta += mp8) {
                double r = a * theta;
                x = Math.cos(theta) * r + centerX;
                y = Math.sin(theta) * r + centerY;
                p.lineTo(x, y);
            }
            p.lineTo(x + 10, y + 10);
            p.moveTo(x, y);
            p.lineTo(x + 10, y - 10);
            g2.draw(p);
            return true;
        }
        return false;
    }

    /***
     * renders curve of movement
     *
     * @param elementAnimations movement animations
     * @param g2                Graphics2D object to draw shape
     * @return true if drawing was successful otherwise false
     */
    private boolean renderMovementCurve(List<SVGOMAnimationElement> elementAnimations, Graphics2D g2) {
        //TODO support for multiple move animations
        if (elementAnimations.size() > 0) {
            String pathString = elementAnimations.get(0).getAttribute("path");
            Path path = new Path(pathString);
            g2.draw(path);
            return true;
        }
        return false;
    }

    /**
     * get rotate animations
     *
     * @param elementAnimations list of animations
     * @return rotate animations (AnimateTransform element with rotate type attribute)
     */
    private List<SVGOMAnimationElement> getRotateAnimations(List<IAnimated> elementAnimations) {
        return elementAnimations
                .stream()
                .filter(aR -> aR.getAnimationElement() instanceof SVGOMAnimateTransformElement
                        && aR.getAnimationElement().getAttribute("type").equals("rotate"))
                .map(IAnimated::getAnimationElement)
                .collect(Collectors.toList());
    }

    /**
     * get movement animations
     *
     * @param elementAnimations list of animations
     * @return movement animations (AnimateMotion element)
     */
    private List<SVGOMAnimationElement> getMovementAnimations(List<IAnimated> elementAnimations) {
        return elementAnimations
                .stream()
                .filter(aR -> aR.getAnimationElement() instanceof SVGAnimateMotionElement)
                .map(IAnimated::getAnimationElement)
                .collect(Collectors.toList());
    }

    /**
     * refreshes the grid
     */

    public void refresh() {

        canvas.doRepaint(null);
    }
}
