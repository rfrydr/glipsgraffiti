package fr.itris.glips.svgeditor.display.canvas.animationsHelperLines;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.ModuleAdapter;
import fr.itris.glips.svgeditor.display.handle.HandlesListener;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 23.12.2015.
 */
public class AnimationsHelperModule extends ModuleAdapter {
    /**
     * the ids
     */
    private String helperDisplayId = "AnimationsHelperDisplay", helperParametersId = "AnimationsHelperParameters";

    /**
     * the labels
     */
    private String helperHiddenLabel = "", helperShownLabel = "", helperParametersLabel = "";

    /**
     * the menu item used to select the state of the grid display
     */
    private final JMenuItem helperDisplayMenuItem = new JMenuItem();

    /**
     * the menu item used to launch the dialog used to modify the grid parameters
     */
    private final JMenuItem helperParametersMenuItem = new JMenuItem();

    /**
     * the constructor of the class
     *
     * @param editor the editor
     */
    public AnimationsHelperModule(Editor editor) {

        //gets the labels from the resources
        ResourceBundle bundle = ResourcesManager.bundle;

        helperHiddenLabel = bundle.getString("AnimationsHelperHidden");
        helperShownLabel = bundle.getString("AnimationsHelperShown");
        helperParametersLabel = bundle.getString("AnimationsHelperParameters");

        //a listener that listens to the changes of the svg handles
        final HandlesListener svgHandlesListener = new HandlesListener() {

            @Override
            public void handleChanged(SVGHandle currentHandle, Set<SVGHandle> handles) {

                helperParametersMenuItem.setEnabled(currentHandle != null);

                if (currentHandle != null) {

                    //enables the menuitems
                    helperDisplayMenuItem.setEnabled(true);

                    if (Editor.getEditor().getHandlesManager().
                            getAnimationsHelperParametersManager().isHelpersEnabled()) {

                        helperDisplayMenuItem.setText(helperShownLabel);

                    } else {

                        helperDisplayMenuItem.setText(helperHiddenLabel);
                    }

                } else {

                    //disables the menuitems
                    helperDisplayMenuItem.setEnabled(false);
                }
            }
        };

        //adds the svg handles change listener
        editor.getHandlesManager().addHandlesListener(svgHandlesListener);

        //the menu items
        helperDisplayMenuItem.setText(helperHiddenLabel);
        helperDisplayMenuItem.setEnabled(false);

        helperParametersMenuItem.setText(helperParametersLabel);
        helperParametersMenuItem.setEnabled(false);

        //adds the listener
        ActionListener listener = new ActionListener() {

            /**
             * the method called when the action is done
             */
            public void actionPerformed(ActionEvent evt) {

                AnimationsHelperParametersManager gridParametersHandler =
                        Editor.getEditor().getHandlesManager().getAnimationsHelperParametersManager();

                if (evt.getSource().equals(helperDisplayMenuItem)) {

                    SVGHandle svgHandle =
                            Editor.getEditor().getHandlesManager().getCurrentHandle();

                    if (svgHandle != null) {

                        if (!gridParametersHandler.isHelpersEnabled()) {

                            gridParametersHandler.setHelpersEnabled(true);
                            helperDisplayMenuItem.setText(helperShownLabel);

                        } else {

                            gridParametersHandler.setHelpersEnabled(false);
                            helperDisplayMenuItem.setText(helperHiddenLabel);
                        }
                    }

                } else {

                    gridParametersHandler.launchDialog(helperParametersMenuItem);
                }
            }
        };

        helperDisplayMenuItem.addActionListener(listener);
        helperParametersMenuItem.addActionListener(listener);

        //setting the icons
        ImageIcon gridDisplayIcon = ResourcesManager.getIcon(helperDisplayId, false);
        ImageIcon gridDisplayDisabledIcon = ResourcesManager.getIcon(helperDisplayId, true);
        ImageIcon gridParametersIcon = ResourcesManager.getIcon(helperParametersId, false);
        ImageIcon gridParametersDisabledIcon = ResourcesManager.getIcon(helperParametersId, true);

        helperDisplayMenuItem.setIcon(gridDisplayIcon);
        helperDisplayMenuItem.setDisabledIcon(gridDisplayDisabledIcon);

        helperParametersMenuItem.setIcon(gridParametersIcon);
        helperParametersMenuItem.setDisabledIcon(gridParametersDisabledIcon);
    }

    @Override
    public HashMap<String, JMenuItem> getMenuItems() {

        HashMap<String, JMenuItem> menuItems = new HashMap<String, JMenuItem>();

        menuItems.put(helperDisplayId, helperDisplayMenuItem);
        menuItems.put(helperParametersId, helperParametersMenuItem);

        return menuItems;
    }
}
