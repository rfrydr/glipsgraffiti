package fr.itris.glips.svgeditor.display.canvas.animationsHelperLines;

import fr.itris.glips.library.PreferencesStore;
import fr.itris.glips.library.widgets.DashChooserWidget;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.canvas.ICanvasParametersManager;
import fr.itris.glips.svgeditor.display.handle.HandlesManager;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 27.12.2015.
 */
public class AnimationsHelperParametersManager implements ICanvasParametersManager {

    /**
     * the preference ids
     */
    private static final String GRID_ENABLED_PREF_ID = "AnimGridEnabled";
    private static final String COLOR_PREF_ID = "AnimGridColor";
    private static final String STROKE_PREF_ID = "AnimGridStroke";
    private static final String STROKE_WIDTH_ID = "AnimGridStrokeWidth";

    /**
     * the animation grid parameters dialog
     */
    private AnimationsHelperParametersDialog animationsHelperParamsDialog;

    protected static final boolean defaultHelpersEnabled = false;

    protected static final Color defaultHelpersColor = new Color(200, 200, 200);

    /**
     * the default stroke
     */
    protected static final BasicStroke defaultHelpersStroke = new BasicStroke(
            1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{2, 3}, 0);

    /**
     * the default string representation of the dashes for the stroke
     */
    protected static final String defaultStrokeDashesValues = "2 3";

    /**
     * the handles manager
     */
    private HandlesManager handlesManager;

    /**
     * whether the grid is enabled or not
     */
    private boolean helpersEnabled = true;

    /**
     * the color of the grid
     */
    private Color helpersColor = defaultHelpersColor;

    /**
     * the stroke of the grid
     */
    private BasicStroke helpersStroke = defaultHelpersStroke;

    /**
     * the string representation of the dashes for the stroke
     */
    private String strokeDashesValues = "2 3";
    private static final float defaultStrokeWidth = 3.0f;

    private float strokeWidth = defaultStrokeWidth;

    /**
     * the constructor of the class
     *
     * @param handlesManager the handles manager
     */
    public AnimationsHelperParametersManager(HandlesManager handlesManager) {

        this.handlesManager = handlesManager;

        initializeParameters();

        //creating the dialog used for setting the parameters
        if (Editor.getParent() instanceof Frame) {

            animationsHelperParamsDialog =
                    new AnimationsHelperParametersDialog(this, (Frame) Editor.getParent());

        } else if (Editor.getParent() instanceof JDialog) {

            animationsHelperParamsDialog =
                    new AnimationsHelperParametersDialog(this, (JDialog) Editor.getParent());
        }
    }

    /**
     * initializes the grid parameters
     */
    protected void initializeParameters() {

        //getting the parameters from the preference store
        try {
            helpersEnabled = Boolean.parseBoolean(
                    PreferencesStore.getPreference(null, GRID_ENABLED_PREF_ID));
        } catch (Exception ex) {
            helpersEnabled = defaultHelpersEnabled;
        }

        helpersColor = Editor.getColorChooser().getColor(null,
                PreferencesStore.getPreference(null, COLOR_PREF_ID));

        if (helpersColor == null) {

            helpersColor = defaultHelpersColor;
        }

        //computing the grid stroke
        String dashesStr = PreferencesStore.getPreference(null, STROKE_PREF_ID);
        handleDashes(dashesStr);

        try {
            strokeWidth = Float.parseFloat(PreferencesStore.getPreference(null, STROKE_WIDTH_ID));
        } catch (Exception ex) {
            strokeWidth = defaultStrokeWidth;
        }
    }

    /**
     * creates the stroke corresponding to the provided dashes string
     *
     * @param dashesString the string representation of dashes
     */
    protected void handleDashes(String dashesString) {

        if (dashesString != null) {

            //getting the array of the dash factors
            float[] dashes = DashChooserWidget.getDashes(dashesString);

            if (dashes.length > 0) {

                strokeDashesValues = dashesString;
                helpersStroke = new BasicStroke(strokeWidth, BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_BEVEL, 0, dashes, 0);

            } else {

                helpersStroke = null;
                strokeDashesValues = "";
            }

        } else {

            helpersStroke = defaultHelpersStroke;
            strokeDashesValues = defaultStrokeDashesValues;
        }
    }

    /**
     * launches the dialog used to modify the parameters
     *
     * @param relativeComponent the component relatively to
     *                          which the dialog should be shown
     */
    public void launchDialog(JComponent relativeComponent) {

        animationsHelperParamsDialog.showDialog(relativeComponent);

        if (animationsHelperParamsDialog.isCorrectValues()) {

            //updating the parameters of the grid
            Color color = animationsHelperParamsDialog.getColor();
            String dashesStr = animationsHelperParamsDialog.getDashes();
            float width = animationsHelperParamsDialog.getStrokeWidth();

            if (color != null) {
                helpersColor = color;
                if (dashesStr == null) {

                    dashesStr = "";
                }
                strokeWidth = width;
                handleDashes(dashesStr);
                updateHelpers();
            }
        }
    }

    /**
     * provides all the grids and animations helper lines with their new parameters
     */
    protected void updateHelpers() {

        for (SVGHandle handle : handlesManager.getHandles()) {

            handle.getCanvas().getAnimationsHelper().refresh();

        }

        updatePreferences();
    }

    /**
     * updates the preferences values
     */
    protected void updatePreferences() {

        PreferencesStore.setPreference(null,
                GRID_ENABLED_PREF_ID, Boolean.toString(helpersEnabled));

        PreferencesStore.setPreference(null,
                COLOR_PREF_ID, Editor.getColorChooser().getColorString(helpersColor));

        PreferencesStore.setPreference(null,
                STROKE_PREF_ID, strokeDashesValues);

        PreferencesStore.setPreference(null,
                STROKE_WIDTH_ID, String.valueOf(strokeWidth));
    }

    /**
     * @return whether the grid should be displayed
     */
    public boolean isHelpersEnabled() {

        return helpersEnabled;
    }

    /**
     * sets whether the grid should be displayed
     *
     * @param helpersEnabled whether the grid should be displayed
     */
    public void setHelpersEnabled(boolean helpersEnabled) {

        this.helpersEnabled = helpersEnabled;
        updateHelpers();
    }

    /**
     * @return the grid color
     */
    public Color getHelpersColor() {
        return helpersColor;
    }

    /**
     * @return the grid stroke
     */
    public BasicStroke getHelpersStroke() {
        return helpersStroke;
    }

    /**
     * @return the string representation of the dashes for the stroke
     */
    public String getStrokeDashesValues() {
        return strokeDashesValues;
    }

    /**
     * @return the horizontal distance
     */
    public double getHorizontalDistance() {
        return 0;
    }

    /**
     * @return the vertical distance
     */
    public double getVerticalDistance() {
        return 0;
    }

    @Override
    public Color getGridColor() {
        return helpersColor;
    }

    public float getStrokeWidth() {
        return strokeWidth;
    }
}
