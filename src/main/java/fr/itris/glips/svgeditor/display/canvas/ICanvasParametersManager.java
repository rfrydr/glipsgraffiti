package fr.itris.glips.svgeditor.display.canvas;

import java.awt.*;

/**
 * Created by Radek Frydrysek on 27.12.2015.
 */
public interface ICanvasParametersManager {
    double getHorizontalDistance();

    double getVerticalDistance();

    Color getGridColor();

    String getStrokeDashesValues();
}
