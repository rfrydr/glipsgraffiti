package fr.itris.glips.svgeditor.io.managers.export.handler;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.io.managers.export.FileExport;
import fr.itris.glips.svgeditor.io.managers.export.handler.dialog.VideoExportDialog;
import fr.itris.glips.svgeditor.io.managers.export.handler.dialog.ExportDialog;
import fr.itris.glips.svgeditor.io.managers.export.monitor.ExportMonitor;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.w3c.dom.Document;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Radek Frydrysek on 15.3.2015.
 */
public class VideoTranscoderAdapter extends Export {

    /**
     * the constructor of the class
     *
     * @param fileExport the object manager the export
     */
    protected VideoTranscoderAdapter(FileExport fileExport) {
        super(fileExport);

        //creating the dialog
        if (Editor.getParent() instanceof Frame) {

            exportDialog = new VideoExportDialog((Frame) Editor.getParent());

        } else {

            exportDialog = new VideoExportDialog((JDialog) Editor.getParent());
        }
    }

    @Override
    public void export(JComponent relativeComponent, Document document, File destFile) {
        monitor = new ExportMonitor(
                Editor.getParent(), 0, 100, FileExport.prefixLabels[4]);
        monitor.setRelativeComponent(relativeComponent);
        VideoExportDialog videoExportDialog = (VideoExportDialog) exportDialog;

        //showing the dialog used to select the values of the parameters for the export
        int res = exportDialog.showExportDialog(document);

        if (res == ExportDialog.OK_ACTION) {

            //getting the parameters
            width = videoExportDialog.getExportSize().getX();
            height = videoExportDialog.getExportSize().getY();
            //usePalette=aviExportDialog.usePalette();

            //creating the image
            writeVideo(document, destFile);
        }
    }

    protected void writeVideo(final org.w3c.dom.Document docOrig, final File destFile) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Transcoder transcoder = new MP4Transcoder(monitor);
                transcoder.addTranscodingHint(ImageTranscoder.KEY_WIDTH, (float) width);
                transcoder.addTranscodingHint(ImageTranscoder.KEY_HEIGHT, (float) height);
                TranscoderInput input = new TranscoderInput(docOrig);
                TranscoderOutput out = new TranscoderOutput(destFile.getAbsolutePath());
                try {
                    transcoder.transcode(input, out);
                } catch (TranscoderException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    @Override
    protected void writeImage(BufferedImage image, File destFile) {
    }
}
