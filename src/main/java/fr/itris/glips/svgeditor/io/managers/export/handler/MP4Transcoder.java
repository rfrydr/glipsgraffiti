package fr.itris.glips.svgeditor.io.managers.export.handler;

import fr.itris.glips.library.monitor.Monitor;
import fr.itris.glips.svgeditor.animations.AnimationController;
import org.apache.batik.anim.timing.TimedElement;
import org.apache.batik.anim.timing.TimegraphAdapter;
import org.apache.batik.bridge.*;
import org.apache.batik.ext.awt.image.GraphicsUtil;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.gvt.RootGraphicsNode;
import org.apache.batik.transcoder.*;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.keys.BooleanKey;
import org.jcodec.codecs.h264.H264Encoder;
import org.jcodec.codecs.h264.H264Utils;
import org.jcodec.common.NIOUtils;
import org.jcodec.common.SeekableByteChannel;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.containers.mp4.Brand;
import org.jcodec.containers.mp4.MP4Packet;
import org.jcodec.containers.mp4.TrackType;
import org.jcodec.containers.mp4.muxer.FramesMP4MuxerTrack;
import org.jcodec.containers.mp4.muxer.MP4Muxer;
import org.jcodec.scale.AWTUtil;
import org.jcodec.scale.RgbToYuv420;
import org.w3c.dom.svg.SVGDocument;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Radek Frydrysek on 11.7.2015.
 * Prizpusobena implementace (cast konvertujici BufferedImage do MP4) od autora Stanislav Vitvitskyy pro pouziti s rozhranim Transcoder
 * http://stackoverflow.com/questions/817524/what-library-can-i-use-to-encode-video-in-a-java-applet
 */
public class MP4Transcoder implements Transcoder {
    public static final TranscodingHints.Key KEY_FPS
            = new BooleanKey();
    private TranscodingHints transcodingHints;
    private ErrorHandler errorHandler;
    private int fps = 25;
    private int width = 640;
    private int height = 480;
    private Monitor monitor;
    private SeekableByteChannel ch;
    private Picture toEncode;
    private RgbToYuv420 transform;
    private H264Encoder encoder;
    private ArrayList<ByteBuffer> spsList;
    private ArrayList<ByteBuffer> ppsList;
    private FramesMP4MuxerTrack outTrack;
    private ByteBuffer _out;
    private int frameNo;
    private MP4Muxer muxer;

    public MP4Transcoder() {
        transcodingHints = new TranscodingHints();
        errorHandler = new DefaultErrorHandler();
    }

    public MP4Transcoder(Monitor monitor) {
        this();
        this.monitor = monitor;
    }

    @Override
    public void transcode(TranscoderInput transcoderInput, TranscoderOutput transcoderOutput) throws TranscoderException {
        if (monitor != null) {
            monitor.start();
            monitor.setProgress(0);
        }

        if (transcodingHints.containsKey(KEY_FPS)) {
            fps = Math.round((float) transcodingHints.get(KEY_FPS));
        }
        if (transcodingHints.containsKey(ImageTranscoder.KEY_HEIGHT)) {
            height = Math.round((float) transcodingHints.get(ImageTranscoder.KEY_HEIGHT));
        }
        if (transcodingHints.containsKey(ImageTranscoder.KEY_WIDTH)) {
            width = Math.round((float) transcodingHints.get(ImageTranscoder.KEY_WIDTH));
        }
        if (transcoderInput.getDocument() == null) {
            errorHandler.fatalError(new TranscoderException("Input document cannot be null!"));
        }
        SVGDocument document = (SVGDocument) transcoderInput.getDocument().cloneNode(true);

        UserAgentAdapter userAgent = null;
        GVTBuilder builder = null;
        BridgeContext ctx = null;
        RootGraphicsNode gvtRoot = null;

        AnimationController.normalize(document);
        try {
            userAgent = new UserAgentAdapter();
            ctx = new BridgeContext(userAgent, null, new DocumentLoader(userAgent));
            builder = new GVTBuilder();
            ctx.setDynamicState(BridgeContext.DYNAMIC);
            monitor.setProgress(5);

            if (monitor.isCancelled()) {
                monitor.stop();
                ctx.dispose();
                return;
            }

            GraphicsNode gvt = builder.build(ctx, document);

            if (gvt != null) {
                gvtRoot = gvt.getRoot();
                // create animation engine
                ctx.setAnimationLimitingNone();
                SVGAnimationEngine animationEngine = ctx.getAnimationEngine();
                final List<TimedElement> timedElementList = new ArrayList<TimedElement>();
                animationEngine.addTimegraphListener(new TimegraphAdapter() {
                    @Override
                    public void elementAdded(TimedElement e) {
                        timedElementList.add(e);
                    }
                });

                if (monitor.isCancelled()) {

                    monitor.stop();
                    ctx.dispose();
                    return;
                }

                // draw image
                if (!animationEngine.hasStarted()) {
                    animationEngine.start(0);
                }
                animationEngine.setCurrentTime(0);


                double duration = 0.0;
                try {
                    //TODO vazani animaci napr. one.end+5s
                    duration = 0.0;
                    for (int i = 0; i < timedElementList.size(); i++) {
                        double simpleDur = timedElementList.get(i).getSimpleDur();
                        double offset = timedElementList.get(i).getCurrentBeginTime();
                        if (duration < (offset + simpleDur) && (!Double.isInfinite(simpleDur) && !Double.isInfinite(offset))) {
                            duration = offset + simpleDur;
                        }
                    }
                } catch (Exception ex) {
                    // in case any error
                    duration = 1;
                }
                final long frames = Math.round(fps * duration);
                monitor.setProgress(20);

                if (monitor.isCancelled()) {

                    monitor.stop();
                    ctx.dispose();
                    return;
                }

                int monitorVal = 20;
                Init(new File(transcoderOutput.getURI()));
                for (float i = 1; i <= frames; i += 1) {
                    animationEngine.setCurrentTime(i / fps);
                    BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);

                    Graphics2D g2d = GraphicsUtil.createGraphics(bufferedImage);

                    g2d.fillRect(0, 0, width, height);
                    gvtRoot.paint(g2d);
                    g2d.dispose();
                    encodeImage(bufferedImage);
                    if (i % Math.round((frames / 10.0)) == 0) {
                        monitorVal += 10;
                        if (monitorVal < 90) {
                            monitor.setProgress(monitorVal);
                        }
                    }
                }
                monitor.setProgress(100);
                finish();
                monitor.stop();
            }
            ctx.dispose();


        } catch (Exception ex) {
            if (ctx != null) {
                ctx.dispose();
            }
        }
    }

    @Override
    public TranscodingHints getTranscodingHints() {
        return transcodingHints;
    }

    @Override
    public void setTranscodingHints(TranscodingHints transcodingHints) {
        this.transcodingHints = transcodingHints;
    }

    @Override
    public void addTranscodingHint(TranscodingHints.Key key, Object o) {
        transcodingHints.put(key, o);
    }

    @Override
    public void removeTranscodingHint(TranscodingHints.Key key) {
        transcodingHints.remove(key);
    }

    @Override
    public void setTranscodingHints(Map map) {
        transcodingHints.putAll(map);
    }

    @Override
    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    @Override
    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    private void Init(File out) throws IOException {
        this.ch = NIOUtils.writableFileChannel(out);

        // Transform to convert between RGB and YUV
        transform = new RgbToYuv420(0, 0);

        // Muxer that will store the encoded frames
        muxer = new MP4Muxer(ch, Brand.MP4);

        // Add video track to muxer
        outTrack = muxer.addTrackForCompressed(TrackType.VIDEO, fps);

        // Allocate a buffer big enough to hold output frames
        _out = ByteBuffer.allocate(1920 * 1080 * 6);

        // Create an instance of encoder
        encoder = new H264Encoder();

        // Encoder extra data ( SPS, PPS ) to be stored in a special place of
        // MP4
        spsList = new ArrayList<ByteBuffer>();
        ppsList = new ArrayList<ByteBuffer>();

    }

    private void encodeImage(BufferedImage bi) throws IOException {
        if (toEncode == null) {
            toEncode = Picture.create(bi.getWidth(), bi.getHeight(), ColorSpace.YUV420);
        }

        // Perform conversion
        for (int i = 0; i < 3; i++)
            Arrays.fill(toEncode.getData()[i], 0);
        transform.transform(AWTUtil.fromBufferedImage(bi), toEncode);

        // Encode image into H.264 frame, the result is stored in '_out' buffer
        _out.clear();
        ByteBuffer result = encoder.encodeFrame(_out, toEncode);

        // Based on the frame above form correct MP4 packet
        spsList.clear();
        ppsList.clear();
        H264Utils.encodeMOVPacket(result, spsList, ppsList);

        // Add packet to video track
        outTrack.addFrame(new MP4Packet(result, frameNo, fps, 1, frameNo, true, null, frameNo, 0));

        frameNo++;
    }

    private void finish() throws IOException {
        // Push saved SPS/PPS to a special storage in MP4
        outTrack.addSampleEntry(H264Utils.createMOVSampleEntry(spsList, ppsList));

        // Write MP4 header and finalize recording
        muxer.writeHeader();
        NIOUtils.closeQuietly(ch);
    }

    private BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }


}
