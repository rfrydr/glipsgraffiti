package fr.itris.glips.svgeditor.io.managers.export.handler.dialog;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 15.3.2015.
 */
public class VideoExportDialog extends ExportDialog {
    public VideoExportDialog(Frame parent) {
        super(parent);
        initialize();
    }

    public VideoExportDialog(JDialog parent) {
        super(parent);
        initialize();
    }

    @Override
    protected void initialize() {

        super.initialize();

        if (bundle != null) {
            try {
                exportDialogTitle = bundle.getString("labelaviexport");
            } catch (Exception ex) {
            }
        }

        //creating the size chooser panel
        JPanel sizechooser = getSizeChooserPanel();

        //setting the title of the dialog
        setTitle(exportDialogTitle);

        //handling the parameters panel
        parametersPanel.setLayout(new BoxLayout(parametersPanel, BoxLayout.Y_AXIS));
        parametersPanel.add(sizechooser);

    }
}
