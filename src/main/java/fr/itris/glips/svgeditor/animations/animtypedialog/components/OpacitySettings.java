package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 13.02.2016.
 */
public class OpacitySettings extends JPanel {
    private static final String beginOpacityLbl;
    private static final String endOpacityLbl;
    private static final String opacitySettingsLbl;

    static {
        beginOpacityLbl = ResourcesManager.bundle.getString("FadeInOutDialogBeginOpacity");
        endOpacityLbl = ResourcesManager.bundle.getString("FadeInOutDialogEndOpacity");
        opacitySettingsLbl = ResourcesManager.bundle.getString("FadeInOutDialogOpacitySettings");
    }

    private IAnimated currentAnimation;
    private JSlider beginOpacity;
    private JSlider endOpacity;
    private JLabel beginOpacityLabel;
    private JLabel endOpacityLabel;

    public OpacitySettings(IAnimated currentAnimation) {
        this.currentAnimation = currentAnimation;
        initialize();
        compose();
    }

    private void initialize() {
        beginOpacityLabel = new JLabel(beginOpacityLbl);
        endOpacityLabel = new JLabel(endOpacityLbl);

        beginOpacity = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
        beginOpacity.setPaintLabels(true);
        beginOpacity.setPaintTicks(true);
        beginOpacity.setPaintTrack(true);
        beginOpacity.setMajorTickSpacing(20);
        beginOpacity.setMinorTickSpacing(10);
        beginOpacity.addChangeListener(e -> {
            if (currentAnimation != null) {
                currentAnimation.updateFromAttribute(String.valueOf(beginOpacity.getValue() / 100.0));
            }
        });

        endOpacity = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        endOpacity.setPaintLabels(true);
        endOpacity.setPaintTrack(true);
        endOpacity.setPaintTicks(true);
        endOpacity.setMajorTickSpacing(20);
        endOpacity.setMinorTickSpacing(10);
        endOpacity.addChangeListener(e -> {
            if (currentAnimation != null) {
                currentAnimation.updateToAttribute(String.valueOf(endOpacity.getValue() / 100.0));
            }
        });
    }

    private void compose() {
        JPanel sliderAndLabelFrom = new JPanel();
        sliderAndLabelFrom.setLayout(new BorderLayout(0, 0));
        sliderAndLabelFrom.add(beginOpacity, BorderLayout.CENTER);
        sliderAndLabelFrom.add(beginOpacityLabel, BorderLayout.NORTH);

        JPanel sliderAndLabelTo = new JPanel();
        sliderAndLabelTo.setLayout(new BorderLayout(0, 0));
        sliderAndLabelTo.add(endOpacity, BorderLayout.CENTER);
        sliderAndLabelTo.add(endOpacityLabel, BorderLayout.NORTH);

        add(sliderAndLabelFrom);
        add(sliderAndLabelTo);
        setBorder(BorderFactory.createTitledBorder(opacitySettingsLbl));
    }

    public void updateAnimation(IAnimated animationObject) {
        String from = animationObject.getFromAttribute();
        String to = animationObject.getToAttribute();
        if (!((from.isEmpty() || from.trim().isEmpty()) && (to.isEmpty() || to.trim().isEmpty()))) {
            double fromOpacity = Double.parseDouble(from);
            double toOpacity = Double.parseDouble(to);
            beginOpacity.setValue((int) (fromOpacity * 100));
            endOpacity.setValue((int) (toOpacity * 100));
        }
    }

    public double getEndOpacity() {
        return endOpacity.getValue();
    }

    public double getBeginOpacity() {
        return beginOpacity.getValue();
    }
}
