package fr.itris.glips.svgeditor.animations.animtypedialog;

import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.*;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 22.08.2015.
 */
public class AnimateTransformDialog extends AbstractAnimationDialog {
    private static String tagName;
    private static String description;
    private static String transformationType;
    private static String transformSettingsLabel;

    static {
        tagName = ResourcesManager.bundle.getString("AnimateTransformDialogTagName");
        description = ResourcesManager.bundle.getString("AnimateTransformDialogDescription");
        transformationType = ResourcesManager.bundle.getString("AnimateTransformDialogTransformationType");
        transformSettingsLabel = ResourcesManager.bundle.getString("AnimateTransformDialogTransformSettings");
    }

    private JComboBox<String> type;

    private JLabel typeLabel;
    private FromToSettings fromToSettings;
    private DurationSettings durationSettings;
    private RepeatSettings repeatSettings;


    public AnimateTransformDialog(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    public AnimateTransformDialog(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateTransformElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }


    @Override
    public void setAnimation(IAnimated animationObject) {
        if (animationObject != null) {
            currentAnimation = animationObject;
            type.setSelectedItem(currentAnimation.getTransformType().toString());
            durationSettings.updateAnimation(currentAnimation);
            repeatSettings.updateAnimation(currentAnimation);
            fromToSettings.updateAnimation(currentAnimation);
        }
    }

    @Override
    public void initialize() {
//labels
        typeLabel = new JLabel(transformationType);

        //components
        repeatSettings = new RepeatSettings(currentAnimation);
        durationSettings = new DurationSettings(currentAnimation);
        fromToSettings = new FromToSettings(currentAnimation);
        header = new DialogHeader(tagName, description);
        fromToSettings.getInputVerifier().addChangeListener(new FromToVerifier.IValidationCompleted() {
            @Override
            public void InputValidated(boolean isValid, String errorMsg, String defaultValidValue) {
                if (isValid) {
                    header.hideError();
                    header.setLabels(tagName, "", description);

                } else {
                    header.showError("", errorMsg + "\n" + defaultValidValue);
                }
            }
        });
        type = new JComboBox<>(Util.enumToComboboxModel(TransformType.class));
    }

    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);


        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.HORIZONTAL;


        cons.gridx = 0;
        cons.gridy = 0;
        controlsPanel.add(durationSettings, cons);


        cons.gridx = 1;
        cons.gridy = 0;
        controlsPanel.add(repeatSettings, cons);


        cons.gridx = 0;
        cons.gridy = 1;
        controlsPanel.add(fromToSettings, cons);

        JPanel transformSettings = new JPanel();
        transformSettings.setBorder(BorderFactory.createTitledBorder(transformSettingsLabel));
        transformSettings.add(typeLabel);
        transformSettings.add(type);

        cons.gridx = 1;
        cons.gridy = 1;
        controlsPanel.add(transformSettings, cons);

        cons.gridx = 0;
        cons.gridy = 2;
        cons.fill = GridBagConstraints.BOTH;
        cons.weighty = cons.weightx = 1.0;
        controlsPanel.add(new JPanel(), cons);

        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }
}
