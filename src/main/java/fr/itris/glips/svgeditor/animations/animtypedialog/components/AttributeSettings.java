package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AttributeType;
import fr.itris.glips.svgeditor.animations.animtypedialog.Util;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.anim.AnimationEngine;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMElement;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Radek Frydrysek on 28.11.2015.
 */
public class AttributeSettings extends JPanel implements IComponent {
    private static String titleLbl, attributeTypeLbl, attributeNameLbl;

    static {
        titleLbl = ResourcesManager.bundle.getString("AttributeSettingsTitle");
        attributeTypeLbl = ResourcesManager.bundle.getString("AttributeSettingsTypeLbl");
        attributeNameLbl = ResourcesManager.bundle.getString("AttributeSettingsNameLbl");
    }

    private final JLabel attributeNameLabel;
    private final JLabel attributeTypeLabel;
    private JComboBox<String> attributeType;
    private JComboBox<String> attributeName;

    private IAnimated currentAnimation;

    private Set<IAttributeSettingsChanged> attributeChangedList;

    private List<String> cssAttributes = new ArrayList<>();
    private List<String> xmlAttributes = new ArrayList<>();

    public AttributeSettings(IAnimated currentAnimation) {
        this();
        this.currentAnimation = currentAnimation;
        InitComboBoxes();
    }

    private void InitComboBoxes() {
        if (!(currentAnimation.getAnimationElement() instanceof SVGOMAnimateTransformElement)) {
            SVGOMElement svgomElement = (SVGOMElement) currentAnimation.getAnimatedElement();
            NamedNodeMap attributes = svgomElement.getAttributes();
            xmlAttributes.clear();
            cssAttributes.clear();
            for (int i = 0; i < attributes.getLength(); i++) {
                String nodeName = attributes.item(i).getNodeName();
                if (getAttributeType(svgomElement, nodeName) == AnimationEngine.ANIM_TYPE_XML) {
                    if (svgomElement.isAttributeAnimatable(null, nodeName))
                        xmlAttributes.add(nodeName);
                } else { //CSS type
                    if (svgomElement.isPropertyAnimatable(nodeName))
                        cssAttributes.add(nodeName);
                }
                if (nodeName.toLowerCase().equals("style")) {
                    cssAttributes.addAll(parseStyle(svgomElement, attributes.item(i)));
                }
            }

            attributeName.setModel(new DefaultComboBoxModel(xmlAttributes.toArray()));
            if (currentAnimation.getAttributeType() != null) {
                attributeType.setSelectedItem(currentAnimation.getAttributeType());
            } else {
                attributeType.setSelectedIndex(0);
            }
            if (currentAnimation.getAttributeName() != null) {
                attributeName.setSelectedItem(currentAnimation.getAttributeName());
            } else {
                attributeName.setSelectedIndex(0);
            }
        } else {
            attributeType.setSelectedItem(AttributeType.XML);
            attributeType.setEnabled(false);
            String[] transformAttribute = {"Transform"};
            attributeName.setModel(new DefaultComboBoxModel<>(transformAttribute));
            attributeName.setEnabled(false);
        }
    }

    private List<String> parseStyle(SVGOMElement element, Node item) {
        List<String> attributes = new ArrayList<>();
        String[] styleAttributes = item.getNodeValue().split(";");
        for (int i = 0; i < styleAttributes.length; i++) {
            String attributeName = styleAttributes[i].substring(0, styleAttributes[i].indexOf(":"));
            if (element.isPropertyAnimatable(attributeName))
                attributes.add(attributeName);
        }
        return attributes;
    }

    private AttributeSettings() {
        attributeChangedList = new LinkedHashSet<>();
        attributeTypeLabel = new JLabel(attributeTypeLbl);
        attributeTypeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        attributeNameLabel = new JLabel(attributeNameLbl);
        attributeNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);

        attributeType = new JComboBox<>(Util.enumToComboboxModel(AttributeType.class));
        attributeName = new JComboBox<>(new DefaultComboBoxModel());

        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(titleLbl));
        this.setPreferredSize(new Dimension(250, 85));

        attributeName.setMinimumSize(new Dimension(200, 20));
        attributeType.setMinimumSize(new Dimension(200, 20));


        //AttributeType
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.EAST;
        cons.weighty = cons.weightx = 0.1;
        cons.insets = new Insets(2, 10, 2, 10);
        add(attributeTypeLabel, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        cons.weighty = cons.weightx = 0.1;
        add(attributeType, cons);

        //AttributeName
        cons.gridx = 0;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.EAST;
        cons.weighty = cons.weightx = 0.1;
        cons.insets = new Insets(2, 10, 2, 10);
        add(attributeNameLabel, cons);

        cons.gridx = 1;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.WEST;
        cons.weighty = cons.weightx = 0.1;
        add(attributeName, cons);

        attributeType.addItemListener(e -> {
            if (attributeType.getSelectedItem() != null) {
                AttributeType selectedType = getAttributeType();
                if (selectedType == AttributeType.XML) {
                    attributeName.setModel(new DefaultComboBoxModel(xmlAttributes.toArray()));
                } else if (selectedType == AttributeType.CSS) {
                    attributeName.setModel(new DefaultComboBoxModel(cssAttributes.toArray()));
                } else {
                    attributeName.setModel(new DefaultComboBoxModel(
                            Stream.concat(xmlAttributes.stream(), cssAttributes.stream()).collect(Collectors.toList()).toArray()
                    ));
                }
//                if (animation != null) {
//                    //animation.removeFromToAttributes();
//                    animation.updateAttributeType(selectedType);
//                    animation.updateAttributeName(attributeName.getSelectedItem().toString());
//                } else {
//                    System.out.println("AttributeSettings: animation is null");
//                }
                if (attributeChangedList != null && !attributeChangedList.isEmpty()) {
                    attributeChangedList.stream().forEach(aR -> {
                        aR.TypeChanged(getAttributeType(), getAttributeName());
                    });
                }
            }
        });

        attributeName.addItemListener(e -> {
            //Aktualizace probehne az pri zadani from/to hodnot, ted to akorat zpusobi problemy s validaci


//            animation.updateAttributeType(getAttributeType());
//            animation.updateAttributeName(getAttributeName());
            //animation.updateAttributeName(attributeName.getSelectedItem().toString());
            if (attributeChangedList != null && !attributeChangedList.isEmpty()) {
                attributeChangedList.stream().forEach(aR -> {
                    aR.NameChanged(getAttributeType(), getAttributeName());
                });
            }
        });

    }

    private int getAttributeType(Element element, String attributeName) {
        SVGOMElement svgomElement = (SVGOMElement) element;

        if (svgomElement.hasProperty(attributeName)) {
            return AnimationEngine.ANIM_TYPE_CSS;
        } else {
            return AnimationEngine.ANIM_TYPE_XML;
        }
    }

    public void addChangeListener(IAttributeSettingsChanged settingsChanged) {
        if (attributeChangedList == null) {
            attributeChangedList = new LinkedHashSet<>();
        }
        attributeChangedList.add(settingsChanged);
    }

    public void removeChangeListener(IAttributeSettingsChanged settingsChanged) {
        if (attributeChangedList != null)
            attributeChangedList.remove(settingsChanged);
    }

    @Override
    public void initComponents() {

    }

    @Override
    public void compose() {

    }

    @Override
    public IAnimated getCurrentAnimation() {
        return null;
    }

    @Override
    public void updateAnimation(IAnimated animation) {
        this.currentAnimation = animation;
        InitComboBoxes();
    }


    public interface IAttributeSettingsChanged {
        void TypeChanged(AttributeType attributeType, String attributeName);

        void NameChanged(AttributeType attributeType, String attributeName);
    }

    public AttributeType getAttributeType() {
        return Enum.valueOf(AttributeType.class, (String) attributeType.getSelectedItem());
    }

    public String getAttributeName() {
        return attributeName.getSelectedItem().toString();
    }
}
