package fr.itris.glips.svgeditor.animations.animtypedialog;

/**
 * Created by Radek Frydrysek on 30.08.2015.
 */
public enum RepeatType {
    definite,
    indefinite
}
