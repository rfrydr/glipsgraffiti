package fr.itris.glips.svgeditor.animations.animtypedialog;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DialogHeader;
import org.apache.batik.dom.svg.LiveAttributeValue;
import org.apache.batik.dom.svg.SVGGraphicsElement;
import org.apache.batik.dom.svg.SVGStylableElement;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Radek Frydrysek on 22.08.2015.
 */
public abstract class AbstractAnimationDialog extends JPanel {
    //protected SVGDocument currentDocument;
    protected IAnimated currentAnimation;
    protected Element selectedElement;
    protected DialogHeader header;

    public AbstractAnimationDialog(IAnimated currentAnimation) {
        this.currentAnimation = currentAnimation;
        initialize();
        compose();
        setAnimation(currentAnimation);
    }

    //new animation
    public AbstractAnimationDialog(Element selectedElement, IAnimated currentAnimation) {
        this.selectedElement = selectedElement;
        this.currentAnimation = currentAnimation;
        if (currentAnimation.getAnimationElement().getParentNode() == null) {
            currentAnimation.getAnimationElement().setParentNode(selectedElement);
        }
        initialize();
        compose();
        setAnimation(currentAnimation);
    }

    public abstract void compose();

    public abstract void setAnimation(IAnimated animationObject);

    public abstract void initialize();

    private java.util.List<String> getXmlAtributeNames(IAnimated animationObject) {
        java.util.List<String> attributeNames = new ArrayList<>();
        SVGGraphicsElement element = (SVGGraphicsElement) animationObject.getAnimatedElement();
        NamedNodeMap attributes = animationObject.getAnimatedElement().getAttributes();

        for (int j = 0; j < attributes.getLength(); j++) {
            /*atributy, ktere maji "LiveAttributeValue",
            tedy jejich hodnota je dynamicka,
            jsou animovatelne atributy
            LiveAttributeValue je rozhrani, ktere implementuji tridy reprezentujici animovatelne atributy + elementy reprezentujici styl,
            coz muze za jistych okolnosti zpusobit problemy

            */
            //TODO ne vzdy bude namespace null a zaroven tento kod nezajisti atributy tykajici se stylu
            LiveAttributeValue liveValue = element.getLiveAttributeValue(null, attributes.item(j).getNodeName());
            if (liveValue != null)
                attributeNames.add(attributes.item(j).getNodeName());

        }
        return attributeNames;
    }

    /*
    * Glips Graffiti vsehny xml stylove atributy prepisuje do atributu style,
    * tedy pokud mame fill="red", tak bude prepsane na style="fill:red;" timto muzeme
    * relativne snadno ziskat seznam pouzitych css atributu, ktere je mozne animovat
    * (animovat lze i neuvedene)
    * */

    /**
     * @param animationObject objekt reprezentujici animaci
     * @return seznam vsech dostupnych css atributu
     * vrati css atributy, ktere jsou v danem elementu uvedeny
     * nema smysl ziskavat nepouzite - v IE by se nezobrazovala staticka verze korektne
     */
    private java.util.List<String> getCssAtributeNames(IAnimated animationObject) {
        java.util.List<String> attributeNames = new ArrayList<>();
        SVGStylableElement element = (SVGStylableElement) animationObject.getAnimatedElement();
        CSSStyleDeclaration style = element.getStyle();

        for (int j = 0; j < style.getLength(); j++) {
            String property = style.item(j);
            CSSValue cssVal = style.getPropertyCSSValue(property);
            if (cssVal != null)
                attributeNames.add(property);

        }
        return attributeNames;
    }

    public interface IAttributeSettingsChanged {
        void DurationChanged();

        void BeginChanged();

    }

//    private void printAnimatableAttributes(Element element) {
//        SVGOMElement svgomElement = (SVGOMElement) element;
//        NamedNodeMap attributes = svgomElement.getAttributes();
//        for (int i = 0; i < attributes.getLength(); i++) {
//            if (getAttributeType(svgomElement, attributes.item(i).getNodeName()) == AnimationEngine.ANIM_TYPE_XML) {
//                if (isElementAnimatable(svgomElement, attributes.item(i).getNodeName())) {
//                    System.out.println("xml attribute " + attributes.item(i).getNodeName() + " is animatable");
//                } else {
//                    System.out.println("xml attribute " + attributes.item(i).getNodeName() + " is not animatable");
//                }
//            } else {
//                if (isCssAnimatable(svgomElement, attributes.item(i).getNodeName())) {
//                    System.out.println("css attribute " + attributes.item(i).getNodeName() + " is animatable");
//                } else {
//                    System.out.println("css attribute " + attributes.item(i).getNodeName() + " is not animatable");
//                }
//            }
//        }
//        //style attribute
//        //style = "fill:purple;stroke:blue;"
//        String styleAttribute = svgomElement.getAttribute("style");
//        String[] cssAttributes = styleAttribute.split(";");
//        for (
//                int i = 0;
//                i < cssAttributes.length; i++)
//
//        {
//            if (isCssAnimatable(svgomElement, cssAttributes[i].substring(0, cssAttributes[i].indexOf(":")))) {
//                System.out.println("yes");
//            } else {
//                System.out.println("no");
//            }
//        }


    //    }
    protected DefaultComboBoxModel getAttributeNamesModel(IAnimated animationObject, AttributeType attributeType) {
        java.util.List<String> attributeNames = new ArrayList<>();

        switch (attributeType) {
            case XML:
                attributeNames.addAll(getXmlAtributeNames(animationObject));
                break;
            case CSS:
                attributeNames.addAll(getCssAtributeNames(animationObject));
                break;
            case Auto:
                attributeNames.addAll(getXmlAtributeNames(animationObject));
                attributeNames.addAll(getCssAtributeNames(animationObject));
                break;
        }

        return new DefaultComboBoxModel(attributeNames.toArray());
    }

    public IAnimated getAnimation() {
        return currentAnimation;
    }

}
