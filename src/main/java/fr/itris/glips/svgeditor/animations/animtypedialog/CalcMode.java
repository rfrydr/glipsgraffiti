package fr.itris.glips.svgeditor.animations.animtypedialog;

/**
 * Created by Radek Frydrysek on 24.10.2015.
 */
public enum CalcMode {
    discrete,
    linear,
    paced,
    spline
}
