package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.bridge.CSSUtilities;
import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.value.Value;
import org.apache.batik.css.engine.value.ValueManager;
import org.apache.batik.css.parser.ExtendedParser;
import org.apache.batik.css.parser.Parser;
import org.apache.batik.dom.AbstractStylableDocument;
import org.apache.batik.dom.anim.AnimationTarget;
import org.w3c.css.sac.LexicalUnit;

import java.io.IOException;

/**
 * Created by Radek Frydrysek on 30.01.2016.
 */
public class CssAttributeValidator {
    private AnimationTarget animationTarget;
    private String validatingValue;
    private String attributeLocalName;

    private String errorMessage = "";

    private static final String defaultValueMessage;

    static {
        defaultValueMessage = ResourcesManager.bundle.getString("MessageValidValue");
    }


    public CssAttributeValidator(AnimationTarget animationTarget, String validatingValue, String attributeLocalName) {
        this.animationTarget = animationTarget;
        this.validatingValue = validatingValue;
        this.attributeLocalName = attributeLocalName;
    }

    public boolean isValid() {
        if (validatingValue == null || validatingValue.isEmpty() || validatingValue.trim().isEmpty())
            return false;

        boolean isValid = true;
        CSSEngine currentCssEngine = CSSUtilities.getCSSEngine(animationTarget.getElement());
        ExtendedParser cssParser = new Parser();
        LexicalUnit lu = null;
        try {
            lu = cssParser.parsePropertyValue(validatingValue);
        } catch (IOException e) {
            isValid = false;
        }
        ValueManager[] valueManagers = currentCssEngine.getValueManagers();
        int idx = currentCssEngine.getPropertyIndex(attributeLocalName); // nazev property
        //nebyl rozpoznan validni typ
        if (idx == -1) return false;
        ValueManager vm = valueManagers[idx];

        //pokud neni css atribut validni tak nastane vyjimka
        try {
            Value val2 = vm.createValue(lu, currentCssEngine);
        } catch (Exception ex) {
            isValid = false;
            errorMessage = ex.getMessage();
            if (errorMessage == null) {
                errorMessage = "";
            }
        }
        return isValid;
    }

    public String getDefaulValue() {
        String defaultValue = "";

        AbstractStylableDocument doc = (AbstractStylableDocument) animationTarget.getElement().getOwnerDocument();
        CSSEngine eng = doc.getCSSEngine();
        int idx = eng.getPropertyIndex(attributeLocalName);
        if (idx != -1) {
            ValueManager[] vms = eng.getValueManagers();
            defaultValue = (vms[idx].getDefaultValue().toString());
        }
        if (!defaultValue.trim().isEmpty()) {
            defaultValue = defaultValueMessage + " " + defaultValue;
        }
        return defaultValue;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
