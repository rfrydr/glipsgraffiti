package fr.itris.glips.svgeditor.animations.animtypedialog;

import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.AnimateMotionSettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DialogHeader;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DurationSettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.RepeatSettings;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMAnimateMotionElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 22.08.2015.
 */
public class AnimateMotionDialog extends AbstractAnimationDialog {

    static {
        tagName = ResourcesManager.bundle.getString("AnimateMotionDialogTagName");
        description = ResourcesManager.bundle.getString("AnimateMotionDialogDescription");
    }

    private static String tagName;
    private static String description;

    private DurationSettings durationSettings;
    private RepeatSettings repeatSettings;
    private AnimateMotionSettings animateMotionSettings;


    public AnimateMotionDialog(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    public AnimateMotionDialog(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateMotionElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }

    @Override
    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);


        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());


        GridBagConstraints cons = new GridBagConstraints();

        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 2;
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.HORIZONTAL;
        controlsPanel.add(repeatSettings, cons);

        cons.gridx = 2;
        cons.gridy = 0;
        cons.fill = GridBagConstraints.HORIZONTAL;
        controlsPanel.add(durationSettings, cons);

        cons.gridx = 0;
        cons.gridy = 1;
        cons.fill = GridBagConstraints.HORIZONTAL;
        controlsPanel.add(animateMotionSettings, cons);

        cons.gridx = 0;
        cons.gridy = 2;
        cons.fill = GridBagConstraints.BOTH;
        cons.weighty = cons.weightx = 1.0;
        controlsPanel.add(new JPanel(), cons);

        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }


    @Override
    public void setAnimation(IAnimated animationObject) {
        if (animationObject != null) {
            currentAnimation = animationObject;
            repeatSettings.updateAnimation(currentAnimation);
            durationSettings.updateAnimation(currentAnimation);
            animateMotionSettings.updateAnimation(currentAnimation);
        }
    }

    @Override
    public void initialize() {
//components
        repeatSettings = new RepeatSettings(currentAnimation);
        durationSettings = new DurationSettings(currentAnimation);
        animateMotionSettings = new AnimateMotionSettings(currentAnimation);
        header = new DialogHeader(tagName, description);
    }

}
