package fr.itris.glips.svgeditor.animations.animtimeline.components;

import fr.itris.glips.svgeditor.animations.AnimationsControlPanel;
import fr.itris.glips.svgeditor.animations.animtimeline.AnimationTimeLine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

/**
 * Created by Radek Frydrysek on 02.11.2015.
 */
public class MovableResizableButton extends JButton {
    private final Dimension MINIMUM_SIZE = new Dimension(25, 25);
    private final int PIXEL_TOLERANCE = 5;
    private boolean isHeld;
    private Point pointClicked;
    private Dimension startingSize;
    private boolean isLeftResize = false;
    private boolean isRightResize = false;
    protected ArrayList<IShapeChangingEvent> events = new ArrayList<>();
    protected ArrayList<IShapeChangedEvent> shapeChangedEvents = new ArrayList<>();

    public MovableResizableButton(String title) {
        super(title);

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                if (e.getX() <= PIXEL_TOLERANCE && e.getX() >= -PIXEL_TOLERANCE) {
                    setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                    isLeftResize = true;
                } else if (e.getX() <= (getWidth() + PIXEL_TOLERANCE) && e.getX() >= (getWidth() - PIXEL_TOLERANCE)) {
                    setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                    isRightResize = true;
                } else {
                    setCursor(new Cursor(Cursor.MOVE_CURSOR));
                    isRightResize = false;
                    isLeftResize = false;
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (isHeld) {
                    Dimension newSize = getSize();
                    Point newPosition = getLocation();
                    int zoomVal = (int) (AnimationTimeLine.defaultFrameWidth * AnimationsControlPanel.zoomValue);
                    int pointDiff = pointClicked.x - e.getPoint().x;
                    if (isRightResize) {
                        newSize = new Dimension(((startingSize.width - (pointDiff)) / zoomVal) * zoomVal,
                                (int) getSize().getHeight());
                    } else if (isLeftResize) {
                        Point startPoint = getLocation();

                        int increment = ((pointDiff) / zoomVal) * zoomVal;

                        newPosition = new Point((startPoint.x - increment),
                                startPoint.y);
                        newSize = new Dimension(getWidth() + (increment), startingSize.height);
                    } else {
                        Point startPoint = getLocation();
                        newPosition = new Point(((startPoint.x - (pointDiff)) / zoomVal) * zoomVal,
                                startPoint.y);
                    }


                    if (newSize.getWidth() > MINIMUM_SIZE.getWidth()) {
                        //TODO call when it's changed to prevent lots of calls
                        setPreferredSize(newSize);
                        if (!events.isEmpty())
                            events.stream().forEach(aR -> aR.changeOfShape());
                    }
                    if (newPosition.getX() >= 0) {
                        //TODO call when it's changed to prevent lots of calls
                        setBounds(new Rectangle(newPosition, getPreferredSize()));
                        if (!events.isEmpty())
                            events.stream().forEach(aR -> aR.changeOfShape());
                    }
                }
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                pointClicked = e.getPoint();
                startingSize = getSize();
                isHeld = true;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                isHeld = false;
                isLeftResize = false;
                isRightResize = false;
                if (!shapeChangedEvents.isEmpty())
                    shapeChangedEvents.stream().forEach(aR -> aR.changedShape());
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });
    }

    public void addShapeChangeEvent(IShapeChangingEvent event) {
        if (events == null)
            events = new ArrayList<>();

        events.add(event);
    }

    public void addShapeChangedEvent(IShapeChangedEvent event) {
        if (shapeChangedEvents == null)
            shapeChangedEvents = new ArrayList<>();

        shapeChangedEvents.add(event);
    }

    public interface IShapeChangingEvent {
        void changeOfShape();
    }

    public interface IShapeChangedEvent {
        void changedShape();
    }
}
