package fr.itris.glips.svgeditor.animations.animtypedialog;

/**
 * Created by Radek Frydrysek on 04.10.2015.
 */
public enum TransformType {
    translate,
    scale,
    rotate,
    skewX,
    skewY
}
