package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMElement;
import org.w3c.dom.Element;

/**
 * Created by Radek Frydrysek on 31.01.2016.
 */
public abstract class AbstractValidator {
    protected final String validatingValue;
    protected final SVGOMElement element;
    protected final FromToAttribute validatingAttributeName = FromToAttribute.to;
    protected SVGOMElement targetElement;
    protected SVGOMElement animationTarget;
    protected boolean isValid;
    protected String defaultValidValue;
    protected String errorMessage;

    protected AbstractValidator(SVGOMAnimationElement animationElement, String validatingValue) {
        this.validatingValue = validatingValue;
        this.element = (SVGOMElement) animationElement.cloneNode(true);

        if (!validatingValue.trim().isEmpty()) {
            this.element.setAttribute(validatingAttributeName.toString(), validatingValue);
        }

        Element t = (Element) animationElement.getParentNode();
        if (t instanceof SVGOMElement) {
            this.targetElement = (SVGOMElement) t;
            this.animationTarget = this.targetElement;
        }
    }

    public boolean isValid() {
        return isValid;
    }

    public String getDefaultValidValue() {
        return defaultValidValue;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    protected abstract boolean validate();

    protected abstract String setDefaultValue();

    protected abstract String setErrorMessage();
}
