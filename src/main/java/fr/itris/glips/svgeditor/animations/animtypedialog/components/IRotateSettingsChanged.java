package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import java.awt.*;

/**
 * Created by Radek Frydrysek on 06.02.2016.
 */
public interface IRotateSettingsChanged {
    void rotateSettingsChanged(int angle, Point rotatingPoint);
}
