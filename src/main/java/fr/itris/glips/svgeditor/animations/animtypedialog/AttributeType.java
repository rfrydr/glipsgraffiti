package fr.itris.glips.svgeditor.animations.animtypedialog;

/**
 * Created by Radek Frydrysek on 22.08.2015.
 */
public enum AttributeType {
    XML,
    CSS,
    Auto
}
