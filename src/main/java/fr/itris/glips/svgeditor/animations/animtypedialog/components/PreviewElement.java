package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.regex.Pattern;

/**
 * Created by Radek Frydrysek on 06.02.2016.
 */
public class PreviewElement extends JPanel {
    private static final String rotatingPointLabel;

    static {
        rotatingPointLabel = ResourcesManager.bundle.getString("RotatingPointLabel");
    }

    private JTextField setPoint;
    private Canvas canvas;
    private Pattern pattern = Pattern.compile("^([-+]?[0-9]+);([-+]?[0-9]+)$");
    private RotateSettings rotateSettings;

    /**
     * initialize PreviewElement component without point chooser
     *
     * @param preserveAspectRatio true if preserve aspect ratio
     */
    public PreviewElement(boolean preserveAspectRatio) {
        canvas = new Canvas(null, preserveAspectRatio);
        this.setLayout(new BorderLayout());
        this.add(canvas, BorderLayout.CENTER);
    }

    /**
     * initialize PreviewElement component with point chooser
     *
     * @param rotateSettings      rotate settings component where will be stored selected point coordinates
     * @param preserveAspectRatio true if preserve aspect ratio
     */
    public PreviewElement(RotateSettings rotateSettings, boolean preserveAspectRatio) {
        this.rotateSettings = rotateSettings;
        Shape shape = Editor.getEditor().getHandlesManager().getCurrentHandle().getSvgElementsManager().getGeometryOutline(rotateSettings.getCurrentAnimation().getAnimatedElement());
        canvas = new Canvas(shape, preserveAspectRatio);
        setPoint = new JTextField(5);

        setPoint.addActionListener(e -> {
            String text = setPoint.getText();
            if (pattern.matcher(text).matches()) {
                String[] newPoint = text.split(";");
                try {
                    int a = Integer.valueOf(newPoint[0]);
                    int b = Integer.valueOf(newPoint[1]);
                    canvas.setPoint(new Point(a, b));
                    setPoint.setBackground(Color.WHITE);
                } catch (Exception ex) {
                    setPoint.setBackground(Color.RED);
                }
            } else {
                setPoint.setBackground(Color.RED);
            }
        });

        canvas.selectedPointListener = point -> {
            setPoint.setText(String.format("%d;%d", (int) point.getX(), (int) point.getY()));
            setPoint.setBackground(Color.white);
        };

        JPanel pointSettings = new JPanel();
        pointSettings.add(new JLabel(rotatingPointLabel));
        pointSettings.add(setPoint);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(canvas);
        add(pointSettings);
    }

    public Point getPoint() {
        return canvas.getPoint();
    }

    public void setPoint(Point point) {
        canvas.setPoint(point);
        setPoint.setText(String.format("%d;%d", (int) point.getX(), (int) point.getY()));
        rotateSettings.setRotatePoint(point);
    }

    public void updateCanvas(Shape shape) {
        if (shape != null && canvas != null)
            canvas.updateShape(shape);
    }

    public interface IPointSelected {
        void selectedPoint(Point point);
    }

    private class Canvas extends JPanel {
        public IPointSelected selectedPointListener;
        private Shape shape;
        private Point point;
        private Rectangle2D viewRect = new Rectangle2D.Double(0, 0, 50, 50);
        private boolean preserveAspectRatio;

        public Canvas(Shape shape, boolean preserveAspectRatio) {
            this.preserveAspectRatio = preserveAspectRatio;
            this.setPreferredSize(new Dimension(52, 52));
            this.point = new Point(0, 0);
            setBackground(Color.white);
            if (shape != null)
                updateShape(shape);
            addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    setPoint(e.getPoint());
                    if (selectedPointListener != null) {
                        selectedPointListener.selectedPoint(point);
                    }
                    repaint();
                }
            });
        }

        public void updateShape(Shape shape) {
            this.shape = shape;
            repaint();
        }

        public Point getPoint() {
            return point;
        }

        public void setPoint(Point point) {
            this.point = point;
            rotateSettings.setRotatePoint(point);
            repaint();
        }

        private Shape drawShape(Shape shape) {
            if (shape == null) return null;
            Rectangle2D shapeBounds2D = shape.getBounds2D();

            double ratioW, ratioH;
            if (viewRect.getWidth() > this.getBounds().getWidth() && viewRect.getHeight() > this.getBounds().getHeight()) {
                ratioW = (viewRect.getWidth() / shapeBounds2D.getWidth());
                ratioH = (viewRect.getHeight() / shapeBounds2D.getHeight());
            } else {
                ratioW = (this.getBounds().getWidth() / shapeBounds2D.getWidth());
                ratioH = (this.getBounds().getHeight() / shapeBounds2D.getHeight());
            }

            AffineTransform scaleInstance;
            if (preserveAspectRatio) {
                double lowerRatio = ratioH > ratioW ? ratioW : ratioH;
                scaleInstance = AffineTransform.getScaleInstance(lowerRatio, lowerRatio);
            } else {
                scaleInstance = AffineTransform.getScaleInstance(ratioW, ratioH);
            }
            /*
            * at first we translate shape to default position (0,0)
            * then we need scale shape to fit inside a canvas
            * scaling causes little movement from default position,
            *   so we need make second translate transformation with -x,-y coordinates of scaled shape
            * */
            AffineTransform transforms[] =
                    {
                            AffineTransform.getTranslateInstance(viewRect.getX(), viewRect.getY()),
                            scaleInstance,
                            AffineTransform.getTranslateInstance(-shapeBounds2D.getX(), -shapeBounds2D.getY())
                    };

            AffineTransform tr = new AffineTransform();
            for (int i = 0; i < transforms.length; ++i) {
                tr.concatenate(transforms[i]);
            }

            return tr.createTransformedShape(shape);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Shape transformedShape = drawShape(shape);
            if (transformedShape != null) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setColor(new Color(100, 100, 255));
                g2.fill(transformedShape);
                if (point != null) {
                    g2.setColor(Color.black);
                    g2.drawLine(((int) point.getX()) - 5, ((int) point.getY()), ((int) point.getX()) + 5, ((int) point.getY()));
                    g2.drawLine(((int) point.getX()), ((int) point.getY()) - 5, ((int) point.getX()), ((int) point.getY()) + 5);
                }
            }
        }
    }
}
