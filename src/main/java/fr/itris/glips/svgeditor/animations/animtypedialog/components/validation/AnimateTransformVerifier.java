package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import fr.itris.glips.svgeditor.animations.animtypedialog.TransformType;
import org.apache.batik.anim.values.AnimatableValue;
import org.apache.batik.bridge.SVGAnimateTransformElementBridge;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;

/**
 * Created by Radek Frydrysek on 29.01.2016.
 */
public class AnimateTransformVerifier extends AbstractValidator {
    private final TransformType transformType;
    private TransformValidator validator;

    public AnimateTransformVerifier(SVGOMAnimateTransformElement animationElement, String validatingValue) {
        super(animationElement, validatingValue);
        this.transformType = TransformType.valueOf(element.getAttributeNS((String) null, "type"));

        isValid = validate();
        defaultValidValue = setDefaultValue();
        errorMessage = setErrorMessage();
    }

    @Override
    protected boolean validate() {
        if (validator == null) {
            validator = new TransformValidator(this);
        }
        return validator.validate();
    }

    @Override
    protected String setDefaultValue() {
        return TransformValidValuesHelper.getDefaultValue(transformType);
    }

    @Override
    protected String setErrorMessage() {
        if (isValid) return "";
        return TransformValidValuesHelper.getErrorMessage(transformType);
    }

    private class TransformValidator extends SVGAnimateTransformElementBridge {
        public TransformValidator(AbstractValidator validator) {
            this.element = validator.element;
            this.targetElement = validator.targetElement;
            this.animationTarget = validator.animationTarget;
        }

        private boolean validate() {
            AnimatableValue parsedVal = null;

            try {
                //typ transformace
                short type = this.parseType();
                if (super.element.hasAttributeNS((String) null, validatingAttributeName.toString())) {
                    //pokus o parse
                    parsedVal = this.parseValue(super.element.getAttributeNS(null, validatingAttributeName.toString()), type, targetElement);
                }
            } catch (Exception ex) {
                return false;
            }
            return (parsedVal != null);
        }
    }
}
