package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.AnimationController;
import fr.itris.glips.svgeditor.animations.animtypedialog.AbstractAnimationDialog;
import fr.itris.glips.svgeditor.animations.animtypedialog.predefined.DialogFactory;
import fr.itris.glips.svgeditor.animations.animtypedialog.predefined.NewAnimationDialog;
import fr.itris.glips.svgeditor.display.selection.Selection;
import fr.itris.glips.svgeditor.display.undoredo.UndoRedoAction;
import fr.itris.glips.svgeditor.display.undoredo.UndoRedoActionList;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 1.5.2015.
 */
public class AnimationsDetail extends JPanel {
    private static final String undoRedoLabel;

    static {
        undoRedoLabel = ResourcesManager.bundle.getString("NewAnimationUndoRedoLabel");
    }

    /**
     * the right panel
     */
    protected JPanel rightPanel = new JPanel();

    /**
     * the panel containing all the widgets
     */
    private JSplitPane allPanel =
            new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);
    /**
     * the jlabel for the no animations message
     */
    private JLabel noAnimationMessageJLabel;

    /**
     * the animations chooser panel
     */
    private AnimationsList animationsListPanel = null;

    /**
     * the current animation object
     */
    private IAnimated currentAnimationObject;

    /**
     * the constructor of the class
     */
    public AnimationsDetail() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        buildPanel();
    }

    /**
     * sets the animations objects that have to be handled
     *
     * @param animationObjects the list of the animation objects to be handled
     */

    public void setAnimations(LinkedList<IAnimated> animationObjects) {

        removeAll();

        if (animationObjects != null) {

            add(allPanel);
            animationsListPanel.setAnimations(animationObjects, new MenuAction());

        } else {
            animationsListPanel.clean();
            currentAnimationObject = null;
        }
    }

    /**
     * sets the new animation object to be displayed
     *
     * @param animationObject an animation object
     */
    public void setCurrentAnimation(final IAnimated animationObject) {

        currentAnimationObject = animationObject;
        rightPanel.removeAll();

        if (animationObject != null) {
            try {
                AbstractAnimationDialog dialog = DialogFactory.getDialog(currentAnimationObject);
                dialog.setAnimation(currentAnimationObject);
                rightPanel.add(dialog);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {

            rightPanel.setLayout(new BorderLayout());
            rightPanel.add(noAnimationMessageJLabel, BorderLayout.CENTER);
        }

        rightPanel.revalidate();
        rightPanel.repaint();
        allPanel.revalidate();
    }

    /**
     * builds the panel displaying the animation widgets
     */
    protected void buildPanel() {

        //creating the animations chooser
        animationsListPanel = new AnimationsList(this);

        //getting the labels
        String noAnimationMessage = "";

        try {
            noAnimationMessage = ResourcesManager.bundle.getString("label_nortdaanimations");
        } catch (Exception ex) {
        }

        noAnimationMessageJLabel = new JLabel(noAnimationMessage);
        noAnimationMessageJLabel.setHorizontalAlignment(SwingConstants.CENTER);

        //handling the panel containing all the other widgets
        allPanel.setDividerLocation(130);
        allPanel.setDividerSize(3);
        allPanel.setOneTouchExpandable(false);
        allPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

        //handling the right panel
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.X_AXIS));

        //adding the animations chooser panel
        allPanel.setLeftComponent(animationsListPanel);

        allPanel.setRightComponent(rightPanel);
        allPanel.setPreferredSize(new Dimension(640, 340));


        animationsListPanel.getAnimationsAndActionsMenu().addListener(new MenuAction());
    }

    class MenuAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            final Selection selection = Editor.getEditor().getHandlesManager().getCurrentHandle().getSelection();
            final AbstractAnimationDialog dialog = DialogFactory.getDialog((JMenuItem) e.getSource(), selection);
            if (dialog != null) {
                animationsListPanel.addNewTempAnim();
                final NewAnimationDialog newAnimation = new NewAnimationDialog(dialog);
                newAnimation.addButtonsListener(e1 -> {
                    NewAnimationDialog.NewAnimationAction actionResult = NewAnimationDialog.NewAnimationAction.valueOf(e1.getActionCommand());
                    if (actionResult == NewAnimationDialog.NewAnimationAction.ok) {
                        final IAnimated animation = dialog.getAnimation();
                        if (!selection.getSelectedElements().isEmpty() && animation != null && animation.getAnimationElement() != null) {
                            final Element selectedElement = selection.getSelectedElements().iterator().next();
                            /*
                            * Parent node is necessary to validate inputs,
                            * but is impossible to append new node with not null parent node
                            * */
                            animation.getAnimationElement().setParentNode(null);
                            Runnable executeRunnable = () -> selectedElement.appendChild(animation.getAnimationElement());

                            Runnable undoRunnable = () -> selectedElement.removeChild(animation.getAnimationElement());

                            Set<Element> editedElements = new HashSet<>();
                            editedElements.add(selectedElement);

                            UndoRedoAction action = new UndoRedoAction(
                                    undoRedoLabel, executeRunnable,
                                    undoRunnable, executeRunnable, editedElements);

                            UndoRedoActionList actionList = new UndoRedoActionList(action.getName(), false);
                            actionList.add(action);
                            Editor.getEditor().getHandlesManager().getCurrentHandle().getUndoRedo().addActionList(actionList, true);


                            setAnimations((LinkedList<IAnimated>) AnimationController.getAllAnimations(editedElements,
                                    Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getBridgeContext()));
                            animationsListPanel.handleListSelection(animationsListPanel.getListSize() - 1);
                        }
                    } else if (actionResult == NewAnimationDialog.NewAnimationAction.cancel) {
                        animationsListPanel.removeNewTempAnim();
                    }
                });
                rightPanel.removeAll();
                rightPanel.add(newAnimation);
                rightPanel.updateUI();
            }
        }
    }
}
