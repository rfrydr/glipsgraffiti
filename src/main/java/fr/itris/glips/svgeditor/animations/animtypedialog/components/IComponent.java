package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;

/**
 * Created by Radek Frydrysek on 19.12.2015.
 */
public interface IComponent {
    void initComponents();

    void compose();

    IAnimated getCurrentAnimation();

    void updateAnimation(IAnimated animation);
}
