package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.rtdaeditor.anim.disposer.RtdaFrameDisposer;
import fr.itris.glips.rtdaeditor.test.MainDisplayToolkitTest;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.ModuleAdapter;
import fr.itris.glips.svgeditor.actions.toolbar.ToolsFrame;
import fr.itris.glips.svgeditor.animations.AnimationController;
import fr.itris.glips.svgeditor.animations.AnimationsPanel;
import fr.itris.glips.svgeditor.display.canvas.dom.SVGDOMListener;
import fr.itris.glips.svgeditor.display.handle.HandlesListener;
import fr.itris.glips.svgeditor.display.handle.HandlesManager;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.display.selection.SelectionChangedListener;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 1.5.2015.
 */
public class AnimationsModule extends ModuleAdapter {
    /**
     * setting whether the rtda animations are handled or not
     */
    static {

        Editor.isRtdaAnimationsVersion = true;
        HandlesManager.handleDisposer = new RtdaFrameDisposer();
    }

    private final String moduleID = "AnimationsModule";

    private String saveDialogMessage = "", saveDialogTitle = "";
    /**
     * the menu item allowing to switch to the test mode
     */
    private JMenuItem launchTestMenuItem = new JMenuItem();
    /**
     * button which opens new window with playbeck of animation
     */
    private JButton launchTestButton = new JButton();
    /**
     * the nodes that are currently selected
     */

    private LinkedList<Element> selectedNodes = new LinkedList<>();
    /**
     * the header panel
     */
    private AnimationsHeader headerPanel;

    /**
     * main panel of AnimationsModule
     */
    private JPanel animationsMainPanel = new JPanel();

    /**
     * the animation panel which consist of left and right side
     * left - list of animations
     * right - detail of animation
     */
    private AnimationsDetail animationsPanel;
    /**
     * the animations and actions main display toolkit test
     */
    private MainDisplayToolkitTest mainDisplayToolkitTest;
    /**
     * the test dialog
     */
    private AnimationsPanel animationsTestPanel;
    /**
     * objects which contains menu and toolbar item
     * it can be used for set visibility of module
     */
    private ToolsFrame animationsToolFrame;
    /**
     * the current dom listener
     */
    private SVGDOMListener domListener;

    /**
     * the constructor of the class
     *
     * @param editor the editor
     */
    public AnimationsModule(final Editor editor) {
        //a listener that listens to the changes of the svg handles
        final HandlesListener svgHandleListener = new HandlesListener() {

            /**
             * a listener on the selection changes
             */
            private SelectionChangedListener selectionListener;

            /**
             * the last handle
             */
            private SVGHandle lastHandle;

            @Override
            public void handleChanged(SVGHandle currentHandle, Set<SVGHandle> handles) {
                if (handles.size() != 0) {
                    animationsToolFrame.getMenuItem().setEnabled(true);
                    animationsToolFrame.getToolBarButton().setEnabled(true);
                } else {
                    animationsToolFrame.getMenuItem().setEnabled(false);
                    animationsToolFrame.getToolBarButton().setEnabled(false);
                }

                if (lastHandle != null && selectionListener != null && lastHandle.getSelection() != null) {

                    //if a selection listener is already registered on a selection module, it is removed
                    lastHandle.getSelection().removeSelectionChangedListener(selectionListener);
                }

                //removing the dom listener
                if (domListener != null) {

                    domListener.removeListener();
                    domListener = null;
                }

                //gets the current selection module
                if (currentHandle != null) {
                    launchTestMenuItem.setEnabled(true);
                    launchTestButton.setEnabled(true);
                    manageSelection(new LinkedList<>(
                            currentHandle.getSelection().getSelectedElements()), true);

                } else {

                    launchTestMenuItem.setEnabled(false);
                    launchTestButton.setEnabled(false);
                    manageSelection(new LinkedList<>(), true);
                }

                if (currentHandle != null) {

                    //the listener of the selection changes
                    selectionListener = new SelectionChangedListener() {

                        @Override
                        public void selectionChanged(Set<Element> selectedElements) {

                            manageSelection(new LinkedList<>(selectedElements), false);
                        }
                    };

                    //adds the selection listener
                    if (selectionListener != null) {

                        currentHandle.getSelection().addSelectionChangedListener(selectionListener);
                    }

                } else {

                    //clears the list of the selected items
                    selectedNodes.clear();
                    refreshAnimationsAndActions(null);
                    selectionListener = null;
                }

                lastHandle = currentHandle;
            }

            /**
             * updates the selected items and the state of the menu items
             * @param selectedElements the list of the selected elements
             * @param frameChanged whether the frame has changed
             */
            protected void manageSelection(LinkedList<Element> selectedElements, boolean frameChanged) {

                boolean selectionNotChanged = false;

                if (!frameChanged) {

                    try {
                        //comparing the old and new selected nodes to check if the selection has changed,
                        //in order to know whether the frame should be refreshed or not
                        HashSet<Element> oldSel = new HashSet<>(selectedNodes);
                        HashSet<Element> newSel = new HashSet<>(selectedElements);

                        if (oldSel.equals(newSel) && newSel.equals(oldSel)) {

                            selectionNotChanged = true;
                        }
                    } catch (Exception ex) {
                    }
                }

                if (frameChanged || !selectionNotChanged) {

                    selectedNodes.clear();

                    //refresh the selected nodes list
                    if (selectedElements != null) {

                        selectedNodes.addAll(selectedElements);
                    }

                    if (selectedNodes.size() >= 1 && animationsToolFrame.isVisible()) {

                        refreshAnimationsAndActions(selectedNodes);

                    } else if (animationsToolFrame.isVisible()) {

                        refreshAnimationsAndActions(null);
                    }
                }
            }
        };

        //adds the svg handle change listener
        editor.getHandlesManager().addHandlesListener(svgHandleListener);

        //creating the main display toolkit
        animationsTestPanel = new AnimationsPanel();
        mainDisplayToolkitTest = new MainDisplayToolkitTest(animationsTestPanel);
        animationsTestPanel.initialize();

        //creating the header panel
        headerPanel = new AnimationsHeader(launchTestButton);

        //creating the animations and actions panel
        animationsPanel = new AnimationsDetail();

        //filling the tabbed pane
        String frameLabel = "", testLabel = "";

        try {
            saveDialogTitle = ResourcesManager.bundle.getString("label_rtdaanimationsandactionsSaveDialogTitle");
            saveDialogMessage = ResourcesManager.bundle.getString("label_rtdaanimationsandactionsSaveDialogMessage");
            frameLabel = ResourcesManager.bundle.getString("label_animator");
            testLabel = ResourcesManager.bundle.getString("label_animationModulePlayer");
        } catch (Exception ex) {
        }

        //creating the internal frame containing the animations panel
        animationsToolFrame = new ToolsFrame(
                editor, moduleID, frameLabel, animationsMainPanel);

        //disable and wait for loading
        animationsToolFrame.getMenuItem().setEnabled(false);
        animationsToolFrame.getToolBarButton().setEnabled(false);

        //setting the visibility changes handler
        Runnable visibilityRunnable = () -> {

            if (!animationsToolFrame.isVisible() || selectedNodes.size() == 0) {

                refreshAnimationsAndActions(null);

            } else {

                refreshAnimationsAndActions(selectedNodes);
            }
        };

        animationsToolFrame.setVisibilityChangedRunnable(visibilityRunnable);

        //handling the test menu item
        ImageIcon launchTestIcon = ResourcesManager.getIcon("TestLauncher", false);
        ImageIcon launchTestDisabledIcon = ResourcesManager.getIcon("TestLauncher", true);

        //handling the button used to launch the test dialog
        launchTestButton.setToolTipText(testLabel);
        launchTestButton.setEnabled(false);

        launchTestButton.setMargin(new Insets(1, 1, 1, 1));
        launchTestButton.setIcon(launchTestIcon);
        launchTestButton.setDisabledIcon(launchTestDisabledIcon);

        launchTestMenuItem.setText(testLabel);
        launchTestMenuItem.setIcon(launchTestIcon);
        launchTestMenuItem.setDisabledIcon(launchTestDisabledIcon);
        launchTestMenuItem.setEnabled(false);

        //the listener
        ActionListener testLauncherActionListener = evt -> launchTestDialog();
        launchTestButton.addActionListener(testLauncherActionListener);
        launchTestMenuItem.addActionListener(testLauncherActionListener);
    }

    @Override
    public HashMap<String, JMenuItem> getMenuItems() {
        HashMap<String, JMenuItem> menuItems = new HashMap<>();
        menuItems.put("Menu_" + this.moduleID, animationsToolFrame.getMenuItem());
        menuItems.put("Menu_" + this.moduleID + "TimeLine", launchTestMenuItem);
        return menuItems;
    }

    @Override
    public HashMap<String, AbstractButton> getToolItems() {
        HashMap<String, AbstractButton> map = new HashMap<>();
        map.put(moduleID, animationsToolFrame.getToolBarButton());
        return map;
    }

    /**
     * refreshes the animations and actions widgets
     *
     * @param nodes the list of nodes
     *              element that should be selected in the animations or actions chooser
     */
    protected void refreshAnimationsAndActions(
            LinkedList<Element> nodes) {
        SVGHandle handle = Editor.getEditor().getHandlesManager().getCurrentHandle();


        //getting the selected element
        Element element = null;

        if (nodes != null && nodes.size() == 1) {

            element = nodes.getFirst();
        }

        //setting the element for the header panel
        headerPanel.setCurrentElement(element);

        //removing the tabbed pane
        animationsMainPanel.removeAll();

        animationsMainPanel.setLayout(new BorderLayout());
        animationsMainPanel.add(headerPanel, BorderLayout.NORTH);
        animationsMainPanel.add(animationsPanel, BorderLayout.CENTER);
        if (handle != null) {
            if (AnimationController.isAnimated(handle.getCanvas().getDocument())) {
                animationsPanel.setAnimations((LinkedList<IAnimated>) AnimationController.getAllAnimations(selectedNodes, handle.getCanvas().getBridgeContext()));
            } else {
                animationsPanel.setAnimations(new LinkedList<>());
            }
            animationsToolFrame.revalidate();
        }
    }

    protected void launchTestDialog() {
        //getting the current handle
        SVGHandle handle = Editor.getEditor().getHandlesManager().getCurrentHandle();

        if (handle != null) {
            Runnable runnable = new Runnable() {
                public void run() {
                    //showing the test dialog
                    mainDisplayToolkitTest.getDialogTestImpl().refreshDialogState(true);
                }
            };
            Editor.getEditor().getIOManager().requestExecution(runnable);
        }
    }

}
