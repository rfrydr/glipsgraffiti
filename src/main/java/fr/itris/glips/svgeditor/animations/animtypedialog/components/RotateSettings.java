package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 06.02.2016.
 */
public class RotateSettings extends JPanel {
    private KnobComponent knobComponent;
    private PreviewElement previewElement;
    private IAnimated currentAnimation;

    private Point rotatingPoint;
    private int angle;

    private IRotateSettingsChanged rotateSettingsChanged;

    public RotateSettings(IAnimated animated) {
        currentAnimation = animated;
        knobComponent = new KnobComponent(this);
        previewElement = new PreviewElement(this, true);

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(knobComponent);
        add(previewElement);
    }

    public void updateAnimation(int angle, Point rotatingPoint) {
        this.angle = angle;
        this.rotatingPoint = rotatingPoint;
        knobComponent.setAngle(angle);
        previewElement.setPoint(rotatingPoint);
    }

    public int getAngle() {
        return knobComponent.getAngle();
    }

    public void setAngle(int angle) {
        this.angle = angle;
        if (rotateSettingsChanged != null)
            rotateSettingsChanged.rotateSettingsChanged(angle, rotatingPoint);
    }

    public Point getRotatePoint() {
        return previewElement.getPoint();
    }

    public void setRotatePoint(Point point) {
        this.rotatingPoint = point;
        if (rotateSettingsChanged != null)
            rotateSettingsChanged.rotateSettingsChanged(angle, rotatingPoint);
    }

    public IAnimated getCurrentAnimation() {
        return currentAnimation;
    }

    public void setRotateSettingsChanged(IRotateSettingsChanged listener) {
        this.rotateSettingsChanged = listener;
    }
}
