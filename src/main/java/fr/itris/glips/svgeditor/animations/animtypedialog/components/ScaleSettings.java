package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMAnimationElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 13.02.2016.
 */
public class ScaleSettings extends JPanel {
    private static final String proportionalModeLabel;
    private static final String nonProportionalModeLabel;
    private static final String beginScaleLabel;
    private static final String endScaleLabel;
    private static final String beginAndEndScaleLabel;
    private static final String beginScaleXaxisLabel;
    private static final String endScaleXaxisLabel;
    private static final String beginScaleYaxisLabel;
    private static final String endScaleYaxisLabel;

    static {
        proportionalModeLabel = ResourcesManager.bundle.getString("ScaleDialogProportionalMode");
        nonProportionalModeLabel = ResourcesManager.bundle.getString("ScaleDialogNonProportionalMode");
        beginScaleLabel = ResourcesManager.bundle.getString("ScaleDialogBeginScale");
        endScaleLabel = ResourcesManager.bundle.getString("ScaleDialogEndScale");
        beginAndEndScaleLabel = ResourcesManager.bundle.getString("ScaleDialogBeginAndEndScale");
        beginScaleXaxisLabel = ResourcesManager.bundle.getString("ScaleDialogBeginXaxisScale");
        endScaleXaxisLabel = ResourcesManager.bundle.getString("ScaleDialogEndXaxisScale");
        beginScaleYaxisLabel = ResourcesManager.bundle.getString("ScaleDialogBeginYaxisScale");
        endScaleYaxisLabel = ResourcesManager.bundle.getString("ScaleDialogEndYaxisScale");
    }

    private IAnimated currentAnimation;

    //keep proportions
    private JSpinner beginScale;
    private JSpinner endScale;

    //nonproportional
    private JSpinner beginScaleX;
    private JSpinner beginScaleY;
    private JSpinner endScaleX;
    private JSpinner endScaleY;

    private JRadioButton proportionalMode;
    private JRadioButton nonProportionalMode;
    private ButtonGroup proportionalSettings;

    private JPanel scaleSettingsPanel;
    private JPanel proportionalPanel;
    private JPanel nonProportionalPanel;

    public ScaleSettings(IAnimated currentAnimation) {
        this.currentAnimation = currentAnimation;
        initialize();
        compose();
    }

    public void initialize() {
        beginScaleX = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 1000, 0.1));
        beginScaleX.addChangeListener(e -> updateBeginScales());

        beginScaleY = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 1000, 0.1));
        beginScaleY.addChangeListener(e -> updateBeginScales());

        endScaleX = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 1000, 0.1));
        endScaleX.addChangeListener(e -> updateEndScales());

        endScaleY = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 1000, 0.1));
        endScaleY.addChangeListener(e -> updateEndScales());

        proportionalMode = new JRadioButton(proportionalModeLabel);
        proportionalMode.setSelected(true);
        proportionalMode.addActionListener(e -> {
            proportionalPanel.setVisible(true);
            nonProportionalPanel.setVisible(false);
        });

        nonProportionalMode = new JRadioButton(nonProportionalModeLabel);
        nonProportionalMode.addActionListener(e -> {
            proportionalPanel.setVisible(false);
            nonProportionalPanel.setVisible(true);
        });

        proportionalSettings = new ButtonGroup();
        proportionalSettings.add(proportionalMode);
        proportionalSettings.add(nonProportionalMode);

        beginScale = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 1000, 0.1));
        beginScale.addChangeListener(e -> updateProportionalBeginScales());

        endScale = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 1000, 0.1));
        endScale.addChangeListener(e -> updateProportionalEndScales());

        proportionalPanel = new JPanel();
        proportionalPanel.add(new JLabel(beginScaleLabel));
        proportionalPanel.add(beginScale);
        proportionalPanel.add(new JLabel(endScaleLabel));
        proportionalPanel.add(endScale);

        nonProportionalPanel = new JPanel();
    }

    private void compose() {
        nonProportionalPanel.setLayout(new GridBagLayout());
        GridBagConstraints cons = new GridBagConstraints();
        cons.weightx = cons.weighty = 0.1;
        cons.gridx = 0;
        cons.gridy = 0;
        cons.insets = new Insets(0, 0, 0, 5);
        nonProportionalPanel.add(new JLabel(beginScaleXaxisLabel), cons);
        cons.gridx = 1;
        cons.gridy = 0;
        nonProportionalPanel.add(beginScaleX, cons);
        cons.gridx = 2;
        cons.gridy = 0;
        cons.insets = new Insets(0, 10, 0, 5);
        nonProportionalPanel.add(new JLabel(endScaleXaxisLabel), cons);
        cons.gridx = 3;
        cons.gridy = 0;
        nonProportionalPanel.add(endScaleX, cons);
        cons.gridx = 0;
        cons.gridy = 1;
        cons.insets = new Insets(0, 0, 0, 5);
        nonProportionalPanel.add(new JLabel(beginScaleYaxisLabel), cons);
        cons.gridx = 1;
        cons.gridy = 1;
        nonProportionalPanel.add(beginScaleY, cons);
        cons.gridx = 2;
        cons.gridy = 1;
        cons.insets = new Insets(0, 10, 0, 5);
        nonProportionalPanel.add(new JLabel(endScaleYaxisLabel), cons);
        cons.gridx = 3;
        cons.gridy = 1;
        nonProportionalPanel.add(endScaleY, cons);
        nonProportionalPanel.setVisible(false);

        setBorder(BorderFactory.createTitledBorder(beginAndEndScaleLabel));
        add(proportionalMode);
        add(nonProportionalMode);
        add(proportionalPanel);
        add(nonProportionalPanel);
    }

    public void updateAnimation(IAnimated animationObject) {
        SVGOMAnimationElement animationElement = animationObject.getAnimationElement();
        String fromAttr = animationElement.getAttribute("from");
        String toAttr = animationElement.getAttribute("to");
        String[] fromValues = fromAttr.split(" ");
        String[] toValues = toAttr.split(" ");
        if (fromValues.length == 2 && toValues.length == 2) {
            if (fromValues[0].equals(fromValues[1]) && toValues[0].equals(toValues[1])) {
                proportionalMode.setSelected(true);
                nonProportionalPanel.setVisible(false);
                proportionalPanel.setVisible(true);
                double fromVal = Double.parseDouble(fromValues[0]);
                double toVal = Double.parseDouble(toValues[0]);
                beginScale.setValue(fromVal);
                endScale.setValue(toVal);
            } else {
                proportionalMode.setSelected(false);
                nonProportionalPanel.setVisible(true);
                proportionalPanel.setVisible(false);
                double fromValX = Double.parseDouble(fromValues[0]);
                double fromValY = Double.parseDouble(fromValues[1]);
                double toValX = Double.parseDouble(toValues[0]);
                double toValY = Double.parseDouble(toValues[1]);
                beginScaleX.setValue(fromValX);
                beginScaleY.setValue(fromValY);
                endScaleX.setValue(toValX);
                endScaleY.setValue(toValY);
            }
        }
    }

    private void updateProportionalEndScales() {
        if (currentAnimation != null) {
            currentAnimation.updateToAttribute(endScale.getValue().toString() + " " + endScale.getValue().toString());
        }
    }

    private void updateProportionalBeginScales() {
        if (currentAnimation != null) {
            currentAnimation.updateFromAttribute(beginScale.getValue().toString() + " " + beginScale.getValue().toString());
        }
    }

    private void updateEndScales() {
        if (currentAnimation != null) {
            currentAnimation.updateToAttribute(endScaleX.getValue().toString() + " " + endScaleY.getValue().toString());
        }
    }

    private void updateBeginScales() {
        if (currentAnimation != null) {
            currentAnimation.updateFromAttribute(beginScaleX.getValue().toString() + " " + beginScaleY.getValue().toString());
        }
    }

    public boolean isProportionalModeSelected() {
        return proportionalMode.isSelected();
    }

    public double getBeginScaleX() {
        return (double) beginScaleX.getValue();
    }

    public double getBeginScaleY() {
        return (double) beginScaleY.getValue();
    }

    public double getEndScaleX() {
        return (double) endScaleX.getValue();
    }

    public double getEndScaleY() {
        return (double) endScaleY.getValue();
    }

    public double getBeginScale() {
        return (double) beginScale.getValue();
    }

    public double getEndScale() {
        return (double) endScale.getValue();
    }
}
