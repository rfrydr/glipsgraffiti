package fr.itris.glips.svgeditor.animations;

import fr.itris.glips.library.geom.path.Path;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import org.apache.batik.bridge.*;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.apache.batik.dom.svg.SVGOMEllipseElement;
import org.apache.batik.dom.svg.SVGOMPathElement;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.GVTTreeRendererAdapter;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;

import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.*;

/**
 * Created by Radek Frydrysek on 16.5.2015.
 */
public class AnimationController {
    private static BridgeContext bridgeContext;
    private static SVGDocument document;
    private static boolean isLoaded = false;
    private static List<IAnimated> animations = new ArrayList<>();
    //events
    private static Set<IAnimationChangeEvent> events;
    private JSVGCanvas canvas = null;
    private float totalTime = 0.0f;
    private List<AnimationListener> listeners = new ArrayList<>();

    public AnimationController(final JSVGCanvas canvas) {

        canvas.addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
            @Override
            public void gvtRenderingStarted(GVTTreeRendererEvent e) {
                bridgeContext = canvas.getUpdateManager().getBridgeContext();
                document = canvas.getSVGDocument();
            }
        });

        canvas.addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
            @Override
            public void gvtRenderingCompleted(GVTTreeRendererEvent e) {

                isLoaded = true;
                //bridgeContext.getAnimationEngine().pause();
                totalTime = (float) getTotalTime();
                if (canvas.getUpdateManager() != null)
                    canvas.getUpdateManager().addUpdateManagerListener(new UpdateManagerAdapter() {
                        @Override
                        public void updateCompleted(UpdateManagerEvent e) {
                            if (bridgeContext.getAnimationEngine().getCurrentTime() == totalTime) {
                                for (AnimationListener listener : listeners) {
                                    listener.animationStopped();
                                }
                            }
                        }
                    });
            }
        });

        this.canvas = canvas;
    }

    public AnimationController(SVGDocument document, BridgeContext bridgeContext) {
        AnimationController.document = document;
        AnimationController.bridgeContext = bridgeContext;
    }

    public static SVGAnimationEngine getAnimationEngine() {
        return bridgeContext.getAnimationEngine();
    }

    public static double getTotalTime() {
        double longestAnimation = 0.0;
        if (isLoaded) {
            List<IAnimated> animations = getAllAnimations();

            for (IAnimated animation : animations) {
                double simpleDur = animation.getDuration();
                double offset = animation.getDurationOffset();
                longestAnimation = simpleDur;
                if (longestAnimation < (offset + simpleDur)) {
                    longestAnimation = offset + simpleDur;
                }
            }
        }

        return longestAnimation;
    }

    public static void updateAnimationsList() {
        animations.clear();
        getAllAnimations();
    }

    public static void animationChanged() {

        events.stream().forEach(aR -> aR.animationChange());
    }

    /**
     * Check if @param document contain any of animation tags
     *
     * @param document which is checked if contain animation tag
     */
    public static boolean isAnimated(Document document) {
        boolean returnVal = false;
        if (document != null) {
            if (document.getElementsByTagName("set").getLength() > 0) {
                returnVal = true;
            }
            if (document.getElementsByTagName("animate").getLength() > 0) {
                returnVal = true;
            }
            if (document.getElementsByTagName("animateColor").getLength() > 0) {
                returnVal = true;
            }
            if (document.getElementsByTagName("animateTransform").getLength() > 0) {
                returnVal = true;
            }
            if (document.getElementsByTagName("animateMotion").getLength() > 0) {
                returnVal = true;
            }
        }
        return returnVal;
    }

    /**
     * pokud se pouzije animace typu AnimateMotion, tak se zacne pozice objektu pocitat od krivky pohybu na misto os canvasu
     * pokud tomu chceme zabranit, tak se musi pro kazdy animovany element (animace AnimateMotion) prepocitat souradnice
     * u elipsy je to jednoduche - cx a cy je 0, bude se pohybovat stredem
     * u vsech ostatnich objektu se musi vzit polovina sirky a polovina hloubky
     * nektere elementy (predevsim ty ktere jsou dane cestou) nemaji sirku ani vysku a musi se prepocitat rovnou cela cesta
     * definujici jejich tvar
     */

    public static void normalize(SVGDocument document) {
        if (isAnimated(document)) {
            if (document.getElementsByTagName("animateMotion").getLength() > 0) {
                NodeList elements = document.getElementsByTagName("animateMotion");
                for (int i = 0; i < elements.getLength(); i++) {
                    Element parrent = (Element) elements.item(i).getParentNode();

                    //TODO tvar dany cestou (hvezda, ...)
                    if (parrent instanceof SVGOMEllipseElement) {
                        if (parrent.getAttribute("cx") != null) {
                            parrent.setAttribute("cx", "0");
                        }
                        if (parrent.getAttribute("cy") != null) {
                            parrent.setAttribute("cy", "0");
                        }
                    } else if (!(parrent instanceof SVGOMPathElement)) {
                        if (parrent.hasAttribute("width") && parrent.hasAttribute("x")) {
                            parrent.setAttribute("x", String.valueOf(-1.0 * Double.valueOf(parrent.getAttribute("width")) / 2.0));
                        }
                        if (parrent.hasAttribute("height") && parrent.hasAttribute("y")) {
                            parrent.setAttribute("y", String.valueOf(-1.0 * Double.valueOf(parrent.getAttribute("height")) / 2.0));
                        }
                    } else if (parrent instanceof SVGOMPathElement) {
                        Path path = new Path(parrent.getAttribute("d"));
                        Rectangle2D bounds = path.getBounds2D();
                        double widthElement = bounds.getWidth();
                        double heightElement = bounds.getHeight();
                        double xElement = bounds.getX();
                        double yElement = bounds.getY();
                        //dostat se k ose a pak vycentrovat
                        double tX = -xElement - widthElement / 2.0;
                        double tY = -yElement - heightElement / 2.0;
                        AffineTransform af = new AffineTransform();
                        af.setToTranslation(tX, tY);
                        path.applyTransform(af);

                        parrent.setAttribute("xBack", String.valueOf(xElement));
                        parrent.setAttribute("yBack", String.valueOf(yElement));

                        parrent.setAttribute("d", path.toString());
                    }
                }

            }
        }
    }

    /**
     * viz. predchozi metoda
     * aby se vse zobrazovalo pri kresleni stejne, tak se musi udelat opacna operace
     */
    public static void deNormalize(SVGDocument document) {
        if (isAnimated(document)) {
            if (document.getElementsByTagName("animateMotion").getLength() > 0) {

                //nastavit souradnice podle krivky - aby element byl na jejim zacatku
                NodeList elements = document.getElementsByTagName("animateMotion");
                for (int i = 0; i < elements.getLength(); i++) {
                    if (((Element) elements.item(i)).getAttribute("path") != null) {
                        Element parrent = (Element) elements.item(i).getParentNode();
                        //TODO tvar dany cestou (hvezda, ...)

                        //get path - get root segment - get its endpoint a x or y
                        double x = new Path(((Element) elements.item(i)).getAttribute("path")).getSegment().getEndPoint().getX();
                        double y = new Path(((Element) elements.item(i)).getAttribute("path")).getSegment().getEndPoint().getY();

                        if (parrent.getClass().equals(SVGOMEllipseElement.class)) {
                            if (parrent.getAttribute("cx") != null) {
                                parrent.setAttribute("cx", String.valueOf(x));
                            }
                            if (parrent.getAttribute("cy") != null) {
                                parrent.setAttribute("cy", String.valueOf(y));
                            }
                        } else if (!(parrent instanceof SVGOMPathElement)) {

                            if (parrent.getAttribute("x") != null) {
                                parrent.setAttribute("x", String.valueOf(x - Double.parseDouble(parrent.getAttribute("width")) / 2));
                            }
                            if (parrent.getAttribute("y") != null) {
                                parrent.setAttribute("y", String.valueOf(y - Double.parseDouble(parrent.getAttribute("height")) / 2));
                            }
                        } else if (parrent instanceof SVGOMPathElement) {
                            //revert
                            if ((parrent.getAttribute("xBack") != null && !parrent.getAttribute("xBack").isEmpty())
                                    && (parrent.getAttribute("yBack") != null && !parrent.getAttribute("yBack").isEmpty())) {
                                Path path = new Path(parrent.getAttribute("d"));
                                Rectangle2D bounds = path.getBounds2D();
                                double widthElement = bounds.getWidth();
                                double heightElement = bounds.getHeight();
                                double xElement = Double.valueOf(parrent.getAttribute("xBack"));
                                double yElement = Double.valueOf(parrent.getAttribute("yBack"));
                                parrent.removeAttribute("xBack");
                                parrent.removeAttribute("yBack");
                                double tX = -xElement + widthElement / 2.0;
                                double tY = -yElement + heightElement / 2.0;
                                AffineTransform af = new AffineTransform();
                                af.setToTranslation(tX, tY);
                                path.applyTransform(af);
                                parrent.setAttribute("d", path.toString());
                            }

                        }
                    }
                }

            }
        }

    }

    public static double getTotalTime(SVGOMDocument document, BridgeContext ctx) {
        List<IAnimated> animations = getAllAnimations(document, ctx);
        if (animations.isEmpty()) return 0;
        return animations
                .stream()
                .max(
                        (anim1, anim2) ->
                                Double.compare(anim1.getTotalTime(), anim2.getTotalTime())
                )
                .get()
                .getTotalTime();
    }

    public static HashMap<Element, List<IAnimated>> getAnimations(SVGOMDocument document, BridgeContext ctx) {
        HashMap<Element, List<IAnimated>> animationCrateHashMap = new HashMap<>();
        getAllAnimations(document, ctx).stream().forEach(aR -> {
            if (animationCrateHashMap.containsKey(aR.getAnimatedElement())) {
                animationCrateHashMap.get(aR.getAnimatedElement()).add(aR);
            } else {
                List<IAnimated> tempList = new ArrayList<IAnimated>();
                tempList.add(aR);
                animationCrateHashMap.put(aR.getAnimatedElement(), tempList);
            }
        });
        return animationCrateHashMap;
    }

    public static List<IAnimated> getAllAnimations(Collection<Element> elements, BridgeContext ctx) {
        List<IAnimated> animations = new LinkedList<>();
        if (ctx.getAnimationEngine() != null) {
            if (!ctx.getAnimationEngine().hasStarted()) {
                ctx.getAnimationEngine().start(0);
            }
            float previousTime = ctx.getAnimationEngine().getCurrentTime();
            ctx.getAnimationEngine().setCurrentTime(0);
            for (Element element : elements) {
                NodeList nodes = element.getChildNodes();
                for (int i = 0; i < nodes.getLength(); i++) {
                    Element child = (Element) nodes.item(i);
                    if (child instanceof SVGOMAnimationElement) {
                        animations.add(new Animation((SVGOMAnimationElement) child));
                    }
                }
            }
            ctx.getAnimationEngine().setCurrentTime(previousTime);
        }
        return animations;
    }

    public static List<IAnimated> getAllAnimations(SVGOMDocument document, BridgeContext ctx) {
        List<IAnimated> animations = new ArrayList<>();
        if (document != null && ctx != null && ctx.getAnimationEngine() != null && document.getCSSEngine() != null) {
            try {
                if (isAnimated(document)) {
                    if (!ctx.getAnimationEngine().hasStarted()) {
                        ctx.getAnimationEngine().start(0);
                    }
                    boolean isNan = false;
                    if (Float.isNaN(ctx.getAnimationEngine().getCurrentTime())) {
                        ctx.getAnimationEngine().setCurrentTime(0);
                        isNan = true;
                    }
                    NodeList nodeList = document.getElementsByTagName("*");
                    Node node;
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        node = nodeList.item(i);
                        if (node != null && node instanceof SVGOMAnimationElement) {
                            SVGOMAnimationElement element = (SVGOMAnimationElement) node;
                            IAnimated animated = new Animation((Element) element.getParentNode(), element);
                            animations.add(animated);
                        }
                    }
                    if (isNan) {
                        ctx.getAnimationEngine().setCurrentTime(Float.NaN);
                    }
                }
            } catch (Exception ex) {

            }
        }
        return animations;
    }

    public static List<IAnimated> getAllAnimations() {
        BridgeContext currentBridgeCtx = Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getBridgeContext();
        SVGOMDocument currentDoc = (SVGOMDocument) currentBridgeCtx.getDocument();
        if (animations.isEmpty()) {
            if (isLoaded) {
                animations.addAll(getAllAnimations(currentDoc, currentBridgeCtx));
            }
        }
        return animations;
    }

    public float getCurrentTime() {
        if (isLoaded) {
            return bridgeContext.getAnimationEngine().getCurrentTime();
        } else {
            return 0.0f;
        }
    }

    public void setCurrentTime(final float time) {
        if (isLoaded) {
            bridgeContext.getUpdateManager().getUpdateRunnableQueue().invokeLater(new Runnable() {
                @Override
                public void run() {
                    bridgeContext.getAnimationEngine().setCurrentTime(time);
                }
            });
        }
    }

    public void addUpdateManagerListener(final UpdateManagerListener listener) {
        if (canvas != null) {
            if (canvas.getUpdateManager() != null) {
                canvas.getUpdateManager().addUpdateManagerListener(listener);
            } else {
                canvas.addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
                    @Override
                    public void gvtRenderingCompleted(GVTTreeRendererEvent e) {
                        if (canvas.getUpdateManager() != null)
                            canvas.getUpdateManager().addUpdateManagerListener(listener);
                    }
                });
            }
        }
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void addListener(AnimationListener toAdd) {
        listeners.add(toAdd);
    }

    public void addEvent(IAnimationChangeEvent event) {
        if (events == null) {
            events = new LinkedHashSet<>();
        }
        events.add(event);
    }

    public void removeEvent(IAnimationChangeEvent event) {
        events.remove(event);
    }

    interface AnimationListener {
        void animationStopped();
    }

    public interface IAnimationChangeEvent {
        void animationChange();
    }
}
