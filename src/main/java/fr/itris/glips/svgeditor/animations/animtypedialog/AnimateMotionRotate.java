package fr.itris.glips.svgeditor.animations.animtypedialog;

/**
 * Created by Radek Frydrysek on 18.01.2016.
 */
public enum AnimateMotionRotate {
    disable("0"),
    auto("auto"),
    reverse("auto-reverse");

    private final String text;


    AnimateMotionRotate(final String text) {
        this.text = text;
    }

    public static AnimateMotionRotate getValue(String text) {
        switch (text) {
            case "0":
                return disable;
            case "auto":
                return auto;
            case "auto-reverse":
                return reverse;
            default:
                return disable;
        }
    }

    @Override
    public String toString() {
        return text;
    }
}
