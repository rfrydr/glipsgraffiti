package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.rtdaeditor.anim.ItemObject;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;

/**
 * Created by Radek Frydrysek on 1.5.2015.
 */
public class AnimationsList extends JPanel {
    /**
     * the handlers of the animations and actions creation menu, the first one handles the jwidget elements
     * and the second one handles the svg elements
     */
    private AnimationsMenu animationsMenu = null;

    /**
     * the animation list chooser
     */
    private JList animationList = null;

    /**
     * the list of the animation items
     */
    private LinkedList<AnimationListItem> listItems = new LinkedList<>();

    /**
     * the animation list chooser scrollpane
     */
    private JScrollPane listScrollPane = null;

    /**
     * the list model
     */
    private final DefaultListModel listModel = new DefaultListModel();

    /**
     * the icons for the buttons
     */
    protected static ImageIcon newIcon = ResourcesManager.getIcon("New", false),
            deleteIcon = ResourcesManager.getIcon("Delete", false),
            deleteDisabledIcon = ResourcesManager.getIcon("Delete", true);

    /**
     * the list selection listener
     */
    private final ListSelectionListener listSelectionListener = evt -> {
        //skip twice call
        if (!evt.getValueIsAdjusting())
            handleListSelection();
    };

    /**
     * the buttons used in the list panel
     */
    private JButton newButton, deleteButton;

    /**
     * the animation panel
     */
    private AnimationsDetail animationPanel;

    /**
     * the current animation objects list
     */
    private LinkedList<IAnimated> currentAnimations = new LinkedList<>();

    /**
     * label for new animation
     */
    private String newAnimationLabel;

    /**
     * the constructor of the class
     *
     * @param animationPanel the animation panel
     */
    public AnimationsList(AnimationsDetail animationPanel) {
        this.animationPanel = animationPanel;
        animationsMenu = new AnimationsMenu();

        buildPanel();
    }

    /**
     * cleans the panel
     */
    public void clean() {

        listModel.removeAllElements();
        currentAnimations.clear();
        animationsMenu.clean(false);
    }

    /**
     * builds this panel
     */
    protected void buildPanel() {

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(150, 277));

        //the list
        animationList = new JList(listModel);
        animationList.setBorder(new EmptyBorder(0, 0, 0, 0));
        listScrollPane = new JScrollPane(animationList);
        animationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        animationList.addListSelectionListener(listSelectionListener);

        //the buttons panel
        newButton = new JButton(newIcon);
        deleteButton = new JButton(deleteIcon);
        deleteButton.setDisabledIcon(deleteDisabledIcon);

        //getting the labels for the tool tip
        String newLabel = "", deleteLabel = "";

        try {
            newLabel = ResourcesManager.bundle.getString("labelnew");
            deleteLabel = ResourcesManager.bundle.getString("labeldelete");
            newAnimationLabel = ResourcesManager.bundle.getString("labelNewAnimation");
        } catch (Exception ex) {
        }

        Insets insets = new Insets(1, 1, 1, 1);

        newButton.setMargin(insets);
        newButton.setToolTipText(newLabel);
        deleteButton.setMargin(insets);
        deleteButton.setToolTipText(deleteLabel);

        JPanel buttons = new JPanel();
        buttons.setBorder(new EmptyBorder(2, 2, 2, 2));
        buttons.setLayout(new FlowLayout(FlowLayout.RIGHT, 2, 0));
        buttons.add(newButton);
        buttons.add(deleteButton);

        deleteButton.setEnabled(false);

        //adding the listeners to the buttons
        newButton.addActionListener(evt -> {

            final Point mousePosition = newButton.getMousePosition();

            if (mousePosition != null) {

                SwingUtilities.invokeLater(() -> showPopup(newButton, mousePosition));
            }
        });

        deleteButton.addActionListener(evt -> {

            //getting the current animation
            int selectedIndex = animationList.getSelectedIndex();
            deleteListItem(selectedIndex);
        });

        //adding a listener to the popup actions on the list
        MouseListener listListener = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (isPopUp(e)) {

                    showPopup(animationList, e.getPoint());
                }
            }

            /**
             * checks whether this mouse event denotes a popup trigger event or not
             * @param evt an event
             * @return whether this mouse event denotes a popup trigger event or not
             */
            protected boolean isPopUp(MouseEvent evt) {

                return evt.isPopupTrigger() || SwingUtilities.isRightMouseButton(evt);
            }
        };

        animationList.addMouseListener(listListener);

        //filling this panel
        add(listScrollPane, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);
        this.setSize(new Dimension(150, 600));
        this.setMinimumSize(new Dimension(150, 600));
    }

    /**
     * sets the new element to be the one whose animations are handled
     *
     * @param list a list of elements
     */
    public void setAnimations(LinkedList<IAnimated> list, ActionListener listener) {

        boolean popupMenuCreated = false;

        //clearing the list of the previous animation items
        listItems.clear();
        currentAnimations.clear();
        revalidate();

        if (list != null && list.size() > 0) {
            currentAnimations.addAll(list);
            handleList();
        } else {
            animationList.removeListSelectionListener(listSelectionListener);
            listModel.removeAllElements();
            animationList.addListSelectionListener(listSelectionListener);
            handleListSelection();
        }

        if (!popupMenuCreated) {
            animationsMenu.createPopUpMenu(listener);
        }
    }

    /**
     * handles the list state for the current element
     */
    protected void handleList() {

        animationList.removeListSelectionListener(listSelectionListener);
        listModel.removeAllElements();
        animationList.addListSelectionListener(listSelectionListener);

        if (currentAnimations.size() > 0) {

            animationList.removeListSelectionListener(listSelectionListener);

            for (IAnimated animationObject : currentAnimations) {

                listModel.addElement(new AnimationListItem(animationObject));
            }

            animationList.addListSelectionListener(listSelectionListener);

            int animationIndex = animationList.getSelectedIndex();

            if (animationIndex > -1) {
                animationPanel.setCurrentAnimation(
                        currentAnimations.get(animationIndex));
            }

        } else {

            animationPanel.setCurrentAnimation(null);
        }
    }

    protected void handleListSelection(int index) {
        if (index >= 0 && (listModel.getSize() - 1) >= index) {
            animationList.setSelectedIndex(index);
            handleListSelection();
        }
    }

    /**
     * handles the list selection changes
     */
    protected void handleListSelection() {
        if (animationList.getSelectedValue() == null || !animationList.getSelectedValue().equals(newAnimationLabel)) {
            int tempAnimationIndex = listModel.indexOf(newAnimationLabel);
            if (tempAnimationIndex > -1)
                listModel.remove(tempAnimationIndex);

            AnimationListItem currentSelection =
                    (AnimationListItem) animationList.getSelectedValue();

            //disabling all the buttons
            deleteButton.setEnabled(false);
            if (currentSelection != null) {

                //setting the new animation to be displayed
                animationPanel.setCurrentAnimation(currentSelection.getAnimationObject());

                //handling the delete button state
                deleteButton.setEnabled(true);

            } else {

                animationPanel.setCurrentAnimation(null);
            }

            SwingUtilities.invokeLater(() -> animationList.repaint());
        }
    }

    /**
     * @return the animationsAndActionsMenu
     */
    public AnimationsMenu getAnimationsAndActionsMenu() {
        return animationsMenu;
    }

    public void addNewTempAnim() {
        if (!listModel.contains(newAnimationLabel)) {
            listModel.addElement(newAnimationLabel);
            animationList.setSelectedIndex(listModel.size() - 1);
        } else {
            animationList.setSelectedIndex(listModel.indexOf(newAnimationLabel));
        }

    }

    public void removeNewTempAnim() {
        listModel.removeElement(newAnimationLabel);
        handleList();
    }

    /**
     * shows the popup create or execute action on the list items
     *
     * @param sourceComponent the source component where the action occured
     * @param clickedPoint    the clicked point
     */
    protected void showPopup(
            JComponent sourceComponent, Point clickedPoint) {//TODO

        getAnimationsAndActionsMenu().showPopupMenu(
                sourceComponent, clickedPoint);
    }

    /**
     * deletes the list item denoted by the provided index
     *
     * @param index a list item index
     */
    protected void deleteListItem(int index) {//TODO

        if (index >= 0) {

            AnimationListItem item = (AnimationListItem)
                    animationList.getModel().getElementAt(index);

            if (item != null) {

                IAnimated animationObject = item.getAnimationObject();

                if (animationObject != null) {

                    //removing this animation object
                    final SVGHandle handle = Editor.getEditor().getHandlesManager().getCurrentHandle();
                    final Element animationElement = animationObject.getAnimationElement();
                    final Element parentElement = (Element) animationElement.getParentNode();
                    Element sibling = null;

                    if (parentElement != null) {
                        //getting the animation that can be found after the selected animation
                        AnimationListItem nextItem = null;

                        if (index + 1 < animationList.getModel().getSize()) {

                            nextItem = (AnimationListItem) animationList.getModel().getElementAt(index + 1);

                            if (nextItem != null) {

                                sibling = nextItem.getAnimationObject().getAnimationElement();
                            }
                        }

                        currentAnimations.remove(animationObject);
                        if (animationList.getModel().getSize() != 0) {
                            animationList.setSelectedIndex(0);
                            handleListSelection();
                        }

                        final Element nextSibling = sibling;

                        //the runnable used to execute the action
                        Runnable executeRunnable = () -> {

                            parentElement.removeChild(animationElement);
                            handle.getSvgDOMListenerManager().
                                    fireNodeRemoved(parentElement, animationElement);
                            handle.getSelection().handleSelection(parentElement, false, true);
                            handleList();
                        };

                        //the undo runnable
                        Runnable undoRunnable = () -> {

                            if (nextSibling != null) {

                                parentElement.insertBefore(animationElement, nextSibling);

                            } else {

                                parentElement.appendChild(animationElement);
                            }

                            handle.getSvgDOMListenerManager().
                                    fireNodeInserted(parentElement, animationElement);
                            handle.getSelection().handleSelection(parentElement, false, true);
                            handleList();
                        };

                        //adding the undo/redo action
                        Editor.getEditor().getHandlesManager().getCurrentHandle().addUndoRedoAction(
                                executeRunnable, undoRunnable, ItemObject.animationUndoRedoLabel, executeRunnable);

                    }
                }
            }
        }
    }


    protected int getListSize() {
        return listModel.getSize();
    }


    /**
     * the class of the list items
     *
     * @author ITRIS, Jordi SUC
     */
    protected class AnimationListItem {

        /**
         * the animation object
         */
        private IAnimated animationObject;

        /**
         * the label for this animation
         */
        private String label;

        /**
         * the constructor of the class
         *
         * @param animationObject the animation object
         */
        public AnimationListItem(IAnimated animationObject) {

            this.animationObject = animationObject;
            label = animationObject.getAnimationLabel();

            String predefined = animationObject.getPredefinedLabel();
            if (!predefined.trim().isEmpty())
                label = label + "(" + predefined + ")";
        }

        /**
         * @return the animation object
         */
        public IAnimated getAnimationObject() {

            return animationObject;
        }

        @Override
        public String toString() {

            return label;
        }
    }


}
