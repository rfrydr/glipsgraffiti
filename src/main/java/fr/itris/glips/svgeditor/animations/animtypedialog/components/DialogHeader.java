package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.library.Icons;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 23.01.2016.
 */
public class DialogHeader extends JPanel {

    private static ImageIcon informationIcon, warningIcon, errorIcon;
    private static Image bannerImage;
    private static String defaultInformationLbl, defaultWarningLbl, defaultErrorLbl;

    private JLabel titleLbl;
    private JLabel messageIconLabel;
    private JTextArea messageLbl;

    private String title = new String();
    private String message = new String();
    private String iconLbl = new String();

    //load images
    static {
        informationIcon = Icons.getIcon("Information", false);
        warningIcon = Icons.getIcon("Warning", false);
        errorIcon = Icons.getIcon("Error", false);
        bannerImage = Icons.getIcon("Banner", false).getImage();
        defaultInformationLbl = ResourcesManager.bundle.getString("DialogHeaderInformation");
        defaultWarningLbl = ResourcesManager.bundle.getString("DialogHeaderWarning");
        defaultErrorLbl = ResourcesManager.bundle.getString("DialogHeaderError");
    }


    public DialogHeader(String title, String iconLbl, String message) {
        this.message = message;
        this.title = title;
        this.iconLbl = iconLbl;
        createHeader();
    }

    public DialogHeader(String title, String message) {
        this.message = message;
        this.title = title;
        createHeader();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (bannerImage != null) {

            g.drawImage(bannerImage, getWidth() - bannerImage.getWidth(this),
                    getHeight() - bannerImage.getHeight(this), this);
        }
    }

    private void createHeader() {
        setBackground(Color.white);

        //creating the jlabels
        titleLbl = new JLabel();
        titleLbl.setHorizontalAlignment(SwingConstants.LEFT);
        titleLbl.setFont(titleLbl.getFont().deriveFont(Font.BOLD));
        titleLbl.setBorder(new EmptyBorder(8, 8, 0, 5));
        titleLbl.setOpaque(false);

        messageIconLabel = new JLabel();
        messageIconLabel.setIcon(informationIcon);
        messageIconLabel.setOpaque(false);
        messageIconLabel.setBorder(new EmptyBorder(2, 0, 0, 5));

        messageLbl = new JTextArea();
        messageLbl.setRows(3);
        messageLbl.setLineWrap(true);
        messageLbl.setOpaque(false);
        messageLbl.setEditable(false);
        messageLbl.setWrapStyleWord(true);

        setLabels(title, iconLbl, message);

        //creating the message panel
        JPanel messagePanel = new JPanel();
        messagePanel.setBorder(new EmptyBorder(5, 10, 5, 5));
        messagePanel.setOpaque(false);

        GridBagLayout layout = new GridBagLayout();
        messagePanel.setLayout(layout);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.WEST;

        c.gridx = 0;
        c.gridy = 0;
        layout.setConstraints(messageIconLabel, c);
        messagePanel.add(messageIconLabel);

        JPanel emptyPanel = new JPanel();
        emptyPanel.setOpaque(false);
        c.gridy = 1;
        layout.setConstraints(emptyPanel, c);
        messagePanel.add(emptyPanel);

        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 2;
        c.weightx = 50;
        c.weighty = 50;

        layout.setConstraints(messageLbl, c);
        messagePanel.add(messageLbl);

        //creating the labels panel
        JPanel labelsPanel = new JPanel();
        labelsPanel.setOpaque(false);

        //setting the layout
        labelsPanel.setLayout(new BorderLayout(0, 0));
        labelsPanel.add(titleLbl, BorderLayout.NORTH);
        labelsPanel.add(messagePanel, BorderLayout.CENTER);

        //filling the title panel
        setLayout(new BorderLayout());
        add(labelsPanel, BorderLayout.CENTER);
        add(new JSeparator(), BorderLayout.SOUTH);
    }

    public void setLabels(String title, String iconLbl, String message) {
        messageIconLabel.setIcon(informationIcon);
        if (titleLbl != null)
            titleLbl.setText(title);

        if (messageIconLabel != null) {
            if (iconLbl.trim().isEmpty()) {
                messageIconLabel.setText(defaultInformationLbl);
            } else {
                messageIconLabel.setText(iconLbl);
            }
        }

        if (messageLbl != null)
            messageLbl.setText(message);
    }

    public void showWarning(String warningLbl, String warningDesc) {
        if (warningLbl.trim().isEmpty()) {
            messageIconLabel.setText(warningLbl);
        } else {
            messageIconLabel.setText(defaultWarningLbl);
        }
        messageLbl.setText(warningDesc);
        messageIconLabel.setIcon(warningIcon);
    }

    public void hideWarning() {
        setLabels(title, iconLbl, message);
        messageIconLabel.setIcon(informationIcon);
    }

    public void showError(String errorLbl, String errorDesc) {
        if (errorLbl.trim().isEmpty()) {
            messageIconLabel.setText(errorLbl);
        } else {
            messageIconLabel.setText(defaultErrorLbl);
        }
        messageLbl.setText(errorDesc);
        messageIconLabel.setIcon(errorIcon);
    }

    public void hideError() {
        setLabels(title, iconLbl, message);
        messageIconLabel.setIcon(informationIcon);
    }
}
