package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.AnimationController;
import fr.itris.glips.svgeditor.animations.animtypedialog.AnimateMotionRotate;
import fr.itris.glips.svgeditor.animations.animtypedialog.AttributeType;
import fr.itris.glips.svgeditor.animations.animtypedialog.RepeatType;
import fr.itris.glips.svgeditor.animations.animtypedialog.TransformType;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.display.undoredo.UndoRedoAction;
import fr.itris.glips.svgeditor.display.undoredo.UndoRedoActionList;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.anim.timing.TimedElement;
import org.apache.batik.bridge.SVGAnimationElementBridge;
import org.apache.batik.dom.svg.SVGOMAnimateMotionElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.w3c.dom.Element;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 4.5.2015.
 */
public class Animation implements IAnimated {
    //elements
    private Element animatedElement;
    private SVGOMAnimationElement animationElement;
//    private Document animatedElementOwner;
//    private Document animationElementOwner;

    //batik timed element
    private TimedElement timedElement;

    //attributes
    /**
     * dur attribute which specifies duration of animation
     * possible values are double + time unit
     * h	Hours
     * min	Minutes
     * s	Seconds
     * ms	Millisecond
     */
    private double duration;
    /**
     * begin attribute which specifies begin of animation
     * possible values are double + time mo
     * h	Hours
     * min	Minutes
     * s	Seconds
     * ms	Millisecond
     * <p>
     * or
     * <p>
     * "pointer" to another animation + begin or end attribute + double with time unit
     * for example one.end+5s (animation will start after 5 seconds after ending of first animation)
     */
    //TODO vazani animaci
    private double durationOffset;
    /**
     * repeatCount attribute which specifies how many times will animation repeat
     * possible values is integer
     */
    private int repeatCount;
    /**
     * this property also edits repeatCount attribute
     * possible values are definite or indefinite
     */
    private RepeatType repeatType;
    /***
     * repeatDur attribute which specifies how long will animation repeat until stops
     */
    private double repeatDur;
    /**
     * repeatDur ... same as repeatType (and same effect)
     */
    private RepeatType repeatDurType;
    /**
     * attributeType attribute specifies type of attribute which will animation changing
     * possible values are CSS, XML and auto
     */
    private AttributeType attributeType;
    /**
     * attributeName attribute specifies name of attribute which will animation changing
     * possible values is string representation
     */
    private String attributeName;
    /**
     * from attribute specifies starting value of attribute
     * possible values are various
     * for example attribute="x" from="0" which specifies starting position of object
     */
    private String from;
    /**
     * to attribute specifies ending value of attribute
     * possible values are various
     * for example attribute="x" to="100" which specifies ending position of object
     */
    private String to;
    /**
     * type attribute specifies type of transformation
     * possible values are representations of transformation (Enum: TransformType)
     */
    private String transformType;
    //misc
    private String animationLabel;

    public Animation(SVGOMAnimationElement element) {
        this((Element) element.getParentNode(), element);
    }

    public Animation(Element animatedElement, SVGOMAnimationElement animationElement) {
        /*
        * pokud se vytvari nova animace, tak je mozne ze element animace nema prirazeneho parenta, coz zpusobi problemy pri validaci
        * --zpusobuje problemy s append
        * */
//        if (animationElement.getParentNode() == null && animatedElement != null) {
//            animationElement.setParentNode(animatedElement);
//        }
        this.animatedElement = animatedElement;
        //this.animatedElementOwner = animatedElement.getOwnerDocument();
        this.animationElement = animationElement;
        //this.animationElementOwner = animationElement.getOwnerDocument();

        try {
            this.timedElement = ((SVGAnimationElementBridge) animationElement.getSVGContext()).getTimedElement();
        } catch (Exception e) {

        }

        try {
            this.repeatCount = Integer.parseInt(animationElement.getAttribute("repeatCount"));
            this.repeatType = RepeatType.definite;
        } catch (NumberFormatException ex) {
            this.repeatCount = 0;
            this.repeatType = RepeatType.indefinite;
        }

        try {
            String repeatDurVal = animationElement.getAttribute("repeatDur");
            if (repeatDurVal.equals("indefinite") || repeatDurVal.trim().isEmpty()) {
                this.repeatDurType = RepeatType.indefinite;
                this.repeatDur = 0;
            } else {
                this.repeatDurType = RepeatType.definite;
                this.repeatDur = Double.valueOf(repeatDurVal);
            }
        } catch (NumberFormatException ex) {
            this.repeatCount = 0;
            this.repeatType = RepeatType.indefinite;
        }

        try {
            attributeType = AttributeType.valueOf(animationElement.getAttribute("attributeType"));
        } catch (Exception ex) {

        }

        if (animationElement.getTagName().toLowerCase().equals("animatetransform")) {
            if (attributeType == null) {
                attributeType = AttributeType.XML;
            }
        }


        try {
            from = animationElement.getAttribute("from");
            to = animationElement.getAttribute("to");
        } catch (Exception ex) {

        }

        attributeName = animationElement.getAttribute("attributeName");
        if (attributeName.trim().length() == 0) {
            if (animationElement.getTagName().toLowerCase().equals("animatetransform")) {
                attributeName = "transform";
                if (attributeType == null) {
                    attributeType = AttributeType.XML;
                }
            }
        }

        try {
            if (animationElement.getTagName().toLowerCase().trim().equals("animatetransform")) {
                transformType = animationElement.getAttribute("type");
            }
        } catch (Exception ex) {

        }
        try {
            duration = parseTime(animationElement.getAttribute("dur"));

        } catch (Exception ex) {
            duration = 0.0;

        }
        try {
            durationOffset = parseTime(animationElement.getAttribute("begin"));

        } catch (Exception ex) {
            durationOffset = 0.0;

        }
        animationLabel = ResourcesManager.bundle.getString("label_" + animationElement.getLocalName());
    }

    //TODO convert any time to seconds in double
    private double parseTime(String dur) {
        return Double.valueOf(dur.replace("s", ""));
    }

    //GETTERS
    @Override
    public Element getAnimatedElement() {
        return animatedElement;
    }

    @Override
    public SVGOMAnimationElement getAnimationElement() {
        return animationElement;
    }

    @Override
    public double getDuration() {
        return duration;
    }

    /**
     * Set/Update duration of animation
     *
     * @param duration duration of animation in seconds
     *                 TODO implement duration in other units
     */
    @Override
    public void updateDuration(final double duration) {
        final double previousDuration = this.duration;
        this.duration = duration;

        if (duration != previousDuration) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            Runnable executeRunnable = () -> animationElement.setAttribute("dur", duration + "s");

            Runnable undoRunnable = () -> animationElement.setAttribute("dur", previousDuration + "s");

            addUndoRedoAction(executeRunnable, undoRunnable, "Duration", currentHandle);
        }
    }

    @Override
    public double getDurationOffset() {
        return durationOffset;
    }

    @Override
    public double getTotalTime() {
        return duration + durationOffset;
    }

    /**
     * Set/Update beginning time of animation
     *
     * @param durationOffset offset in seconds
     *                       TODO implement offset in other units
     */
    @Override
    public void updateDurationOffset(final double durationOffset) {
        final double previousValue = this.durationOffset;
        this.durationOffset = durationOffset;

        if (durationOffset != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("begin", durationOffset + "s");

            final Runnable undoRunnable = () -> animationElement.setAttribute("begin", previousValue + "s");

            addUndoRedoAction(executeRunnable, undoRunnable, "DurationOffset", currentHandle);
        }
    }

    private void addUndoRedoAction(Runnable redo, Runnable undo, String caption, SVGHandle currentHandle) {
        AnimationController.updateAnimationsList();
        Set<Element> desiredElement = new HashSet<>();
        desiredElement.add(animatedElement);
        desiredElement.add(animationElement);

        UndoRedoActionList actionList = new UndoRedoActionList(caption, true);
        actionList.add(new UndoRedoAction(
                caption, redo,
                undo, redo, desiredElement));
        currentHandle.getUndoRedo().addActionList(actionList, false);

        AnimationController.animationChanged();
    }

    @Override
    public String getAnimationLabel() {
        return animationLabel;
    }

    public TimedElement getTimedElement() {
        return timedElement;
    }

    @Override
    public int getRepeatCount() {
        return repeatCount;
    }

    @Override
    public void updateRepeatCount(final int repeatCount) {
        final int previousValue = this.repeatCount;
        this.repeatCount = repeatCount;

        if (repeatCount != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("repeatCount", String.valueOf(repeatCount));

            final Runnable undoRunnable = () -> animationElement.setAttribute("repeatCount", String.valueOf(previousValue));

            addUndoRedoAction(executeRunnable, undoRunnable, "RepeatCount", currentHandle);
        }
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    @Override
    public void updateRepeatType(final RepeatType repeatType) {
        if (repeatType == null) return;
        final RepeatType previousValue = this.repeatType;
        this.repeatType = repeatType;

        if (repeatType != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> {
                if (repeatType == RepeatType.definite) {
                    animationElement.setAttribute("repeatCount", "0");
                } else {
                    animationElement.setAttribute("repeatCount", repeatType.toString());
                }
            };

            final Runnable undoRunnable = () -> animationElement.setAttribute("repeatCount", previousValue.toString());

            addUndoRedoAction(executeRunnable, undoRunnable, "RepeatType", currentHandle);
        }
    }

    public double getRepeatDur() {
        return repeatDur;
    }

    @Override
    public void updateRepeatDur(final double repeatDur) {
        final double previousValue = this.repeatDur;
        this.repeatDur = repeatDur;

        if (repeatDur != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("repeatDur", repeatDur + "s");

            final Runnable undoRunnable = () -> animationElement.setAttribute("repeatDur", previousValue + "s");

            addUndoRedoAction(executeRunnable, undoRunnable, "RepeatDur", currentHandle);
        }
    }

    public RepeatType getRepeatDurType() {
        return repeatDurType;
    }

    @Override
    public void updateRepeatDurType(final RepeatType repeatDurType) {
        if (repeatDurType == null) return;
        final RepeatType previousValue = this.repeatDurType;
        this.repeatDurType = repeatDurType;

        if (repeatDurType != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("repeatDur", repeatDurType.toString());

            final Runnable undoRunnable = () -> animationElement.setAttribute("repeatDur", previousValue.toString());

            addUndoRedoAction(executeRunnable, undoRunnable, "RepeatDurType", currentHandle);
        }
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    @Override
    public void updateAttributeType(final AttributeType attributeType) {
        if (attributeType == null) return;
        final AttributeType previousValue = this.attributeType;
        this.attributeType = attributeType;

        if (attributeType != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("attributeType", attributeType.toString());

            final Runnable undoRunnable = () -> animationElement.setAttribute("attributeType", previousValue.toString());

            addUndoRedoAction(executeRunnable, undoRunnable, "AttributeType", currentHandle);
        }
    }

    public String getAttributeName() {
        return attributeName;
    }

    @Override
    public void updateAttributeName(final String attributeName) {
        if (attributeName == null || attributeName.trim().isEmpty()) return;
        final String previousValue = this.attributeName;
        this.attributeName = attributeName;

        if (!attributeName.equals(previousValue)) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("attributeName", attributeName);

            final Runnable undoRunnable = () -> animationElement.setAttribute("attributeName", previousValue);

            addUndoRedoAction(executeRunnable, undoRunnable, "AttributeName", currentHandle);
        }
    }

    @Override
    public String getFromAttribute() {
        return from;
    }

    @Override
    public void updateFromAttribute(final String from) {
        if (from == null || from.trim().isEmpty()) return;
        final String previousValue = this.from;
        this.from = from;

        if (!from.equals(previousValue)) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("from", from);

            final Runnable undoRunnable = () -> animationElement.setAttribute("from", previousValue);

            addUndoRedoAction(executeRunnable, undoRunnable, "From", currentHandle);
        }
    }

    @Override
    public String getToAttribute() {
        return to;
    }

    @Override
    public void updateToAttribute(final String to) {
        if (to == null || to.trim().isEmpty()) return;
        final String previousValue = this.to;
        this.to = to;

        if (!to.equals(previousValue)) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("to", to);

            final Runnable undoRunnable = () -> animationElement.setAttribute("to", previousValue);

            addUndoRedoAction(executeRunnable, undoRunnable, "To", currentHandle);
        }
    }

    public TransformType getTransformType() {
        return TransformType.valueOf(animationElement.getAttribute("type"));
    }

    @Override
    public void removeFromToAttributes() {
        final String previousFromVal = animationElement.getAttribute("from");
        final String previousToVal = animationElement.getAttribute("to");

        final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

        final Runnable executeRunnable = () -> {
            animationElement.removeAttribute("from");
            animationElement.removeAttribute("to");
        };

        final Runnable undoRunnable = () -> {
            animationElement.setAttribute("from", previousFromVal);
            animationElement.setAttribute("to", previousToVal);
        };

        addUndoRedoAction(executeRunnable, undoRunnable, "DisableFromTo", currentHandle);
    }

    @Override
    public void insertFromToAttributes(final String fromVal, final String toVal) {
        if (!(animationElement.hasAttribute("from") && animationElement.hasAttribute("to"))) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> {
                animationElement.setAttribute("from", fromVal);
                animationElement.setAttribute("to", toVal);
            };

            final Runnable undoRunnable = () -> {
                animationElement.removeAttribute("from");
                animationElement.removeAttribute("to");
            };

            addUndoRedoAction(executeRunnable, undoRunnable, "InsertFromTo", currentHandle);
        }
    }

    @Override
    public void updateAttributeSettings(String fromVal, String toVal, AttributeType attributeType, String attributeName) {
        final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

        final String previousFrom = animationElement.getAttribute("from");
        final String previousTo = animationElement.getAttribute("to");
        final String previousAttributeName = animationElement.getAttribute("attributeName");
        final String previousAttributeType = animatedElement.getAttribute("attributeType");

        this.from = fromVal;
        this.to = toVal;
        this.attributeType = attributeType;
        this.attributeName = attributeName;

        final Runnable executeRunnable = () -> {
            animationElement.setAttribute("from", fromVal);
            animationElement.setAttribute("to", toVal);
            animationElement.setAttribute("attributeName", attributeName);
            animationElement.setAttribute("attributeType", attributeType.toString());
        };

        final Runnable undoRunnable = () -> {
            animationElement.setAttribute("from", previousFrom);
            animationElement.setAttribute("to", previousTo);
            animationElement.setAttribute("attributeName", previousAttributeName);
            animationElement.setAttribute("attributeType", previousAttributeType);
        };

        addUndoRedoAction(executeRunnable, undoRunnable, "UpdateAttrSettings", currentHandle);
    }

    @Override
    public String getAnimateMotionPath() {
        if (animationElement instanceof SVGOMAnimateMotionElement) {
            return animationElement.getAttribute("path");
        }
        return "";
    }

    @Override
    public AnimateMotionRotate getAnimateMotionRotate() {
        if (animationElement instanceof SVGOMAnimateMotionElement) {
            return AnimateMotionRotate.getValue(animationElement.getAttribute("rotate"));
        }
        return AnimateMotionRotate.disable;
    }

    @Override
    public void updateAnimateMotionRotate(AnimateMotionRotate rotateValue) {
        final AnimateMotionRotate previousValue = this.getAnimateMotionRotate();

        if (rotateValue != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("rotate", rotateValue.toString());

            final Runnable undoRunnable = () -> animationElement.setAttribute("rotate", previousValue.toString());

            addUndoRedoAction(executeRunnable, undoRunnable, "rotate", currentHandle);
        }
    }

    @Override
    public void updateAnimateMotionPath(String pathValue) {
        final String previousValue = this.getAnimateMotionPath();

        if (!pathValue.equals(previousValue)) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("path", pathValue);

            final Runnable undoRunnable = () -> animationElement.setAttribute("path", previousValue);

            addUndoRedoAction(executeRunnable, undoRunnable, "path", currentHandle);
        }
    }

    @Override
    public String getPredefinedLabel() {
        return animationElement.getAttribute("predefined");
    }

    @Override
    public void updateTransformType(TransformType transformType) {
        final TransformType previousValue = this.getTransformType();

        if (transformType != previousValue) {
            final SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();

            final Runnable executeRunnable = () -> animationElement.setAttribute("type", transformType.toString());

            final Runnable undoRunnable = () -> animationElement.setAttribute("type", previousValue.toString());

            addUndoRedoAction(executeRunnable, undoRunnable, "transformType", currentHandle);
        }
    }
}
