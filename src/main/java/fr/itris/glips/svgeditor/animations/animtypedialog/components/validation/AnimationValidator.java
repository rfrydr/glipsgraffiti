package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import org.apache.batik.dom.svg.SVGOMAnimationElement;

/**
 * Created by Radek Frydrysek on 31.01.2016.
 */
public abstract class AnimationValidator extends AbstractValidator {

    protected final int animationType;
    protected final String attributeLocalName;
    protected final int attributeType;
    protected CssAttributeValidator cssAttributeValidator;

    protected AnimationValidator(SVGOMAnimationElement animationElement, String validatingValue, String attributeLocalName) {
        super(animationElement, validatingValue);
        if (this.element.hasProperty(attributeLocalName)) {
            this.animationType = 1;

            cssAttributeValidator = new CssAttributeValidator(this.animationTarget, validatingValue, attributeLocalName);

        } else {
            this.animationType = 0;

        }
        this.attributeLocalName = attributeLocalName;
        if (animationType == 1) {
            attributeType = this.targetElement.getPropertyType(attributeLocalName);
        } else {
            attributeType = this.targetElement.getAttributeType(null, attributeLocalName);
        }
        isValid = validate();
        defaultValidValue = setDefaultValue();
        errorMessage = setErrorMessage();
    }

    protected abstract boolean validate();

    @Override
    protected String setDefaultValue() {
        if (this.animationType == 1) {
            return cssAttributeValidator.getDefaulValue();
        } else {
            return XmlValidValuesHelper.getDefaultValue(attributeType);
        }
    }

    @Override
    protected String setErrorMessage() {
        if (isValid) return "";

        if (this.animationType == 1) {
            return cssAttributeValidator.getErrorMessage();
        } else {
            return XmlValidValuesHelper.getErrorMessage(attributeType);
        }
    }
}
