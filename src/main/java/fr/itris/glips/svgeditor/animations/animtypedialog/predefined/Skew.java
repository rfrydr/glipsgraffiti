package fr.itris.glips.svgeditor.animations.animtypedialog.predefined;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AbstractAnimationDialog;
import fr.itris.glips.svgeditor.animations.animtypedialog.TransformType;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DialogHeader;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DurationSettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.RepeatSettings;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimateTransformElement;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 01.02.2016.
 */
public class Skew extends AbstractAnimationDialog {
    private static final String fromAngleSettingsLabel;
    private static final String toAngleSettingsLabel;
    private static final String yAxisSkewLabel;
    private static final String xAxisSkewLabel;

    static {
        fromAngleSettingsLabel = ResourcesManager.bundle.getString("SkewAngleFromAngleSettingsLabel");
        toAngleSettingsLabel = ResourcesManager.bundle.getString("SkewAngleToAngleSettingsLabel");
        xAxisSkewLabel = ResourcesManager.bundle.getString("SkewXaxisSkewLabel");
        yAxisSkewLabel = ResourcesManager.bundle.getString("SkewYaxisSkewLabel");
    }

    private RepeatSettings repeatSettings;
    private DurationSettings durationSettings;

    private JPanel angleSettingsPanel;
    private JSpinner fromAngle;
    private JLabel fromAngleLabel;
    private JSpinner toAngle;
    private JLabel toAngleLabel;


    private JRadioButton xAxisSkew;
    private JRadioButton yAxisSkew;
    private ButtonGroup axisSettings;

    public Skew(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    public Skew(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateTransformElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }

    @Override
    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);


        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        //AttributeType
        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.BOTH;

        cons.gridx = 2;
        cons.gridy = 0;
        cons.gridwidth = 2;
        cons.gridheight = 3;


        controlsPanel.add(angleSettingsPanel, cons);
        cons.weighty = cons.weightx = 0.1;
        //From

        cons.gridheight = 2;
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.gridx = 0;
        cons.gridy = 0;

        controlsPanel.add(repeatSettings, cons);

        cons.gridx = 0;
        cons.gridy = 2;

        controlsPanel.add(durationSettings, cons);


        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }


    @Override
    public void setAnimation(IAnimated animationObject) {
        currentAnimation = animationObject;
        String from = animationObject.getFromAttribute();
        String to = animationObject.getToAttribute();

        if (!((from.isEmpty() || from.trim().isEmpty()) && (to.isEmpty() || to.trim().isEmpty()))) {
            double fromAngleVal = Double.parseDouble(from);
            double toAngleVal = Double.parseDouble(to);
            fromAngle.setValue((int) (fromAngleVal));
            toAngle.setValue((int) (toAngleVal));
        }

        if (animationObject.getTransformType() == TransformType.skewX) {
            xAxisSkew.setSelected(true);
            yAxisSkew.setSelected(false);
        } else {
            xAxisSkew.setSelected(false);
            yAxisSkew.setSelected(true);
        }

        durationSettings.updateAnimation(animationObject);
        repeatSettings.updateAnimation(animationObject);
    }

    @Override
    public void initialize() {
        header = new DialogHeader("Skew animation", "Animate skew of object");
        repeatSettings = new RepeatSettings(currentAnimation);
        durationSettings = new DurationSettings(currentAnimation);

        fromAngle = new JSpinner(new SpinnerNumberModel(0, 0, 360, 1));
        fromAngle.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (currentAnimation != null)
                    currentAnimation.updateFromAttribute(fromAngle.getValue().toString());
            }
        });
        fromAngleLabel = new JLabel(fromAngleSettingsLabel);
        toAngle = new JSpinner(new SpinnerNumberModel(0, 0, 360, 1));
        toAngle.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (currentAnimation != null)
                    currentAnimation.updateToAttribute(toAngle.getValue().toString());
            }
        });
        toAngleLabel = new JLabel(toAngleSettingsLabel);

        xAxisSkew = new JRadioButton(xAxisSkewLabel);
        xAxisSkew.addActionListener(e -> {
            if (currentAnimation != null) {
                if (xAxisSkew.isSelected()) {
                    currentAnimation.updateTransformType(TransformType.skewX);
                } else {
                    currentAnimation.updateTransformType(TransformType.skewY);
                }
            }
        });

        yAxisSkew = new JRadioButton(yAxisSkewLabel);
        yAxisSkew.addActionListener(e -> {
            if (currentAnimation != null) {
                if (yAxisSkew.isSelected()) {
                    currentAnimation.updateTransformType(TransformType.skewY);
                } else {
                    currentAnimation.updateTransformType(TransformType.skewX);
                }
            }
        });

        axisSettings = new ButtonGroup();
        axisSettings.add(xAxisSkew);
        axisSettings.add(yAxisSkew);

        angleSettingsPanel = new JPanel();
        angleSettingsPanel.setBorder(BorderFactory.createTitledBorder("Angle settings"));

        JPanel axisChooser = new JPanel();
        axisChooser.add(xAxisSkew);
        axisChooser.add(yAxisSkew);


        JPanel angleChooser = new JPanel();
        angleChooser.setLayout(new BoxLayout(angleChooser, BoxLayout.PAGE_AXIS));

        angleChooser.add(axisChooser);

        JPanel fromAnglePanel = new JPanel();
        fromAnglePanel.add(fromAngleLabel);
        fromAnglePanel.add(fromAngle);
        JPanel toAnglePanel = new JPanel();
        toAnglePanel.add(toAngleLabel);
        toAnglePanel.add(toAngle);

        angleChooser.add(fromAnglePanel);
        angleChooser.add(toAnglePanel);

        angleSettingsPanel.add(angleChooser);
    }

    public IAnimated getAnimation() {
        SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
        Document document = currentHandle.getCanvas().getDocument();

        //soubory ktere jsou vygenerovane editorem nemaji predponu svg:
        SVGAnimateTransformElement animateTransformElement = new SVGOMAnimateTransformElement(null, (AbstractDocument) document);
        animateTransformElement.setAttribute("attributeName", "transform");
        if (xAxisSkew.isSelected()) {
            animateTransformElement.setAttribute("type", "skewX");
        } else {
            animateTransformElement.setAttribute("type", "skewY");
        }

        animateTransformElement.setAttribute("from", fromAngle.getValue().toString());
        animateTransformElement.setAttribute("to", toAngle.getValue().toString());

        animateTransformElement.setAttribute("repeatCount", repeatSettings.getSelectedValue());
        animateTransformElement.setAttribute("begin", durationSettings.getBegin() + "s");
        animateTransformElement.setAttribute("dur", durationSettings.getDuration() + "s");
        animateTransformElement.setAttribute("fill", "freeze");
        animateTransformElement.setAttribute("predefined", "Skew");

        IAnimated animated = new Animation(selectedElement, (SVGOMAnimationElement) animateTransformElement);
        return animated;
    }
}
