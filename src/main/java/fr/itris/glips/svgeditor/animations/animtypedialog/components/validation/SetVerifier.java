package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import fr.itris.glips.svgeditor.Editor;
import org.apache.batik.anim.values.AnimatableValue;
import org.apache.batik.bridge.SVGAnimationEngine;
import org.apache.batik.bridge.SVGSetElementBridge;
import org.apache.batik.dom.svg.SVGOMSetElement;

/**
 * Created by Radek Frydrysek on 29.01.2016.
 */
public class SetVerifier extends AnimationValidator {
    private SetValidator validator;

    public SetVerifier(SVGOMSetElement animationElement, String validatingValue, String attributeLocalName) {
        super(animationElement, validatingValue, attributeLocalName);
        validator = new SetValidator(this);
    }

    @Override
    protected boolean validate() {
        if (validator == null) {
            validator = new SetValidator(this);
        }
        return validator.validate();
    }

    private class SetValidator extends SVGSetElementBridge {
        public SetValidator(AnimationValidator validator) {
            this.element = validator.element;
            this.targetElement = validator.targetElement;
            this.attributeLocalName = validator.attributeLocalName;
            this.animationTarget = validator.animationTarget;
        }

        private boolean validate() {
            this.eng = (SVGAnimationEngine) Editor.getEditor().getCurrentAnimationEngine();
            AnimatableValue animatableVal;
            try {
                if (this.animationType == 1 || this.element.hasProperty(attributeLocalName)) {//css
                    return cssAttributeValidator.isValid();
                }
                animatableVal = this.parseAnimatableValue(validatingAttributeName.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
            return (animatableVal != null);
        }
    }
}
