package fr.itris.glips.svgeditor.animations.animtypedialog.predefined;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AbstractAnimationDialog;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DialogHeader;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DurationSettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.OpacitySettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.RepeatSettings;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.svg.SVGOMAnimateElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimateElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 25.09.2015.
 */
public class FadeInOut extends AbstractAnimationDialog {
    private RepeatSettings repeatSettings;
    private DurationSettings durationSettings;
    private OpacitySettings opacitySettings;

    //new animation
    public FadeInOut(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }

    public FadeInOut(IAnimated currentAnimation) {
        super(currentAnimation);
    }


    @Override
    public void initialize() {
        header = new DialogHeader("Fade in/out animations", "Animate fade of element");
        repeatSettings = new RepeatSettings(currentAnimation);
        durationSettings = new DurationSettings(currentAnimation);
        opacitySettings = new OpacitySettings(currentAnimation);
    }

    @Override
    public void setAnimation(IAnimated animationObject) {
        currentAnimation = animationObject;
        opacitySettings.updateAnimation(animationObject);
        durationSettings.updateAnimation(animationObject);
        repeatSettings.updateAnimation(animationObject);
    }

    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);

        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        //AttributeType
        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.HORIZONTAL;

        cons.gridwidth = 4;
        cons.gridheight = 3;
        cons.gridx = 0;
        cons.gridy = 0;
        controlsPanel.add(opacitySettings, cons);

        cons.gridwidth = 2;
        cons.gridx = 0;
        cons.gridy = 3;
        controlsPanel.add(repeatSettings, cons);

        cons.gridx = 2;
        cons.gridy = 3;
        controlsPanel.add(durationSettings, cons);

        cons.gridx = 0;
        cons.gridy = 4;
        cons.fill = GridBagConstraints.BOTH;
        cons.weighty = cons.weightx = 1.0;
        controlsPanel.add(new JPanel(), cons);

        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }

    public IAnimated getAnimation() {
        SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
        Document document = currentHandle.getCanvas().getDocument();

        SVGAnimateElement animateElement = new SVGOMAnimateElement(null, (AbstractDocument) document);
        animateElement.setAttribute("attributeName", "opacity");
        animateElement.setAttribute("attributeType", "CSS");
        animateElement.setAttribute("from", String.valueOf(opacitySettings.getBeginOpacity() / 100.0));
        animateElement.setAttribute("to", String.valueOf(opacitySettings.getEndOpacity() / 100.0));
        animateElement.setAttribute("begin", durationSettings.getBegin() + "s");
        animateElement.setAttribute("dur", durationSettings.getDuration() + "s");
        animateElement.setAttribute("repeatCount", repeatSettings.getSelectedValue());
        animateElement.setAttribute("fill", "freeze");
        animateElement.setAttribute("predefined", "FadeInOut");

        IAnimated animated = new Animation(selectedElement, (SVGOMAnimationElement) animateElement);
        return animated;
    }
}
