package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

/**
 * Created by Radek Frydrysek on 29.01.2016.
 */
public enum FromToAttribute {
    from,
    to,
    by
}
