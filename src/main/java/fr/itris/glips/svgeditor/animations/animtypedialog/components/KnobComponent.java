package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Created by Radek Frydrysek on 05.02.2016.
 */
public class KnobComponent extends JPanel {
    private final int radius = 25;

    private RotatingKnob knob;
    private JTextField degreesToggle;
    private JLabel degreesLabel;
    private RotateSettings rotateSettings;

    public KnobComponent(RotateSettings rotateSettings) {
        this.rotateSettings = rotateSettings;
        degreesToggle = new JTextField();
        degreesToggle.setPreferredSize(new Dimension(radius * 2, 21));
        degreesToggle.setHorizontalAlignment(JTextField.RIGHT);


        degreesLabel = new JLabel("°");
        JPanel degreesSettings = new JPanel();
        degreesSettings.add(degreesToggle);
        degreesSettings.add(degreesLabel);

        knob = new RotatingKnob(degreesToggle);
        degreesToggle.addActionListener(e -> {
            int temp;
            try {
                temp = Integer.parseInt(degreesToggle.getText());
            } catch (Exception ex) {
                return;
            }
            knob.setDegreesAngle(temp);
        });


        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(knob);
        add(degreesSettings);
    }

    public int getAngle() {
        return (int) knob.getDegAngle();
    }

    public void setAngle(int angle) {
        knob.setDegreesAngle(angle);
        degreesToggle.setText(String.valueOf(angle));
    }

    private class RotatingKnob extends JPanel {
        private final double defaultTheta = 0;
        private final int spotRadius = 5;
        private Line2D.Double toggleLine;
        private double theta;
        private int currentSector;
        private int rounds;
        private int prevSector;
        private GradientPaint knobColor = new GradientPaint(new Point2D.Double(radius / 2.0, 0), new Color(211, 211, 211), new Point2D.Double(radius / 2.0, radius), new Color(255, 255, 255));
        private Color spotColor = new Color(122, 122, 122);
        private JTextField degreesToggle;

        public RotatingKnob(JTextField degreesToggle) {
            super();
            this.setPreferredSize(new Dimension(2 * radius + 2, 2 * radius + 2));
            this.degreesToggle = degreesToggle;
            theta = defaultTheta;

            this.addMouseMotionListener(new MouseAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                    //update rounds
                    if (rounds == 0 && getDegAngle() > 360) {
                        rounds = (int) (getDegAngle() / 360);
                    }

                    //update previous sector index
                    if (prevSector != currentSector)
                        prevSector = currentSector;

                    //get center of toggle (absolute position)
                    Point2D centerPoint = new Point2D.Double(getLocationOnScreen().getX() + getWidth() / 2.0, getLocationOnScreen().getY() + getHeight() / 2.0);

                    //position of cursor according to toggle
                    Point2D mousePosAccToToggle = new Point2D.Double(e.getLocationOnScreen().getX() - centerPoint.getX(),
                            -1 * (e.getLocationOnScreen().getY() - centerPoint.getY()));

                    //angle in radians according to default position
                    theta = Math.atan2(mousePosAccToToggle.getY(), mousePosAccToToggle.getX());

                    //get current sector
                    currentSector = getQuadrant(theta);
                    if (prevSector == 4 && currentSector == 1) {
                        rounds++; //if previous sector was 4 and current is 1, then increase round
                    } else if (prevSector == 1 && currentSector == 4) {
                        if (rounds > 0)
                            rounds--;
                    }

                    //update box with text
                    RotatingKnob.this.degreesToggle.setText(String.valueOf((int) getDegAngle() + (rounds * 360)));
                    rotateSettings.setAngle(((int) getDegAngle() + (rounds * 360)));
                    repaint();
                }
            });
        }

        /**
         * gets quadrant of theta
         *
         * @param theta angle in radians
         * @return quadrant index (1-4), for axis returns -1
         */
        private int getQuadrant(double theta) {
            if (0 < theta && theta < Math.PI / 2.0) {
                return 1;
            } else if (Math.PI / 2.0 < theta && theta < Math.PI) {
                return 2;
            } else if (0 > theta && theta > -Math.PI / 2.0) {
                return 4;
            } else if (-Math.PI / 2.0 > theta && theta > -Math.PI) {
                return 3;
            }
            return -1;
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2 = (Graphics2D) g;
            Rectangle rect = g2.getClipBounds();

            //set antialising
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);

            //knob --------
            //background
            g2.setPaint(knobColor);
            Shape knob = new Ellipse2D.Double(rect.getCenterX() - radius, rect.getCenterY() - radius, 2 * radius, 2 * radius);
            g2.fill(knob);
            //stroke
            g2.setStroke(new BasicStroke(1));
            g2.setPaint(spotColor);

            g2.draw(knob);

            //knob center -------
            Shape knobCenter = new Ellipse2D.Double(knob.getBounds2D().getCenterX() - (spotRadius / 2.0), knob.getBounds2D().getCenterY() - (spotRadius / 2.0), spotRadius, spotRadius);
            g2.fill(knobCenter);
            g2.setPaint(spotColor);
            g2.draw(knobCenter);

            Point2D centerPoint = new Point2D.Double(knobCenter.getBounds2D().getCenterX() + (radius * Math.cos(theta)), knobCenter.getBounds2D().getCenterY() - (radius * Math.sin(theta)));
            g2.setColor(spotColor);

            toggleLine = new Line2D.Double(knobCenter.getBounds2D().getCenterX(), knobCenter.getBounds2D().getCenterY(), centerPoint.getX(), centerPoint.getY());

            g2.draw(toggleLine);
        }

        /**
         * @return degrees in 0-360 range
         */
        public double getDegAngle() {
            return ((theta) / Math.PI * 180) + ((theta) > 0 ? 0 : 360);
        }

        /**
         * sets degrees to toggle
         *
         * @param degrees degrees in 0-360 range
         */
        public void setDegreesAngle(int degrees) {
            theta = (degrees * Math.PI / 180.0);
            currentSector = getQuadrant(theta);
            rounds = degrees / 360;
            repaint();
        }
    }
}
