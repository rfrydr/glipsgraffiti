package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;

/**
 * Created by Radek Frydrysek on 8.5.2015.
 */
public class AnimationsMenu {
    private final String MENU_LABEL_PREFIX = "Menu_";
    /**
     * the list of the menu items
     */
    private LinkedList<JMenuItem> menuItems = new LinkedList<>();

    /**
     * the pop up menu
     */
    private JPopupMenu popUpMenu = null;

    /**
     * cleans this object
     *
     * @param cleanAll whether all the object should be cleant
     */
    public void clean(boolean cleanAll) {
        if (cleanAll) {
            menuItems.clear();
            popUpMenu = null;
        }
    }

    /**
     * builds the menu items
     */
    protected void buildMenuItems(ActionListener listener) {
        menuItems.clear();
        popUpMenu = null;

        JMenu basicSubM = new JMenu(getLabel("Basic"));
        basicSubM.add(new JMenuItem(getLabel("Change of scale"))).addActionListener(listener);
        basicSubM.add(new JMenuItem(getLabel("Rotate"))).addActionListener(listener);
        basicSubM.add(new JMenuItem(getLabel("Skew"))).addActionListener(listener);
        basicSubM.add(new JMenuItem(getLabel("Fade in/out"))).addActionListener(listener);

        menuItems.add(basicSubM);

        JMenu advancedSubM = new JMenu(getLabel("Advanced"));
        advancedSubM.add(new JMenuItem(getLabel("Set"))).addActionListener(listener);
        advancedSubM.add(new JMenuItem(getLabel("Animate"))).addActionListener(listener);
        advancedSubM.add(new JMenuItem(getLabel("AnimateColor"))).addActionListener(listener);
        advancedSubM.add(new JMenuItem(getLabel("AnimateTransform"))).addActionListener(listener);
        advancedSubM.add(new JMenuItem(getLabel("AnimateMotion"))).addActionListener(listener);
        menuItems.add(advancedSubM);
    }

    /**
     * returns the label corresponding to the provided id
     *
     * @param id an id
     * @return the label corresponding to the provided id
     */
    public String getLabel(String id) {

        String label = "";

        try {
            label = ResourcesManager.bundle.getString(MENU_LABEL_PREFIX + id);
        } catch (Exception ex) {
        }

        if (label == null || label.equals("")) {

            label = id;
        }

        return label;
    }

    /**
     * creates a new popup menu
     */
    public void createPopUpMenu(ActionListener listener) {

        buildMenuItems(listener);
        popUpMenu = new JPopupMenu();

        //filling the popup menu
        for (JMenuItem menuItem : menuItems) {

            popUpMenu.add(menuItem);
        }
    }

    /**
     * shows a pop up used to create an animation
     *
     * @param component     a component
     * @param mousePosition the position of the mouse
     */
    public void showPopupMenu(JComponent component, Point mousePosition) {

        //showing the pop up menu
        popUpMenu.show(component, mousePosition.x, mousePosition.y);
    }

    public void addListener(ActionListener actionListener) {
        for (JMenuItem subMenu : menuItems) {
            JMenu menu = ((JMenu) subMenu);
            for (int i = 0; i < menu.getItemCount(); i++) {
                menu.getItem(i).addActionListener(actionListener);
            }
        }
    }

}
