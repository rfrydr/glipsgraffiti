package fr.itris.glips.svgeditor.animations.animtimeline.components;

import fr.itris.glips.svgeditor.animations.animtimeline.AnimationTimeLine;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;


/**
 * Třída reprezentující vizuální tlačítko předtavující animovaný objekt v časové ose
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public class AnimatedShape extends JButton {

    private static final long serialVersionUID = 1349610000119058933L;
    public static Font font = new Font("Arial", Font.PLAIN, 10);
    private Element shape;

    public AnimatedShape(Element shape, int animationsCount) {
        super();
        this.shape = shape;
        setFont(font);
        String elementID = shape.getAttribute("id");
        if (elementID == null || elementID.trim().length() == 0) {
            elementID = shape.getLocalName();
        }
        setText(elementID);

        setPreferredSize(new Dimension(AnimationTimeLine.animationShapeWidth, AnimationTimeLine.animationHeight * animationsCount));

        setToolTipText(shape.getTagName());
    }

    public Element getShape() {
        return shape;
    }

//    private String getText(final Element element) {
//        return element.getTagName();
//    }

    public void updateParallelAnimations(int animationsCount) {
        setPreferredSize(new Dimension(AnimationTimeLine.animationShapeWidth, AnimationTimeLine.animationHeight * animationsCount));
        revalidate();
        repaint();
    }
}
