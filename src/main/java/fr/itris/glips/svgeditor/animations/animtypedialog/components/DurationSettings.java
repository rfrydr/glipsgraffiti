package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMSetElement;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Created by Radek Frydrysek on 27.11.2015.
 */
public class DurationSettings extends JPanel implements IComponent {

    private static String titleLbl, durationLbl, startLbl;

    static {
        titleLbl = ResourcesManager.bundle.getString("DurationSettingsTitle");
        durationLbl = ResourcesManager.bundle.getString("DurationSettingsDurationLbl");
        startLbl = ResourcesManager.bundle.getString("DurationSettingsStartLbl");
    }


    private final JLabel durationLabel;
    private final JLabel beginLabel;
    private final JSpinner begin;
    private final JSpinner duration;

    private IAnimated currentAnimation;
    private Set<ITimingChanged> timingChangedList;

    public DurationSettings(IAnimated currentAnimation) {
        this();
        this.currentAnimation = currentAnimation;
        //set animation has only begin - not duration
        if (!(currentAnimation.getAnimationElement() instanceof SVGOMSetElement)) {
            this.duration.setValue(currentAnimation.getDuration());
        } else {
            duration.setEnabled(false);
            durationLabel.setEnabled(false);
        }

        this.begin.setValue(currentAnimation.getDurationOffset());
    }

    private DurationSettings() {
        timingChangedList = new LinkedHashSet<>();
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(titleLbl));
        this.setPreferredSize(new Dimension(250, 85));
        durationLabel = new JLabel(durationLbl);
        beginLabel = new JLabel(startLbl);
        Dimension d = new Dimension(50, 25);
        SpinnerModel smS = new SpinnerNumberModel(0, 0, 1000, 1);
        SpinnerModel smS2 = new SpinnerNumberModel(0, 0, 1000, 1);
        duration = new JSpinner(smS);
        duration.setPreferredSize(d);
        begin = new JSpinner(smS2);
        begin.setPreferredSize(d);

        //Duration
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.EAST;
        cons.insets = new Insets(0, 10, 0, 10);
        this.add(durationLabel, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        this.add(duration, cons);

        //Begin
        cons.gridx = 0;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.EAST;
        cons.insets = new Insets(0, 10, 0, 10);
        this.add(beginLabel, cons);

        cons.gridx = 1;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.WEST;
        this.add(begin, cons);

        begin.addChangeListener(e -> {
            currentAnimation.updateDurationOffset(getBegin());
            if (timingChangedList != null && !timingChangedList.isEmpty())
                timingChangedList.stream().forEach(aR -> aR.BeginChanged());
        });

        duration.addChangeListener(e -> {
            currentAnimation.updateDuration(getDuration());
            if (timingChangedList != null && !timingChangedList.isEmpty())
                timingChangedList.stream().forEach(aR -> aR.DurationChanged());
        });
    }

    public void setDuration(double duration) {
        this.duration.setValue(duration);
    }

    public void setBegin(double begin) {
        this.begin.setValue(begin);
    }

    public void addChangeListener(ITimingChanged timingChanged) {
        if (timingChangedList == null) {
            timingChangedList = new LinkedHashSet<>();
        }
        timingChangedList.add(timingChanged);
    }

    public void removeChangeListener(ITimingChanged timingChanged) {
        if (timingChangedList != null)
            timingChangedList.remove(timingChanged);
    }

    public double getBegin() {
        return ((SpinnerNumberModel) begin.getModel()).getNumber().doubleValue();
    }

    public double getDuration() {
        return ((SpinnerNumberModel) duration.getModel()).getNumber().doubleValue();
    }

    @Override
    public void initComponents() {

    }

    @Override
    public void compose() {

    }

    @Override
    public IAnimated getCurrentAnimation() {
        return null;
    }

    @Override
    public void updateAnimation(IAnimated animation) {
        this.currentAnimation = animation;
        //set animation has only begin - not duration
        if (!(currentAnimation.getAnimationElement() instanceof SVGOMSetElement)) {
            this.duration.setValue(currentAnimation.getDuration());
        } else {
            duration.setEnabled(false);
            durationLabel.setEnabled(false);
        }

        this.begin.setValue(currentAnimation.getDurationOffset());
    }

    public interface ITimingChanged {
        void DurationChanged();

        void BeginChanged();
    }
}
