package fr.itris.glips.svgeditor.animations;

import fr.itris.glips.rtdaeditor.test.display.DialogTest;
import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.apache.batik.dom.util.DOMUtilities;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.GVTTreeRendererAdapter;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

/**
 * Created by Radek Frydrysek on 7.4.2015.
 */
public class AnimationsPanel implements DialogTest {
    public static AnimationController animationController;
    /**
     * the canvas container
     */
    private static JSVGCanvas canvasContainer;
    /**
     * the split pane
     */
    private JSplitPane splitPane;
    /**
     * the panel containing the controls
     */
    private AnimationsControlPanel controlsPanel;
    /**
     * the size of the screen
     */
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    /**
     * the bounds when the dialog is in normal mode
     */
    private Rectangle normalBounds = new Rectangle();
    /**
     * whether the dialog is in full screen mode or not
     */
    private boolean isInFullScreenMode = false;
    /**
     * the normal dialog
     */
    private JFrame frame = new JFrame();
    private AnimationController.IAnimationChangeEvent updateDocumentEvent;

    /**
     * the constructor of the class
     */
    public AnimationsPanel() {
    }

    /**
     * @return Returns the canvas container
     */
    public static JSVGCanvas getCanvasContainer() {

        return canvasContainer;
    }

    /**
     * initializes the dialog
     */
    public void initialize() {
        //this.mainDisplay = aMainDisplay;
        updateDocumentEvent = () -> updateDocument();
        //getting the labels
        ResourceBundle bundle = ResourcesManager.bundle;
        String titleLabel = bundle.getString("rtdaanim_test_dialogTitle");

        //creating the canvas container
        canvasContainer = new JSVGCanvas();
        animationController = new AnimationController(canvasContainer);
        animationController.addEvent(updateDocumentEvent);

        //creating the controls panel
        controlsPanel = new AnimationsControlPanel(this);

        //creating the split pane
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setResizeWeight(1);
        splitPane.setDividerLocation(0.8);
        splitPane.setOneTouchExpandable(true);
        splitPane.setTopComponent(canvasContainer);
        splitPane.setBottomComponent(controlsPanel);

        //the layout for this dialog content pane
        frame.getContentPane().setLayout(new BoxLayout(
                frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.setTitle(titleLabel);

        //the listener to the state of the window
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent evt) {
                animationController.removeEvent(updateDocumentEvent);
                refreshDialogState(false);
                frame.setVisible(false);
            }
        });

        //packing the dialogs
        frame.getContentPane().add(splitPane);
        frame.pack();

        //setting the size for this dialog
        Rectangle bounds = new Rectangle();

        bounds.width = 3 * screenSize.width / 4;
        bounds.height = 3 * screenSize.height / 4;
        bounds.x = (screenSize.width - bounds.width) / 2;
        bounds.y = (screenSize.height - bounds.height) / 2;

        normalBounds = bounds;
        frame.setBounds(normalBounds);
    }

    /**
     * @return whether the dialog is in the full screen mode
     */
    public boolean isInFullScreenMode() {
        return isInFullScreenMode;
    }

    /**
     * handles the full screen mode
     */
    protected void handleFullScreenMode() {

        frame.setVisible(false);

        if (!isInFullScreenMode) {

            frame.dispose();
            frame.setUndecorated(true);
            frame.setExtendedState(Frame.MAXIMIZED_BOTH);
            splitPane.validate();

            isInFullScreenMode = true;

        } else {

            frame.dispose();
            frame.setUndecorated(false);
            frame.setExtendedState(Frame.NORMAL);
            splitPane.validate();
            frame.setBounds(normalBounds);

            isInFullScreenMode = false;
        }

        frame.setVisible(true);
    }

    /**
     * refreshes the state of the dialog
     *
     * @param visible whether the dialog will be visible
     */
    public void refreshDialogState(boolean visible) {
        if (visible) {

            SVGHandle handle =
                    Editor.getEditor().getHandlesManager().getCurrentHandle();

            if (handle != null) {
                canvasContainer.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
                canvasContainer.setDoubleBufferedRendering(true);
                SVGOMDocument doc = (SVGOMDocument) DOMUtilities.deepCloneDocument(handle.getCanvas().getDocument(), handle.getCanvas().getDocument().getImplementation());
                AnimationController.normalize(doc);
                canvasContainer.setDocument(doc);
                canvasContainer.addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
                    @Override
                    public void gvtRenderingCompleted(GVTTreeRendererEvent e) {
                        controlsPanel.refreshSimulationValuesPanel();
                    }
                });
                frame.setVisible(true);
            }
        } else {

            frame.setVisible(false);
        }
    }

    private void updateDocument() {
        SVGHandle handle =
                Editor.getEditor().getHandlesManager().getCurrentHandle();

        if (handle != null) {
            canvasContainer.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
            canvasContainer.setDoubleBufferedRendering(true);
            SVGOMDocument doc = (SVGOMDocument) DOMUtilities.deepCloneDocument(handle.getCanvas().getDocument(), handle.getCanvas().getDocument().getImplementation());
            AnimationController.normalize(doc);
            canvasContainer.setDocument(doc);
        }
    }

    @Override
    public void refreshSimulationValuesPanel() {

    }

    /**
     * @return the normal dialog
     */
    public JFrame getFrame() {
        return frame;
    }
}
