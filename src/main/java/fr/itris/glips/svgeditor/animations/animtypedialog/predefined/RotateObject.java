package fr.itris.glips.svgeditor.animations.animtypedialog.predefined;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AbstractAnimationDialog;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.*;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimateTransformElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 27.11.2015.
 */
public class RotateObject extends AbstractAnimationDialog {
    private static final String fromRotateSettingsLabel;
    private static final String toRotateSettingsLabel;
    private static final String dialogHeader;
    private static final String dialogSubHeader;

    static {
        fromRotateSettingsLabel = ResourcesManager.bundle.getString("RotateDialogFromRotateSettings");
        toRotateSettingsLabel = ResourcesManager.bundle.getString("RotateDialogToRotateSettings");
        dialogHeader = ResourcesManager.bundle.getString("RotateDialogDialogHeader");
        dialogSubHeader = ResourcesManager.bundle.getString("RotateDialogDialogSubHeader");
    }

    private DurationSettings durationSettings;
    private RepeatSettings repeatSettings;
    private RotateSettings fromRotateSettings;
    private RotateSettings toRotateSettings;

    public RotateObject(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    public RotateObject(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateTransformElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }

    @Override
    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);

        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.BOTH;

        cons.gridx = 0;
        cons.gridy = 0;
        controlsPanel.add(durationSettings, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        controlsPanel.add(repeatSettings, cons);

        cons.gridx = 0;
        cons.gridy = 1;
        controlsPanel.add(fromRotateSettings, cons);
        fromRotateSettings.setBorder(BorderFactory.createTitledBorder(fromRotateSettingsLabel));

        cons.gridx = 1;
        cons.gridy = 1;
        controlsPanel.add(toRotateSettings, cons);
        toRotateSettings.setBorder(BorderFactory.createTitledBorder(toRotateSettingsLabel));

        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }

    @Override
    public void initialize() {
        header = new DialogHeader(dialogHeader, dialogSubHeader);
        durationSettings = new DurationSettings(currentAnimation);
        repeatSettings = new RepeatSettings(currentAnimation);
        fromRotateSettings = new RotateSettings(currentAnimation);
        fromRotateSettings.setRotateSettingsChanged(new IRotateSettingsChanged() {
            @Override
            public void rotateSettingsChanged(int angle, Point rotatingPoint) {
                currentAnimation.updateFromAttribute(angle + " " + rotatingPoint.getX() + " " + rotatingPoint.getY());
            }
        });
        toRotateSettings = new RotateSettings(currentAnimation);
        toRotateSettings.setRotateSettingsChanged(new IRotateSettingsChanged() {
            @Override
            public void rotateSettingsChanged(int angle, Point rotatingPoint) {
                currentAnimation.updateToAttribute(angle + " " + rotatingPoint.getX() + " " + rotatingPoint.getY());
            }
        });
    }

    @Override
    public IAnimated getAnimation() {
        SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
        Document document = currentHandle.getCanvas().getDocument();

        //soubory ktere jsou vygenerovane editorem nemaji predponu svg:
        SVGAnimateTransformElement animateTransformElement = new SVGOMAnimateTransformElement(null, (AbstractDocument) document);
        animateTransformElement.setAttribute("attributeName", "transform");
        animateTransformElement.setAttribute("type", "rotate");

        Point fromRotatePoint = fromRotateSettings.getRotatePoint();
        Point toRotatePoint = toRotateSettings.getRotatePoint();
        int rotateFrom = fromRotateSettings.getAngle();
        int rotateTo = toRotateSettings.getAngle();
        animateTransformElement.setAttribute("from", rotateFrom + " " + fromRotatePoint.getX() + " " + fromRotatePoint.getY());
        animateTransformElement.setAttribute("to", rotateTo + " " + toRotatePoint.getX() + " " + toRotatePoint.getY());
        animateTransformElement.setAttribute("repeatCount", repeatSettings.getSelectedValue());
        animateTransformElement.setAttribute("begin", durationSettings.getBegin() + "s");
        animateTransformElement.setAttribute("dur", durationSettings.getDuration() + "s");
        animateTransformElement.setAttribute("fill", "freeze");
        animateTransformElement.setAttribute("predefined", "Rotation");

        IAnimated animated = new Animation(selectedElement, (SVGOMAnimationElement) animateTransformElement);
        return animated;
    }

    @Override
    public void setAnimation(IAnimated animationObject) {
        currentAnimation = animationObject;
        durationSettings.updateAnimation(animationObject);
        repeatSettings.updateAnimation(animationObject);
        fromRotateSettings.updateAnimation(getAngleFromAttr(animationObject.getFromAttribute()), getPointFromAttr(animationObject.getFromAttribute()));
        toRotateSettings.updateAnimation(getAngleFromAttr(animationObject.getToAttribute()), getPointFromAttr(animationObject.getToAttribute()));
    }

    private int getAngleFromAttr(String attrVal) {
        try {
            String[] vals = attrVal.split(" ");
            return (int) Double.parseDouble(vals[0]);
        } catch (Exception ex) {
            return 0;
        }
    }

    private Point getPointFromAttr(String attrVal) {
        try {
            String[] vals = attrVal.split(" ");
            double x = Double.parseDouble(vals[1]);
            double y = Double.parseDouble(vals[2]);
            return new Point((int) x, (int) y);
        } catch (Exception ex) {
            return new Point(0, 0);
        }
    }
}
