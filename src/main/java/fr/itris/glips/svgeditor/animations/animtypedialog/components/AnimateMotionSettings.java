package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AnimateMotionRotate;
import fr.itris.glips.svgeditor.animations.animtypedialog.Util;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.ext.awt.geom.ExtendedGeneralPath;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Radek Frydrysek on 18.01.2016.
 */
public class AnimateMotionSettings extends JPanel implements IComponent {
    private static String titleLbl, rotateLbl, currentPathLbl;

    static {
        titleLbl = ResourcesManager.bundle.getString("AnimateMotionSettingsTitle");
        rotateLbl = ResourcesManager.bundle.getString("AnimateMotionSettingsRotateLbl");
        currentPathLbl = ResourcesManager.bundle.getString("AnimateMotionSettingsCurrentPathLbl");
    }

    private JComboBox<String> rotate;
    private JLabel rotateLabel;

    private JTextArea currentPath;
    private JLabel currentPathLabel;

    private PathSettings pathSettings;

    private IAnimated currentAnimation;

    public AnimateMotionSettings(IAnimated currentAnimation) {
        this();
        this.currentAnimation = currentAnimation;
        rotate.setSelectedItem(currentAnimation.getAnimateMotionRotate());
        currentPath.setText(currentAnimation.getAnimateMotionPath());
    }

    public AnimateMotionSettings() {
        initComponents();
        compose();
        setListeners();
    }

    public void initComponents() {
        rotate = new JComboBox<>(Util.enumToComboboxModel(AnimateMotionRotate.class));
        pathSettings = new PathSettings();
        rotateLabel = new JLabel(rotateLbl);
        currentPath = new JTextArea();
        currentPath.setEnabled(false);
        currentPath.setPreferredSize(new Dimension(181, 21));
        currentPath.setMaximumSize(new Dimension(181, 21));
        currentPath.setSize(new Dimension(181, 21));
        currentPathLabel = new JLabel(currentPathLbl);
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(titleLbl));
        this.setPreferredSize(new Dimension(250, 120));
    }

    public void compose() {
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.EAST;
        cons.insets = new Insets(0, 10, 0, 10);
        this.add(rotateLabel, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        this.add(rotate, cons);

        cons.gridx = 0;
        cons.gridy = 2;
        cons.gridwidth = 2;
        cons.anchor = GridBagConstraints.EAST;
        cons.insets = new Insets(0, 10, 0, 10);
        this.add(pathSettings, cons);

        JScrollPane scrollPane = new JScrollPane(currentPath);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setPreferredSize(new Dimension(300, 40));
        scrollPane.setMinimumSize(new Dimension(20, 40));
        cons.gridx = 0;
        cons.gridy = 3;
        cons.gridheight = 2;
        cons.gridwidth = 2;
        cons.fill = GridBagConstraints.BOTH;
        this.add(scrollPane, cons);

        this.revalidate();
    }

    @Override
    public IAnimated getCurrentAnimation() {
        return null;
    }

    @Override
    public void updateAnimation(IAnimated animation) {
        this.currentAnimation = animation;
        rotate.setSelectedItem(currentAnimation.getAnimateMotionRotate());
        currentPath.setText(currentAnimation.getAnimateMotionPath());
    }

    private void setListeners() {
        rotate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentAnimation.updateAnimateMotionRotate(Enum.valueOf(AnimateMotionRotate.class, (String) rotate.getSelectedItem()));
            }
        });
        pathSettings.addPathCreatedListener(new PathSettings.IPathCreated() {
            @Override
            public void pathCreated(ExtendedGeneralPath pathShape, String pathString) {
                currentAnimation.updateAnimateMotionPath(pathString);
            }
        });
    }

}
