package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.RepeatType;
import fr.itris.glips.svgeditor.animations.animtypedialog.Util;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 24.10.2015.
 */
public class RepeatSettings extends JPanel implements IComponent {
    private static String titleLbl, repeatTypeLbl, repeatCountLbl;

    static {
        titleLbl = ResourcesManager.bundle.getString("RepeatSettingsTitle");
        repeatTypeLbl = ResourcesManager.bundle.getString("RepeatSettingsRepeatTypeLbl");
        repeatCountLbl = ResourcesManager.bundle.getString("RepeatSettingsRepeatCountLbl");
    }


    private JLabel repeatTypeLabel;
    private JLabel repeatCountLabel;
    private JComboBox<String> repeatType;
    private JSpinner repeatCount;

    private IAnimated currentAnimation;

    private Set<RepeatSettingsEvent> repeatChangeList;

    public RepeatSettings(IAnimated currentAnimation) {
        this();
        this.currentAnimation = currentAnimation;
        this.repeatCount.setValue(currentAnimation.getRepeatCount());
        this.repeatType.setSelectedItem(currentAnimation.getRepeatType());
    }

    private RepeatSettings() {
        initComponents();
        compose();
        setListeners();
    }

    public void initComponents() {
        repeatChangeList = new LinkedHashSet<>();
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(titleLbl));
        this.setPreferredSize(new Dimension(250, 85));
        repeatTypeLabel = new JLabel(repeatTypeLbl);
        repeatCountLabel = new JLabel(repeatCountLbl);
        repeatType = new JComboBox<>(Util.enumToComboboxModel(RepeatType.class));
        SpinnerModel spinnerModel = new SpinnerNumberModel(0, 0, 1000, 1);
        Dimension dimension = new Dimension(50, 25);
        repeatCount = new JSpinner(spinnerModel);
        repeatCount.setPreferredSize(dimension);
    }

    public void compose() {
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.EAST;
        cons.insets = new Insets(0, 10, 0, 10);
        this.add(repeatTypeLabel, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        this.add(repeatType, cons);

        cons.gridx = 0;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.EAST;
        cons.insets = new Insets(0, 10, 0, 10);
        this.add(repeatCountLabel, cons);

        cons.gridx = 1;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.WEST;
        this.add(repeatCount, cons);

        this.revalidate();
    }

    @Override
    public IAnimated getCurrentAnimation() {
        return null;
    }

    @Override
    public void updateAnimation(IAnimated animation) {
        this.currentAnimation = animation;
        this.repeatCount.setValue(currentAnimation.getRepeatCount());
        this.repeatType.setSelectedItem(currentAnimation.getRepeatType().toString());
        if (getSelectedType() == RepeatType.definite) {
            repeatCount.setVisible(true);
            repeatCountLabel.setVisible(true);
        } else {
            repeatCount.setVisible(false);
            repeatCountLabel.setVisible(false);
        }
    }

    private void setListeners() {
        repeatType.addItemListener(e -> {
            if (getSelectedType() == RepeatType.definite) {
                repeatCount.setVisible(true);
                repeatCountLabel.setVisible(true);
            } else {
                repeatCount.setVisible(false);
                repeatCountLabel.setVisible(false);
            }
            currentAnimation.updateRepeatType(getSelectedType());
            if (repeatChangeList != null && !repeatChangeList.isEmpty())
                repeatChangeList.stream().forEach(aR -> aR.repeatTypeChanged());
        });

        repeatCount.addChangeListener(e -> {
            currentAnimation.updateRepeatCount(getRepeatCount());
            if (repeatChangeList != null && !repeatChangeList.isEmpty())
                repeatChangeList.stream().forEach(aR -> aR.repeatCountChanged());
        });
    }

    public void addChangeListener(RepeatSettingsEvent aListener) {
        repeatChangeList.add(aListener);
    }

    public void removeActionListener(RepeatSettingsEvent aListener) {
        repeatChangeList.remove(aListener);
    }

    public int getRepeatCount() {
        return ((SpinnerNumberModel) repeatCount.getModel()).getNumber().intValue();
    }

    public RepeatType getSelectedType() {
        return Enum.valueOf(RepeatType.class, (String) repeatType.getSelectedItem());
    }

    public void setRepeatCount(double repeatCount) {
        this.repeatCount.setValue(repeatCount);
    }

    public void setSelectedType(RepeatType repeatType) {
        this.repeatType.setSelectedItem(repeatType);
    }

    public interface RepeatSettingsEvent {
        void repeatTypeChanged();

        void repeatCountChanged();
    }

    public String getSelectedValue() {
        if (repeatType.getSelectedItem().equals(RepeatType.definite.toString())) {
            return repeatCount.getValue().toString();
        }
        return repeatType.getSelectedItem().toString();
    }
}
