package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import javax.swing.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 19.12.2015.
 */
public class ValidationResultComponent extends JPanel {

    private Set<ValidationItem> validationItems = new LinkedHashSet<>();

    public ValidationResultComponent(FromToVerifier verifier) {
        verifier.addChangeListener((isValid, errorMsg, helpMsg) -> {
            if (!isValid) {
                validationItems.add(new ValidationItem(isValid, errorMsg, helpMsg));
                if (validationItems.size() > 3) {
                    validationItems.remove(validationItems.iterator().next());
                }
            } else {
                validationItems.clear();
            }
            update();
        });
    }

    private void update() {
        this.removeAll();
        if (validationItems.size() != 0) {
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            validationItems.forEach(this::add);
        }
        revalidate();
    }

    private class ValidationItem extends JPanel {
        private JLabel messageLbl;
        private JLabel subMessageLbl;
        private Icon icon;

        public ValidationItem(boolean isError, String message, String subMessage) {
            if (isError) {
                icon = UIManager.getIcon("OptionPane.errorIcon");
            } else {
                icon = UIManager.getIcon("OptionPane.warningIcon");
            }
            messageLbl = new JLabel(message);
            subMessageLbl = new JLabel(subMessage);

            compose();
        }

        private void compose() {
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            messageLbl.setIcon(icon);
            add(messageLbl);
            if (subMessageLbl.getText().length() > 0)
                add(subMessageLbl);
        }

        //comparing text of labels
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ValidationItem)) return false;

            ValidationItem that = (ValidationItem) o;

            if (!messageLbl.getText().equals(that.messageLbl.getText())) return false;
            return subMessageLbl.getText().equals(that.subMessageLbl.getText());

        }

        //hashing text of labels
        @Override
        public int hashCode() {
            int result = messageLbl.getText().hashCode();
            result = 31 * result + subMessageLbl.getText().hashCode();
            return result;
        }
    }
}
