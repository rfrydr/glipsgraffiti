package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import fr.itris.glips.svgeditor.animations.animtypedialog.TransformType;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

/**
 * Created by Radek Frydrysek on 30.01.2016.
 */
public class TransformValidValuesHelper {
    /**
     * init error message
     * static - initialize only once
     * */
    static {
        translateValueError = ResourcesManager.bundle.getString("ErrorTranslateValue");
        scaleValueError = ResourcesManager.bundle.getString("ErrorScaleValue");
        rotateValueError = ResourcesManager.bundle.getString("ErrorRotateValue");
        skewxError = ResourcesManager.bundle.getString("ErrorSkewxValue");
        skewyError = ResourcesManager.bundle.getString("ErrorSkewyValue");

        translateDefaultValue = ResourcesManager.bundle.getString("DefaultValueTranslate");
        scaleDefaultValue = ResourcesManager.bundle.getString("DefaultValueScale");
        rotateDefaultValue = ResourcesManager.bundle.getString("DefaultValueRotate");
        skewxDefaultValue = ResourcesManager.bundle.getString("DefaultValueSkewx");
        skewyDefaultValue = ResourcesManager.bundle.getString("DefaultValueSkewy");

        defaultValueMessage = ResourcesManager.bundle.getString("MessageValidValue");
    }

    //error messages
    private static String translateValueError;
    private static String scaleValueError;
    private static String rotateValueError;
    private static String skewxError;
    private static String skewyError;

    //default values
    private static String translateDefaultValue;
    private static String scaleDefaultValue;
    private static String rotateDefaultValue;
    private static String skewxDefaultValue;
    private static String skewyDefaultValue;


    private static String defaultValueMessage;

    /**
     * @param transformType type of attribute (XML attribute)
     * @return default value corresponding to type of attribute
     */
    public static String getDefaultValue(TransformType transformType) {
        String defaultValue = "";
        switch (transformType) {
            case translate:
                defaultValue = translateDefaultValue;
                break;
            case scale:
                defaultValue = scaleDefaultValue;
                break;
            case rotate:
                defaultValue = rotateDefaultValue;
                break;
            case skewX:
                defaultValue = skewxDefaultValue;
                break;
            case skewY:
                defaultValue = skewyDefaultValue;
                break;
        }
        if (!defaultValue.trim().isEmpty()) {
            defaultValue = defaultValueMessage + " " + defaultValue;
        }
        return defaultValue;
    }

    /**
     * @param transformType type of attribute (XML attribute)
     * @return message error corresponding to type of attribute
     */
    public static String getErrorMessage(TransformType transformType) {
        String message = "";
        switch (transformType) {
            case translate:
                message = translateValueError;
                break;
            case scale:
                message = scaleValueError;
                break;
            case rotate:
                message = rotateValueError;
                break;
            case skewX:
                message = skewxError;
                break;
            case skewY:
                message = skewyError;
                break;
        }
        return message;
    }
}
