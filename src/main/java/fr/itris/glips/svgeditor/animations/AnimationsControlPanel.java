package fr.itris.glips.svgeditor.animations;

import fr.itris.glips.rtdaeditor.test.display.table.TestTableBuilder;
import fr.itris.glips.svgeditor.animations.animtimeline.AbstractAnimationTimeLine;
import fr.itris.glips.svgeditor.animations.animtimeline.AnimationTimeLine;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.bridge.SVGAnimationEngine;
import org.apache.batik.bridge.UpdateManager;
import org.apache.batik.dom.svg.SVGOMDocument;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

/**
 * Created by Radek Frydrysek on 7.4.2015.
 */
public class AnimationsControlPanel extends JPanel {
    public static int zoomValue;

    /**
     * the main display
     */
    //private MainDisplay mainDisplay;
    /**
     * the test dialog
     */
    private AnimationsPanel animationsPanel;
    /**
     * the builder for the table allowing to set the
     * values of the test simulator
     */
    private TestTableBuilder tableBuilder;
    /**
     * the toolbar containing the buttons
     */
    private JToolBar controlsToolbar;

    //private JSlider zoomSlider;
    /**
     * the panel containing the table used to enter
     * the values for the simulation
     */
    //private JPanel simulationValuesPanel;
    //private SVGAnimationEngine animationEngine;
    //private BridgeContext ctx;
    private AbstractAnimationTimeLine animTimeLine;
    private boolean zoomLock;

    /**
     * the constructor of the class
     *
     * @param animationsPanel the test dialog
     */
    public AnimationsControlPanel(final AnimationsPanel animationsPanel) {

        this.animationsPanel = animationsPanel;
        /*ctx = animationsPanel.getBridgeContext();
        animationEngine = ctx.getAnimationEngine();*/

        /*animationsPanel.getCanvasContainer().addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
            @Override
            public void gvtRenderingCompleted(GVTTreeRendererEvent e) {
                animationEngine = Editor.getAnimationHandler().getAnimationEngine();
                refreshSimulationValuesPanel();
            }
        });*/

        this.tableBuilder = new TestTableBuilder();
        build();
    }

    /**
     * builds the controls panel
     */
    protected void build() {

        createToolbar();
        createSimulationValuesPanel();

        //filling the panel
        setLayout(new GridBagLayout());
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.NORTH;
        cons.fill = GridBagConstraints.HORIZONTAL;
        add(controlsToolbar, cons);
        cons.gridx = 0;
        cons.gridy = 1;
        cons.weighty = cons.weightx = 1.0;
        cons.anchor = GridBagConstraints.SOUTH;
        cons.fill = GridBagConstraints.BOTH;
        add(animTimeLine, cons);
        refreshSimulationValuesPanel();
    }

    /**
     * creates the toolbar
     */
    protected void createToolbar() {

        //getting the labels
        ResourceBundle bundle = ResourcesManager.bundle;

        zoomValue = 5;
//        zoomSlider = new JSlider();
//
//        zoomSlider.setAlignmentX(0.0F);
//        zoomSlider.setAlignmentY(0.0F);
//        zoomSlider.setPreferredSize(new java.awt.Dimension(120, 12));
//        zoomSlider.setMaximumSize(zoomSlider.getPreferredSize());
//        zoomSlider.setMinimum(1);
//        zoomSlider.setMaximum(10);
//        zoomSlider.setValue(4);
//        zoomSlider.addChangeListener(new ChangeListener() {
//            @Override
//            public void stateChanged(ChangeEvent arg0) {
//                //notifyChange(zoomSlider);
//                //JSlider zoom = (JSlider)slider;
//                int zoomVal = zoomSlider.getValue();
//                animTimeLine.setZoom(zoomVal);
//            }
//        });

//        gridBagConstraints = new java.awt.GridBagConstraints();
//        gridBagConstraints.gridx = 7;
//        gridBagConstraints.gridy = 2;
//        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
//        add(zoomSlider, gridBagConstraints);

        String beginLabel = bundle.getString("rtdaanim_test_begin");
        String playLabel = bundle.getString("rtdaanim_test_play");
        String pauseLabel = bundle.getString("rtdaanim_test_pause");
        String stopLabel = bundle.getString("rtdaanim_test_stop");
        String endLabel = bundle.getString("rtdaanim_test_end");
        String exitLabel = bundle.getString("rtdaanim_test_exit");
//        String zoomLabel = bundle.getString("rtdaanim_test_zoom");
        String zoomInLabel = bundle.getString("ZoomInItemLabel");
        String zoomOutLabel = bundle.getString("ZoomOutItemLabel");
        final String displayFullScreenMode = bundle.getString("rtdaanim_test_fullscreen");
        final String hideFullScreenMode = bundle.getString("rtdaanim_test_unfullscreen");
        String zoomStatusLabel = bundle.getString("rtdaanim_test_zoom_status");

        //the control buttons
        final JButton beginButton = new JButton();
        final JButton playButton = new JButton();
        final JButton pauseButton = new JButton();
        final JButton stopButton = new JButton();
        final JButton endButton = new JButton();
        final JButton exitButton = new JButton();
        //final JLabel zoomCaption = new JLabel(zoomLabel);
        final JButton zoomInButton = new JButton();
        final JButton zoomOutButton = new JButton();
        JLabel zoomLabel = new JLabel(String.format(zoomStatusLabel, zoomValue * 10));

        final JToggleButton displayFullScreenModeButton = new JToggleButton();
        //final JToggleButton displayInvalidMarkersButton = new JToggleButton();

        //the ok action name
        String actionName = "f11Action";

        //registering the ok action
        Action f11Action = new AbstractAction(actionName) {

            public void actionPerformed(ActionEvent e) {

                if (animationsPanel.getFrame().isVisible()) {

                    displayFullScreenModeButton.doClick();
                }
            }
        };

        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0), actionName);
        getActionMap().put(actionName, f11Action);

        //getting the icons for the buttons
        ImageIcon beginIcon = ResourcesManager.getIcon("Begin", false);
        ImageIcon playIcon = ResourcesManager.getIcon("Play", false);
        ImageIcon pauseIcon = ResourcesManager.getIcon("Pause", false);
        ImageIcon stopIcon = ResourcesManager.getIcon("Stop", false);
        ImageIcon endIcon = ResourcesManager.getIcon("End", false);
        ImageIcon exitIcon = ResourcesManager.getIcon("Exit", false);
        ImageIcon fullScreenIcon = ResourcesManager.getIcon("FullScreen", false);
        ImageIcon zoomInIcon = ResourcesManager.getIcon("ZoomIn", false);
        ImageIcon zoomOutIcon = ResourcesManager.getIcon("ZoomOut", false);

        //setting the properties for the buttons
        beginButton.setIcon(beginIcon);
        playButton.setIcon(playIcon);
        pauseButton.setIcon(pauseIcon);
        stopButton.setIcon(stopIcon);
        endButton.setIcon(endIcon);
        exitButton.setIcon(exitIcon);
        displayFullScreenModeButton.setIcon(fullScreenIcon);
        zoomInButton.setIcon(zoomInIcon);
        zoomOutButton.setIcon(zoomOutIcon);
//        displayInvalidMarkersButton.setIcon(displayInvalidMarkersIcon);
//        displayInvalidMarkersButton.setDisabledIcon(displayInvalidMarkersIconDisabled);
//        displayInvalidMarkersButton.setIcon(displayInvalidMarkersIcon);

        beginButton.setToolTipText(beginLabel);
        playButton.setToolTipText(playLabel);
        pauseButton.setToolTipText(pauseLabel);
        stopButton.setToolTipText(stopLabel);
        endButton.setToolTipText(endLabel);
        exitButton.setToolTipText(exitLabel);
        displayFullScreenModeButton.setToolTipText(displayFullScreenMode);
        zoomInButton.setToolTipText(zoomInLabel);
        zoomOutButton.setToolTipText(zoomOutLabel);

//        displayInvalidMarkersButton.setSelected(false);
//        displayInvalidMarkersButton.setToolTipText(displayInvalidMarkersLabel);

        //handles the state of the full screen button
        displayFullScreenModeButton.setSelected(animationsPanel.isInFullScreenMode());

        playButton.setEnabled(true);
        pauseButton.setEnabled(true);
        stopButton.setEnabled(true);

        AnimationsPanel.animationController.addListener(new AnimationController.AnimationListener() {
            @Override
            public void animationStopped() {
                //stopButton.setEnabled(false);
                //playButton.setEnabled(true);
            }
        });

        beginButton.addActionListener(evt -> {
            final SVGAnimationEngine animationEngine = AnimationController.getAnimationEngine();
            if (animationEngine != null) {
                UpdateManager um = AnimationsPanel.getCanvasContainer().getUpdateManager();
                um.getUpdateRunnableQueue().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        animationEngine.setCurrentTime(0);
                        animationEngine.pause();
                    }
                });
            }
        });

        //setting the listeners to the buttons
        playButton.addActionListener(evt -> {
            final SVGAnimationEngine animationEngine = AnimationController.getAnimationEngine();
            if (animationEngine != null) {
                UpdateManager um = AnimationsPanel.getCanvasContainer().getUpdateManager();
                um.getUpdateRunnableQueue().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (!animationEngine.hasStarted()) {
                            animationEngine.start(0);
                            //playButton.setEnabled(false);
                        } else if (animationEngine.isPaused()) {
                            animationEngine.unpause();
                            //pauseButton.setEnabled(true);
                            //playButton.setEnabled(false);
                        }
                    }
                });

            }
        });

        pauseButton.addActionListener(evt -> {
            final SVGAnimationEngine animationEngine = AnimationController.getAnimationEngine();
            if (animationEngine != null) {
                if (animationEngine.hasStarted() &&
                        !animationEngine.isPaused()) {
                    UpdateManager um = AnimationsPanel.getCanvasContainer().getUpdateManager();
                    um.getUpdateRunnableQueue().invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            animationEngine.pause();
                        }
                    });
                    //pauseButton.setEnabled(false);
                    //playButton.setEnabled(true);
                }
            }
        });

        stopButton.addActionListener(evt -> {
            final SVGAnimationEngine animationEngine = AnimationController.getAnimationEngine();
            if (animationEngine != null) {
                UpdateManager um = AnimationsPanel.getCanvasContainer().getUpdateManager();
                um.getUpdateRunnableQueue().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        animationEngine.setCurrentTime(0);
                        animationEngine.pause();
                    }
                });
                if (!pauseButton.isEnabled()) {
                    //pauseButton.setEnabled(true);
                }
                //playButton.setEnabled(true);
            }
        });

        endButton.addActionListener(evt -> {
            final SVGAnimationEngine animationEngine = AnimationController.getAnimationEngine();
            if (animationEngine != null) {
                animationEngine.pause();
                UpdateManager um = AnimationsPanel.getCanvasContainer().getUpdateManager();
                um.getUpdateRunnableQueue().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        animationEngine.setCurrentTime((float) AnimationController.getTotalTime((SVGOMDocument) AnimationsPanel.getCanvasContainer().getSVGDocument(), um.getBridgeContext()));
                        animationEngine.pause();
                    }
                });
            }
        });

        zoomInButton.addActionListener(e -> {
            if (zoomValue < 10 && !zoomOutButton.getModel().isPressed()) {
                zoomValue++;
                animTimeLine.setZoom(zoomValue);
                zoomLabel.setText(String.format(zoomStatusLabel, zoomValue * 10));

                //disable in
                if (zoomValue == 10) {
                    zoomInButton.setEnabled(false);
                    zoomInButton.setIcon(ResourcesManager.getIcon("ZoomIn", true));
                }
                //enable out
                if (!zoomOutButton.isEnabled()) {
                    zoomOutButton.setEnabled(true);
                    zoomOutButton.setIcon(ResourcesManager.getIcon("ZoomOut", false));
                }
            }


        });

        zoomOutButton.addActionListener(e -> {
            if (zoomValue > 1 && !zoomInButton.getModel().isPressed()) {
                zoomValue--;
                animTimeLine.setZoom(zoomValue);
                zoomLabel.setText(String.format(zoomStatusLabel, zoomValue * 10));

                //disable out
                if (zoomValue == 1) {
                    zoomOutButton.setEnabled(false);
                    zoomOutButton.setIcon(ResourcesManager.getIcon("ZoomOut", true));
                }
                //enable in
                if (!zoomInButton.isEnabled()) {
                    zoomInButton.setEnabled(true);
                    zoomInButton.setIcon(ResourcesManager.getIcon("ZoomIn", false));
                }
            }
        });
        exitButton.addActionListener(evt -> animationsPanel.refreshDialogState(false));

        displayFullScreenModeButton.addActionListener(evt -> {

            if (!animationsPanel.isInFullScreenMode()) {

                displayFullScreenModeButton.setToolTipText(hideFullScreenMode);

            } else {

                displayFullScreenModeButton.setToolTipText(displayFullScreenMode);
            }

            animationsPanel.handleFullScreenMode();
        });

        //creating the controls toolbar
        controlsToolbar = new JToolBar(SwingConstants.HORIZONTAL);
        controlsToolbar.setMaximumSize(new Dimension(1080, 32));
        controlsToolbar.setFloatable(false);
        controlsToolbar.setBorderPainted(false);

        controlsToolbar.add(beginButton);
        controlsToolbar.add(playButton);
        controlsToolbar.add(pauseButton);
        controlsToolbar.add(stopButton);
        controlsToolbar.add(endButton);
        controlsToolbar.addSeparator();
        controlsToolbar.add(displayFullScreenModeButton);
        controlsToolbar.addSeparator();
        controlsToolbar.add(exitButton);
        controlsToolbar.add(Box.createHorizontalGlue());
        //controlsToolbar.add(zoomCaption);
        //controlsToolbar.add(zoomSlider);
        controlsToolbar.add(zoomLabel);
        controlsToolbar.addSeparator();
        controlsToolbar.add(zoomInButton);
        controlsToolbar.add(zoomOutButton);
    }

    /**
     * creates the simulation values panel
     */
    protected void createSimulationValuesPanel() {

        //simulationValuesPanel = tableBuilder.getComponent();
        //AbstractAnimationTimeLine animTimeLine=

        animTimeLine = new AnimationTimeLine(this);
        animTimeLine.setBackground(new Color(216, 236, 241));
        animTimeLine.setZoom(zoomValue);
//        simulationValuesPanel = animTimeLine;
    }

    /**
     * refreshes the simulation values panel that will be displayed
     * to allow the users to specify the values they want for the simulation
     */
    public void refreshSimulationValuesPanel() {
        //tableBuilder.disposeTable();
        animTimeLine.refresh();
    }

    /**
     * @return the table builder
     */
    public TestTableBuilder getTableBuilder() {
        return tableBuilder;
    }


}
