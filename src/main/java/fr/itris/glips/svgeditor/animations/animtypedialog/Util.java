package fr.itris.glips.svgeditor.animations.animtypedialog;

import javax.swing.*;
import java.util.Arrays;

/**
 * Created by Radek Frydrysek on 24.10.2015.
 */
public class Util {
    public static DefaultComboBoxModel enumToComboboxModel(Class<? extends Enum<?>> e) {
        return new DefaultComboBoxModel(Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new));
    }
}
