package fr.itris.glips.svgeditor.animations.animtimeline.components;


import javax.swing.*;
import java.awt.*;

public class __TimeLineRuler extends JPanel {

    private static final long serialVersionUID = 5305187476843764926L;

    public void paintComponent(Graphics g) {
        if (g == null)
            return;

        int selectedFrame = 12;//MainDialog.getDialog().getAnimationController().getActualFrame();
        //MainDialog.getDialog().getAnimationController();
        int frameCount = 10;//AnimationController.getMaxFrame();
        Dimension d = getSize();
        Graphics2D g2 = (Graphics2D) g;
        Color temp = g.getColor();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, d.width, d.height);

        g.setColor(Color.BLACK);
        int framesize = 12;//(AnimationTimeLine.animationWidthOnSecond / AnimationController.getFps());
        int x = 0;
        for (int i = 0; i < frameCount; i++) {
            g2.drawLine(x, 0, x, d.height);
            x += framesize;
        }

        g2.setColor(Color.BLUE);
        g2.drawLine((selectedFrame) * framesize, 0, (selectedFrame) * framesize, d.height);

        g2.setColor(temp);
    }

    public void update(Graphics g) {
        super.update(g);
        paintComponent(g);
    }

    public int getActualFrame(Point pt) {
//        MainDialog.getDialog().getAnimationController();
//        int frame = pt.x / (AnimationTimeLine.animationWidthOnSecond / AnimationController.getFps());
//        return frame;
        return 1;
    }
}
