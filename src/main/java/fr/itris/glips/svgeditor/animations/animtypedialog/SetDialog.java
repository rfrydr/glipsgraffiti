package fr.itris.glips.svgeditor.animations.animtypedialog;

import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.*;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.apache.batik.dom.svg.SVGOMSetElement;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 22.08.2015.
 */
public class SetDialog extends AbstractAnimationDialog {
    static {
        tagName = ResourcesManager.bundle.getString("SetDialogTagName");
        description = ResourcesManager.bundle.getString("SetDialogDescription");
    }

    private static String tagName;
    private static String description;

    private FromToSettings fromToSettings;
    private DurationSettings durationSettings;
    private AttributeSettings attributeSettings;

    public SetDialog(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    public SetDialog(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMSetElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }

    @Override
    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);


        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());
        GridBagConstraints cons = new GridBagConstraints();


        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.HORIZONTAL;

        cons.gridx = 0;
        cons.gridy = 0;
        controlsPanel.add(fromToSettings, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        controlsPanel.add(attributeSettings, cons);

        cons.gridx = 0;
        cons.gridy = 1;
        controlsPanel.add(durationSettings, cons);

        cons.gridx = 0;
        cons.gridy = 2;
        cons.fill = GridBagConstraints.BOTH;
        cons.weighty = cons.weightx = 1.0;
        controlsPanel.add(new JPanel(), cons);

        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }

    @Override
    public void setAnimation(IAnimated animationObject) {
        if (animationObject != null) {
            currentAnimation = animationObject;
            if (currentAnimation.getAttributeName() == null && currentAnimation.getAttributeType() == null) {
                currentAnimation.updateAttributeName(attributeSettings.getAttributeName());
                currentAnimation.updateAttributeType(attributeSettings.getAttributeType());
            }

            attributeSettings.updateAnimation(currentAnimation);
            fromToSettings.updateAnimation(currentAnimation);
            durationSettings.updateAnimation(currentAnimation);
        }
    }

    @Override
    public void initialize() {
        attributeSettings = new AttributeSettings(currentAnimation);
        fromToSettings = new FromToSettings(currentAnimation, attributeSettings);
        durationSettings = new DurationSettings(currentAnimation);
        header = new DialogHeader(tagName, description);
        fromToSettings.getInputVerifier().addChangeListener(new FromToVerifier.IValidationCompleted() {
            @Override
            public void InputValidated(boolean isValid, String errorMsg, String defaultValidValue) {
                if (isValid) {
                    header.hideError();
                    header.setLabels(tagName, "", description);

                } else {
                    header.showError("", errorMsg + "\n" + defaultValidValue);
                }
            }
        });
    }
}
