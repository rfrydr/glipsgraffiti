package fr.itris.glips.svgeditor.animations.animtimeline.components;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.AnimationController;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtimeline.AbstractAnimationTimeLine;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Třída reprezentující celou vnitřní komponentu časové osy
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public class AnimationTimeLineComponent extends JPanel {
    public static final Color first_color = new Color(216, 236, 241);
    public static final Color second_color = new Color(238, 248, 248);
    private static final long serialVersionUID = 15554883132165654L;
    private AbstractAnimationTimeLine parent;
    private int rowsCount = 0;
    private int height;

    public AnimationTimeLineComponent(AbstractAnimationTimeLine parent) {
        super();
        this.parent = parent;
        setBackground(new Color(238, 248, 248));
        initComponents();
    }

    private void initComponents() {
        GridBagLayout gl = new GridBagLayout();
        setLayout(gl);
    }

    public void repaint() {
        super.repaint();
        height = 0;
        removeAll();
        GridBagConstraints c;

        int rownum = 0;
        boolean first = true;
        SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
        if (currentHandle == null) return;

        BridgeContext ctx = null;
        SVGOMDocument currentDoc = null;
        HashMap<Element, List<IAnimated>> animationCrateHashMap = new HashMap<>();
        if (currentHandle != null) {
            currentDoc = (SVGOMDocument) currentHandle.getCanvas().getDocument();
            ctx = currentHandle.getCanvas().getBridgeContext();
            animationCrateHashMap = AnimationController.getAnimations(currentDoc, ctx);
        }

        for (Map.Entry<Element, List<IAnimated>> entry : animationCrateHashMap.entrySet()) {
            AnimatedShape s = new AnimatedShape(entry.getKey(), entry.getValue().size());

            c = new GridBagConstraints();
            c.gridx = 0;
            c.weightx = 0;
            c.gridy = rownum;
            c.anchor = GridBagConstraints.NORTHWEST;
            c.fill = GridBagConstraints.NONE;
            add(s, c);

            AnimationLine panel = new AnimationLine(s, this);
            if (first)
                panel.setBackground(first_color);
            else
                panel.setBackground(second_color);
            first = !first;
            entry.getValue().stream().forEach(aR -> panel.add(aR));
            height = height + panel.getHeight();
            c = new GridBagConstraints();
            c.gridx = 1;
            c.weightx = 1;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.anchor = GridBagConstraints.NORTHWEST;
            add(panel, c);
            c.gridy = rownum;

            rownum++;
        }
        rowsCount = rownum;
        revalidate();
    }

    public AbstractAnimationTimeLine getParent() {
        return parent;
    }

    public Dimension getSize() {
        return new Dimension(this.getWidth(), height);
    }
}
