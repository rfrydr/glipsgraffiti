package fr.itris.glips.svgeditor.animations.animtimeline.components;

import fr.itris.glips.svgeditor.animations.AnimationsControlPanel;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtimeline.AnimationTimeLine;

import javax.swing.*;
import java.awt.*;

/**
 * Třída reprezentující jeden řádek v časové ose animace
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public class AnimationLine extends JPanel {
    private static final long serialVersionUID = 1L;
    private AnimationTimeLineComponent parent;
    private AnimatedShape correspondingShape;
    private int height;

    public AnimationLine(AnimatedShape correspondingShape, AnimationTimeLineComponent parent) {
        super();
        this.correspondingShape = correspondingShape;
        this.parent = parent;
        setLayout(null);
    }

    public void add(IAnimated animation) {
        double start = animation.getDurationOffset();
        double duration = animation.getDuration();

        Rectangle rect = new Rectangle(
                (int) (AnimationTimeLine.defaultFrameWidth * AnimationsControlPanel.zoomValue * start),
                0,
                (int) (AnimationTimeLine.defaultFrameWidth * AnimationsControlPanel.zoomValue * duration),
                AnimationTimeLine.animationHeight);

        Dimension dim = new Dimension(getWidth(), getHeight());

        for (Component component : getComponents()) {
            if (component instanceof AnimationItem) {
                AnimationItem animace = (AnimationItem) component;
                if (animace.animation == animation)
                    continue;

                if (component.getBounds().intersects(rect)) {
                    rect.y += AnimationTimeLine.animationHeight;
                }
            }
        }

        AnimationItem item = new AnimationItem(animation);
        item.setBounds(rect);
        item.setBackground(new Color(177, 217, 228));
        add(item);
        item.addShapeChangedEvent(() -> parent.repaint());
        for (Component component : getComponents()) {
            if (component instanceof AnimationItem) {
                AnimationItem animationItem = (AnimationItem) component;
                Rectangle r = animationItem.getBounds();

                if ((r.getWidth() + r.getX()) > dim.getWidth())
                    dim.width = (int) (r.getWidth() + r.getX());
                if ((r.getHeight() + r.getY()) > dim.getHeight())
                    dim.height = (int) (r.getHeight() + r.getY());
            }
        }
        setPreferredSize(dim);
        height = dim.height;
        correspondingShape.updateParallelAnimations(dim.height / AnimationTimeLine.animationHeight);
    }

    @Override
    public int getHeight() {
        return height;
    }
}
