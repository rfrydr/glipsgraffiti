package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.svgeditor.EditorToolkit;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 1.5.2015.
 */
public class AnimationsHeader extends JPanel {
    /**
     * the label displaying the type of the selected node
     */
    private JLabel selectedNodeTypeLbl = new JLabel();

    /**
     * the description label
     */
    private String descriptionLabel = "", noHandleOpenLabel = "";

    /**
     * the test launch button
     */
    private JButton testLaunchButton;

    /**
     * the separator
     */
    private JSeparator separator = new JSeparator();

    /**
     * the constructor of the class
     *
     * @param testLaunchButton the button used to launch the test dialog
     */
    public AnimationsHeader(JButton testLaunchButton) {

        //this.animationsModule = animationsModule;
        this.testLaunchButton = testLaunchButton;

        descriptionLabel = ResourcesManager.bundle.getString("rtdaanim_headerElementName");
        noHandleOpenLabel = ResourcesManager.bundle.getString("rtdaanim_noHandleOpen");

        //setting the properties of the jlabel
        selectedNodeTypeLbl.setHorizontalAlignment(SwingConstants.LEFT);
        selectedNodeTypeLbl.setBorder(new EmptyBorder(0, 5, 0, 0));

        //building the panel
        setLayout(new BorderLayout(5, 2));
        setBorder(new EmptyBorder(2, 2, 2, 2));

        add(selectedNodeTypeLbl, BorderLayout.CENTER);
        add(testLaunchButton, BorderLayout.EAST);
        add(separator, BorderLayout.SOUTH);
    }

    /**
     * sets the currently edited element
     *
     * @param element an element
     */
    public void setCurrentElement(Element element) {

        separator.setVisible(false);
        testLaunchButton.setVisible(false);

        if (element != null) {

            selectedNodeTypeLbl.setText("<html><body>" + descriptionLabel +
                    " <b>" + getLabel(element) + "</b></body></html>");
            separator.setVisible(true);
            testLaunchButton.setVisible(true);

        } else {

            selectedNodeTypeLbl.setText(noHandleOpenLabel);
        }
    }

    /**
     * returns the label corresponding to the given element
     *
     * @param element an element
     * @return the label corresponding to the given element
     */
    public String getLabel(Element element) {

        String label = "";

        if (element != null) {
            label = EditorToolkit.getElementLabel(element);
        }

        return label;
    }
}
