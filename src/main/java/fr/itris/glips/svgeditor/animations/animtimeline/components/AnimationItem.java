package fr.itris.glips.svgeditor.animations.animtimeline.components;

import fr.itris.glips.svgeditor.animations.AnimationsControlPanel;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtimeline.AnimationTimeLine;
import org.apache.batik.dom.util.DOMUtilities;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.swing.*;
import java.awt.*;


/**
 * Třída reprezentující animaci v časové ose jako tlačítko
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public class AnimationItem extends MovableResizableButton {
    private static final long serialVersionUID = -3611245132450412091L;

    public IAnimated animation;


    public AnimationItem(IAnimated animation) {
        //TODO localized animation name
        super(animation.getAnimationElement().getLocalName());
        this.animation = animation;

        setMargin(new Insets(0, 0, 0, 0));
        setMinimumSize(new Dimension(25, 25));
        setToolTipText(getText(animation.getAnimationElement()));
        setHorizontalAlignment(SwingConstants.LEFT);
        setVerticalAlignment(SwingConstants.TOP);

        addShapeChangedEvent(() -> {
            double dur = getWidth() / AnimationsControlPanel.zoomValue / AnimationTimeLine.defaultFrameWidth;
            double durOffset = getX() / AnimationsControlPanel.zoomValue / AnimationTimeLine.defaultFrameWidth;

            //TODO update values
            if (dur != animation.getDuration())
                animation.updateDuration(dur);

            if (durOffset != animation.getDurationOffset())
                animation.updateDurationOffset(durOffset);
        });


    }

    public static void removeChilds(Node node) {
        while (node.hasChildNodes())
            node.removeChild(node.getFirstChild());
    }

    public void update(Graphics g) {
        paintComponent(g);
    }

    private String getText(final Element element) {
        Element resElement = element;
        removeChilds(resElement);
        return DOMUtilities.getXML(resElement);
    }
}
