package fr.itris.glips.svgeditor.animations.animtypedialog.predefined;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AbstractAnimationDialog;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DialogHeader;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.DurationSettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.RepeatSettings;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.ScaleSettings;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimateTransformElement;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 04.10.2015.
 */
public class ChangeOfScale extends AbstractAnimationDialog {
    private DurationSettings durationSettings;
    private RepeatSettings repeatSettings;
    private ScaleSettings scaleSettings;


    public ChangeOfScale(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    public ChangeOfScale(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateTransformElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }

    @Override
    public void initialize() {
        header = new DialogHeader("Change of scale animation", "Animate scale of element");
        durationSettings = new DurationSettings(currentAnimation);
        repeatSettings = new RepeatSettings(currentAnimation);
        scaleSettings = new ScaleSettings(currentAnimation);
    }


    @Override
    public void setAnimation(IAnimated animationObject) {
        currentAnimation = animationObject;
        scaleSettings.updateAnimation(animationObject);
        repeatSettings.updateAnimation(animationObject);
        durationSettings.updateAnimation(animationObject);
    }

    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);


        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        //AttributeType
        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.BOTH;

        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 4;
        cons.gridheight = 3;


        controlsPanel.add(scaleSettings, cons);
        cons.weighty = cons.weightx = 0.1;
        //From
        cons.gridwidth = 2;
        cons.gridheight = 2;
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.gridx = 0;
        cons.gridy = 3;

        controlsPanel.add(repeatSettings, cons);

        cons.gridx = 2;
        cons.gridy = 3;

        controlsPanel.add(durationSettings, cons);


        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }

    public IAnimated getAnimation() {
        SVGHandle currentHandle = Editor.getEditor().getHandlesManager().getCurrentHandle();
        Document document = currentHandle.getCanvas().getDocument();

        //soubory ktere jsou vygenerovane editorem nemaji predponu svg:
        SVGAnimateTransformElement animateTransformElement = new SVGOMAnimateTransformElement(null, (AbstractDocument) document);
        animateTransformElement.setAttribute("attributeName", "transform");
        animateTransformElement.setAttribute("type", "scale");
        if (scaleSettings.isProportionalModeSelected()) {
            animateTransformElement.setAttribute("from", scaleSettings.getBeginScale() + " " + scaleSettings.getBeginScale());
            animateTransformElement.setAttribute("to", scaleSettings.getEndScale() + " " + scaleSettings.getEndScale());
        } else {
            animateTransformElement.setAttribute("from", scaleSettings.getBeginScaleX() + " " + scaleSettings.getBeginScaleY());
            animateTransformElement.setAttribute("to", scaleSettings.getEndScaleX() + " " + scaleSettings.getEndScaleY());
        }
        animateTransformElement.setAttribute("repeatCount", repeatSettings.getSelectedValue());
        animateTransformElement.setAttribute("begin", durationSettings.getBegin() + "s");
        animateTransformElement.setAttribute("dur", durationSettings.getDuration() + "s");
        animateTransformElement.setAttribute("fill", "freeze");
        animateTransformElement.setAttribute("predefined", "ChangeOfScale");

        IAnimated animated = new Animation(selectedElement, (SVGOMAnimationElement) animateTransformElement);
        return animated;
    }


}
