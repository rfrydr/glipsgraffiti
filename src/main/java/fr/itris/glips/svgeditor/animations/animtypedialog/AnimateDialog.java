package fr.itris.glips.svgeditor.animations.animtypedialog;

import fr.itris.glips.svgeditor.animations.animdialog.Animation;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.*;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMAnimateElement;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 22.08.2015.
 */
public class AnimateDialog extends AbstractAnimationDialog {

    static {
        tagName = ResourcesManager.bundle.getString("AnimateDialogTagName");
        description = ResourcesManager.bundle.getString("AnimateDialogDescription");
    }

    private static String tagName;
    private static String description;

    private DurationSettings durationSettings;
    private RepeatSettings repeatSettings;
    private AttributeSettings attributeSettings;
    private FromToSettings fromToSettings;

    public AnimateDialog(IAnimated currentAnimation) {
        super(currentAnimation);
    }

    //new animation
    public AnimateDialog(Element selectedElement) {
        super(selectedElement, new Animation(selectedElement, new SVGOMAnimateElement(null, (SVGOMDocument) selectedElement.getOwnerDocument())));
    }


    @Override
    public void setAnimation(IAnimated animationObject) {
        if (animationObject != null) {
            currentAnimation = animationObject;
            if (currentAnimation.getAttributeName().isEmpty() && currentAnimation.getAttributeType() == null) {
                currentAnimation.updateAttributeName(attributeSettings.getAttributeName());
                currentAnimation.updateAttributeType(attributeSettings.getAttributeType());
            }

            durationSettings.updateAnimation(currentAnimation);
            repeatSettings.updateAnimation(currentAnimation);
            attributeSettings.updateAnimation(currentAnimation);
            fromToSettings.updateAnimation(currentAnimation);
        }
    }

    @Override
    public void initialize() {
//components
        repeatSettings = new RepeatSettings(currentAnimation);
        durationSettings = new DurationSettings(currentAnimation);
        attributeSettings = new AttributeSettings(currentAnimation);
        fromToSettings = new FromToSettings(currentAnimation, attributeSettings);
        header = new DialogHeader(tagName, description);
        fromToSettings.getInputVerifier().addChangeListener(new FromToVerifier.IValidationCompleted() {
            @Override
            public void InputValidated(boolean isValid, String errorMsg, String defaultValidValue) {
                if (isValid) {
                    header.hideError();
                    header.setLabels(tagName, "", description);

                } else {
                    header.showError("", errorMsg + "\n" + defaultValidValue);
                }
            }
        });
    }

    public void compose() {
        setLayout(new BorderLayout());
        add(header, BorderLayout.PAGE_START);


        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());

        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.FIRST_LINE_START;
        cons.weighty = cons.weightx = 0.1;
        cons.fill = GridBagConstraints.HORIZONTAL;

//        cons.gridx = 0;
//        cons.gridy = 0;
//        cons.gridwidth = 2;
//        cons.weightx = 1.0;
//        add(header);


        cons.gridx = 0;
        cons.gridy = 1;
        cons.gridwidth = 1;
        controlsPanel.add(attributeSettings, cons);

        cons.gridx = 1;
        cons.gridy = 1;
        controlsPanel.add(fromToSettings, cons);


        cons.gridx = 0;
        cons.gridy = 2;
        controlsPanel.add(durationSettings, cons);


        cons.gridx = 1;
        cons.gridy = 2;
        controlsPanel.add(repeatSettings, cons);

        cons.gridx = 0;
        cons.gridy = 3;
        cons.fill = GridBagConstraints.BOTH;
        cons.weighty = cons.weightx = 1.0;
        controlsPanel.add(new JPanel(), cons);

        add(controlsPanel, BorderLayout.CENTER);

        revalidate();
    }
}
