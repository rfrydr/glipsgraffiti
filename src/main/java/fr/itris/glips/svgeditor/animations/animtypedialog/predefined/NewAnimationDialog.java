package fr.itris.glips.svgeditor.animations.animtypedialog.predefined;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AbstractAnimationDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Radek Frydrysek on 03.10.2015.
 */
public class NewAnimationDialog extends JPanel {
    private JPanel topPanel;
    private JPanel bottomPanel;

    private AbstractAnimationDialog animationDialog;
    private IAnimated animation;

    private JButton okButton;
    private JButton cancelButton;

    private List<ActionListener> listenerList = new ArrayList();

    public NewAnimationDialog(AbstractAnimationDialog animationDialog) {
        this.animationDialog = animationDialog;
        initComponents();
        compose();
    }

    private void initComponents() {
        okButton = new JButton("Create animation");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                animation = animationDialog.getAnimation();
                for (ActionListener listener : listenerList)
                    listener.actionPerformed(new ActionEvent(this, 0, NewAnimationAction.ok.toString()));
            }
        });

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (ActionListener listener : listenerList)
                    listener.actionPerformed(new ActionEvent(this, 0, NewAnimationAction.cancel.toString()));
            }
        });

        topPanel = animationDialog;
        bottomPanel = new JPanel();
    }

    public void addButtonsListener(ActionListener actionListener) {
        listenerList.add(actionListener);
    }

    public IAnimated getAnimation() {
        return animation;
    }

    private void compose() {

        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        bottomPanel.add(Box.createHorizontalGlue());
        bottomPanel.add(cancelButton);
        bottomPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        bottomPanel.add(okButton);

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.add(topPanel, BorderLayout.NORTH);
        this.add(bottomPanel, BorderLayout.SOUTH);
    }

    public enum NewAnimationAction {
        ok,
        cancel
    }

}
