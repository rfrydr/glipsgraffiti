package fr.itris.glips.svgeditor.animations.animtimeline.components;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.AnimationController;
import fr.itris.glips.svgeditor.animations.AnimationsPanel;
import fr.itris.glips.svgeditor.animations.animtimeline.AnimationTimeLine;
import org.apache.batik.bridge.UpdateManagerAdapter;
import org.apache.batik.bridge.UpdateManagerEvent;
import org.apache.batik.dom.svg.SVGOMDocument;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * Třída reprezentující pravítko v časové ose
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public class AnimationRuler extends JPanel {
    private static final long serialVersionUID = 1L;
    //    private ActionFactoryTimeLine actionFactory;
//    private IAction action;
    //private static AnimationRuler instance;
    private double frame_size = 10;
    private int frame_height = 20;
    private double selectedFrame = 0.0;
    private JComponent parent;
    private float dur = 0;
    private double zoomLevel;

    public AnimationRuler(JComponent parent) {
        super();
        //instance = this;

        this.parent = parent;
        AnimationsPanel.animationController.addUpdateManagerListener(new UpdateManagerAdapter() {
            @Override
            public void updateCompleted(UpdateManagerEvent updateManagerEvent) {

                dur = (float) AnimationController.getTotalTime((SVGOMDocument) Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getDocument(),
                        Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getBridgeContext());

                float currentTime = AnimationsPanel.animationController.getCurrentTime();
                if (currentTime <= dur) {
                    setSelectedFrame(currentTime);
                } else {
                    setSelectedFrame(currentTime % dur);
                }
            }
        });

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point2D pt = e.getPoint();
                final double x = pt.getX();
                final float clickedTime = (float) (((x - AnimationTimeLine.animationShapeWidth) / (frame_size)));
                AnimationsPanel.animationController.setCurrentTime(clickedTime);
            }
        });

        setPreferredSize(new Dimension(0, frame_height));
    }

    public void setZoomLevel(double zoomLevel) {
        this.zoomLevel = zoomLevel;
        frame_size = AnimationTimeLine.defaultFrameWidth * zoomLevel;
        repaint();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (g == null) {
            return;
        }

        Rectangle clipRect = g.getClipBounds();
        double viewPortWidth = ((JScrollPane) parent).getViewport().getPreferredSize().getWidth();
        double rulerWidth = clipRect.width > viewPortWidth ? clipRect.width : viewPortWidth;
        Color SPACES_COLOR = new Color(238, 248, 248);
        g.setColor(SPACES_COLOR);
        g.fillRect(clipRect.x, 0, (int) rulerWidth, frame_height);

        int max_frame = (int) (rulerWidth / frame_size);

        g.setColor(new Color(0, 255, 0));
        int x = (int) Math.round(selectedFrame * frame_size);
        g.fillRect(150, 0, x, frame_height);
        g.setFont(new Font("SansSerif", Font.PLAIN, 10));
        g.setColor(Color.BLACK);
        for (int frame = 0; frame < max_frame; frame++) {
            if (zoomLevel > 3.0) {//kazdy ma popisek
                g.drawString(String.valueOf(frame), (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth) - (String.valueOf(frame).length() * 2), 7);
                g.drawLine((int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 8, (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 15);
            } else if (zoomLevel > 2.0) {
                if (frame % 2 == 0) {//kazdy druhy
                    g.drawLine((int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 0, (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 15);
                } else {
                    g.drawLine((int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 8, (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 15);
                    g.drawString(String.valueOf(frame), (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth) - (String.valueOf(frame).length() * 2), 7);
                }

            } else if (zoomLevel > 1.0) {
                if (frame % 5 == 0) {//kazdy paty
                    g.drawLine((int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 8, (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 15);
                    g.drawString(String.valueOf(frame), (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth) - (String.valueOf(frame).length() * 2), 7);
                } else {
                    g.drawLine((int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 0, (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 15);
                }
            } else {
                g.drawLine((int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 0, (int) (frame * frame_size + AnimationTimeLine.animationShapeWidth), 15);
            }
        }
    }

//    public double getSelectedFrame() {
//        return selectedFrame;
//    }

    public void setSelectedFrame(double frame) {
        selectedFrame = frame;
        repaint();
    }
}
