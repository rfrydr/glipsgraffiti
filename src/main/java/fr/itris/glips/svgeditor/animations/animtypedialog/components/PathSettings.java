package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.display.selection.Selection;
import fr.itris.glips.svgeditor.display.selection.SelectionItem;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import fr.itris.glips.svgeditor.shape.AbstractShape;
import fr.itris.glips.svgeditor.shape.path.PathCreatedListener;
import fr.itris.glips.svgeditor.shape.path.PathShape;
import fr.itris.glips.svgeditor.shape.path.SimplePathShape;
import org.apache.batik.dom.svg.SVGGraphicsElement;
import org.apache.batik.ext.awt.geom.ExtendedGeneralPath;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGRect;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 25.10.2015.
 */
public class PathSettings extends JPanel {
    private static String bezierCurveLbl, simpleLineLbl;
    private static ImageIcon lineIcon, bezierIcon;

    static {
        bezierCurveLbl = ResourcesManager.bundle.getString("PathSettingsBezierLbl");
        simpleLineLbl = ResourcesManager.bundle.getString("PathSettingsSimpleLineLbl");
        lineIcon = ResourcesManager.getIcon("SimplePathShape", false);
        bezierIcon = ResourcesManager.getIcon("PathShape", false);
    }


    private JButton bezierCurve;
    private JButton line;

    private Editor editor;

    private List<IPathCreated> pathCreatedListeners = new ArrayList<>();


    public PathSettings() {
        editor = Editor.getEditor();
        initComponents();
        compose();
        setListeners();
    }


    private void initComponents() {
        bezierCurve = new JButton(bezierCurveLbl);
        if (bezierIcon != null)
            bezierCurve.setIcon(bezierIcon);

        line = new JButton(simpleLineLbl);
        if (lineIcon != null)
            line.setIcon(lineIcon);
    }


    private void compose() {
        this.setLayout(new GridBagLayout());

        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.EAST;
        this.add(bezierCurve, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        this.add(line, cons);

        this.revalidate();
    }


    private void setListeners() {

        final SVGHandle currentHandle = editor.getHandlesManager().getCurrentHandle();
        final Selection currentSelection = currentHandle.getSelection();
        final Set<Element> currentSelectedElements = new HashSet<>(currentSelection.getSelectedElements());
        bezierCurve.addActionListener(e -> {
            drawPath(currentSelectedElements);
        });

        line.addActionListener(e -> {
            drawLine(currentSelectedElements);
        });
    }

    private void drawLine(Set<Element> currentSelectedElements) {
        final SimplePathShape pathShapeModule = new SimplePathShape(editor, false);

        startDrawing(currentSelectedElements, pathShapeModule);
    }

    private void startDrawing(final Set<Element> currentSelectedElements, final PathShape pathShapeModule) {

        pathShapeModule.setModuleMode(0);
        pathShapeModule.notifyDrawingMode();
        Point startDrawingPos = new Point();

        Set<SelectionItem> items = Editor.getEditor().getHandlesManager().getCurrentHandle().getSelection().getSelectionManager().getSelectionItems(
                Editor.getEditor().getHandlesManager().getCurrentHandle(),
                currentSelectedElements,
                Selection.SELECTION_LEVEL_1
        );
        //center of selected element
        if (items.size() > 0) {
            SVGRect selectedItemBounds = ((SVGGraphicsElement) items.iterator().next().getElements().iterator().next()).getBBox();

            startDrawingPos.setLocation(
                    selectedItemBounds.getX() + (selectedItemBounds.getWidth() / 2.0),
                    selectedItemBounds.getY() + (selectedItemBounds.getHeight() / 2.0)
            );
        } else { //...or center of current canvas
            startDrawingPos.setLocation(
                    Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getHeight() / 2.0,
                    Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getWidth() / 2.0
            );
        }

        pathShapeModule.notifyDrawingAction(
                editor.getHandlesManager().getCurrentHandle(),
                startDrawingPos,
                1,
                AbstractShape.DRAWING_MOUSE_PRESSED);

        pathShapeModule.addPathCreatedListener(new PathCreatedListener() {
            @Override
            public void pathCreated(ExtendedGeneralPath pathShape, String pathString) {
                if (!pathCreatedListeners.isEmpty())
                    pathCreatedListeners.stream().forEach(aR -> aR.pathCreated(pathShape, pathString));
            }
        });
    }

    private void drawPath(final Set<Element> currentSelectedElements) {
        final PathShape pathShapeModule = new PathShape(editor, false);

        startDrawing(currentSelectedElements, pathShapeModule);
    }

    public void addPathCreatedListener(IPathCreated pathCreated) {
        if (pathCreatedListeners == null)
            pathCreatedListeners = new ArrayList<>();

        pathCreatedListeners.add(pathCreated);
    }

    public void removePathCreatedListener(IPathCreated pathCreated) {
        if (pathCreatedListeners != null)
            pathCreatedListeners.remove(pathCreated);
    }

    public interface IPathCreated {
        void pathCreated(ExtendedGeneralPath pathShape, String pathString);
    }
}
