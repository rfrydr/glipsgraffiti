package fr.itris.glips.svgeditor.animations.animtimeline;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.ModuleAdapter;
import fr.itris.glips.svgeditor.actions.toolbar.ToolsFrame;
import fr.itris.glips.svgeditor.display.handle.HandlesListener;
import fr.itris.glips.svgeditor.display.handle.SVGHandle;
import fr.itris.glips.svgeditor.resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 26.02.2016.
 */
public class TimeLineModule extends ModuleAdapter {
    private ToolsFrame moduleFrame;
    private final String moduleId = "TimeLineModule";
    private final String frameTitle;

    private AbstractAnimationTimeLine timeLine;
    private JPanel controlsPanel = new JPanel();

    public TimeLineModule(final Editor editor) {
        frameTitle = ResourcesManager.bundle.getString("label_" + moduleId.toLowerCase());

        initGuiItems();
        composeGui();

        moduleFrame = new ToolsFrame(editor, moduleId, frameTitle, controlsPanel);
        moduleFrame.getMenuItem().setEnabled(false);
        moduleFrame.getToolBarButton().setEnabled(false);

        final HandlesListener svgHandleListener = new HandlesListener() {
            @Override
            public void handleChanged(SVGHandle currentHandle, Set<SVGHandle> handles) {
                if (handles.size() != 0) {
                    moduleFrame.getMenuItem().setEnabled(true);
                    moduleFrame.getToolBarButton().setEnabled(true);
                    timeLine.refresh();
                    controlsPanel.revalidate();
                    moduleFrame.revalidate();
                } else {
                    moduleFrame.getMenuItem().setEnabled(false);
                    moduleFrame.getToolBarButton().setEnabled(false);
                    timeLine.refresh();
                    controlsPanel.revalidate();
                    moduleFrame.revalidate();
                }
            }
        };
        moduleFrame.setVisibilityChangedRunnable(() -> timeLine.refresh());
        editor.getHandlesManager().addHandlesListener(svgHandleListener);
    }

    private void composeGui() {
        this.controlsPanel.setLayout(new BoxLayout(controlsPanel, BoxLayout.X_AXIS));
        this.controlsPanel.add(timeLine);

    }

    private void initGuiItems() {
        timeLine = new AnimationTimeLine(controlsPanel);
        timeLine.setBackground(new Color(216, 236, 241));
        timeLine.setZoom(5);
    }

    @Override
    public HashMap<String, JMenuItem> getMenuItems() {
        HashMap<String, JMenuItem> menuItems = new HashMap<>();
        menuItems.put("Menu_" + moduleId, moduleFrame.getMenuItem());
        return menuItems;
    }

    @Override
    public HashMap<String, AbstractButton> getToolItems() {
        HashMap<String, AbstractButton> map = new HashMap<>();
        map.put("Tool_" + moduleId, moduleFrame.getToolBarButton());
        return map;
    }
}
