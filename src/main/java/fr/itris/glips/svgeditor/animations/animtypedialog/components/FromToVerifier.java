package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.Editor;
import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AttributeType;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.validation.AbstractValidator;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.validation.AnimateTransformVerifier;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.validation.AnimateVerifier;
import fr.itris.glips.svgeditor.animations.animtypedialog.components.validation.SetVerifier;
import org.apache.batik.bridge.SVGAnimationEngine;
import org.apache.batik.dom.anim.AnimationTarget;
import org.apache.batik.dom.svg.SVGOMAnimateElement;
import org.apache.batik.dom.svg.SVGOMAnimateTransformElement;
import org.apache.batik.dom.svg.SVGOMSetElement;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Radek Frydrysek on 19.12.2015.
 */
public class FromToVerifier extends InputVerifier {
    public final static Color validColor = new Color(255, 255, 255);
    public final static Color invalidColor = new Color(249, 232, 232);

    private Set<IValidationCompleted> eventList = new LinkedHashSet<>();
    private SVGAnimationEngine animationEngine;
    private IAnimated currentAnimation;
    private AttributeSettings attributeSettings;

    public FromToVerifier(IComponent parentOfValidated, AttributeSettings attributeSettings) {
        //this(parentOfValidated);
        this.attributeSettings = attributeSettings;
        this.currentAnimation = parentOfValidated.getCurrentAnimation();
    }

//    public FromToVerifier(IComponent parentOfValidated) {
//
//    }


    @Override
    public boolean verify(JComponent input) {
        //pro neaktivni prvky budeme predpokladat vzdy validni vstup
        if (input == null) return false;
        if (!input.isEnabled()) return true;

        //String text = ((JTextField) input).getText();
        try {
            this.animationEngine = (SVGAnimationEngine) Editor.getEditor().getCurrentAnimationEngine();

            if (currentAnimation == null) return false;

            if (attributeSettings == null) {
                /*validace aktualnich hodnot (ne z animace)
                * do animace se zapisou az po validaci
                *
                * v pripade AnimateTransform se currentAnimation musi pouzit
                * */
                return isValid(input, currentAnimation, currentAnimation.getAttributeName(), animationEngine, currentAnimation.getAttributeName(), currentAnimation.getAttributeType());
            } else {
                return isValid(input, currentAnimation, attributeSettings.getAttributeName(), animationEngine, attributeSettings.getAttributeName(), attributeSettings.getAttributeType());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return raiseEvent(false, "Hodnota neni validni", "Neznama chyba");
        }
    }

    private boolean isValid(JComponent input, IAnimated currentAnimation, String atrName, SVGAnimationEngine currentAnimationEngine, String attributeLocalName, AttributeType attributeType) {
        String validatingValue = ((JTextField) input).getText();
        if (validatingValue.trim().isEmpty()) {
            input.setBackground(FromToVerifier.invalidColor);
            return false;
        }

        Element animationElement = currentAnimation.getAnimationElement();
        AnimationTarget animationTarget = (AnimationTarget) currentAnimation.getAnimatedElement();

        //pokud element nema atribut
        if (!atrName.equals("transform") && !animationTarget.getElement().hasAttributeNS(null, atrName)) {
            //pak zkusim css - style
            if (attributeType == AttributeType.CSS || attributeType == AttributeType.Auto) {
                //ma-li style, tak jej zkusime prohledat zda neobsahuje vybrany atribut
                if (animationTarget.getElement().hasAttributeNS(null, "style")) {
                    if (!animationTarget.getElement().getAttribute("style").contains(attributeLocalName)) {
                        input.setBackground(invalidColor);
                        return raiseEvent(false, "Hodnota neni validni", "Vybrany element nema atribut" + atrName);
                    }
                }
            } else {
                input.setBackground(invalidColor);
                return raiseEvent(false, "Hodnota neni validni", "Vybrany element nema atribut" + atrName);
            }
        }

//        IAnimationAttributeVerifier verifier = null;
//        if (currentAnimation.getAnimationElement() instanceof SVGOMAnimateElement) {
//            verifier = new AnimateVerifier((SVGOMAnimateElement) animationElement, attributeLocalName, validatingValue);
//        } else if (currentAnimation.getAnimationElement() instanceof SVGOMAnimateTransformElement) {
//            verifier = new AnimateTransformVerifier((SVGOMAnimateTransformElement) animationElement, validatingValue, TransformType.rotate);
//        } else if (currentAnimation.getAnimationElement() instanceof SVGOMSetElement) {
//            verifier = new SetVerifier((SVGOMSetElement) animationElement, attributeLocalName, validatingValue);
//        }
        AbstractValidator validator = null;
        if (currentAnimation.getAnimationElement() instanceof SVGOMAnimateElement) {
            validator = new AnimateVerifier((SVGOMAnimateElement) animationElement, validatingValue, attributeLocalName);
        } else if (currentAnimation.getAnimationElement() instanceof SVGOMSetElement) {
            validator = new SetVerifier((SVGOMSetElement) animationElement, validatingValue, attributeLocalName);
        } else if (currentAnimation.getAnimationElement() instanceof SVGOMAnimateTransformElement) {
            validator = new AnimateTransformVerifier((SVGOMAnimateTransformElement) animationElement, validatingValue);
        }

        boolean res = validator.isValid();
        if (res) {
            input.setBackground(validColor);
            return raiseEvent(true, "Hodnota je validni", "Custom");
        } else {
            input.setBackground(invalidColor);
            return raiseEvent(false, validator.getErrorMessage(), validator.getDefaultValidValue());
        }


//        try {
//
//            /*
//            * zkusime zparsovat atribut
//            * u xml atributu je vracen null pokud hodnota neni validni
//            * u css se vrati obecna textova hodnota, kterou je nutne overit pomoc CSSEngine
//            * */
//            AnimatableValue val = null;
//            try {
//                val = currentAnimationEngine.parseAnimatableValue
//                        (animationElement, animationTarget, null, //namespace
//                                attributeLocalName, attributeType == AttributeType.CSS,
//                                validatingValue);
//
//            } catch (BridgeException ex) {
//                System.out.println("Atribut neni typu XML");
//            }
//
////            if (attributeType == AttributeType.XML && val == null) {
////                System.out.println("atribut neni validni");
////                return false;
////            }
//
//            //------------------------------------------------------
//            //CSS nebo AUTO
//            //------------------------------------------------------
//            if (attributeType == AttributeType.Auto || attributeType == AttributeType.CSS) {
//                ExtendedParser cssParser = new Parser();
//                CSSEngine cssEngine = CSSUtilities.getCSSEngine(animationTarget.getElement());
//
//            /*pokud je AttributeType.css (nebo auto a vime ze se jedna o css), tak zkusit jeste overit pomoci CSSEngine*/
////                Value cssValue = null;
////                boolean test = val instanceof AnimatablePaintValue ? ((AnimatablePaintValue) val).getPaintType() == 2 : val instanceof AnimatableColorValue;
////                if (attributeType == AttributeType.CSS) {
////                    CSSStylableElement stylableElement = (CSSStylableElement) animationTarget.getElement();
////                    cssValue = CSSUtilities.getCSSEngine(animationTarget.getElement()).parsePropertyValue(null, atrName, validatingValue);
////                    if (cssValue.getStringValue().equals("none")) return false;
////                }
//
//                LexicalUnit lu = cssParser.parsePropertyValue(validatingValue);
//                ValueManager[] valueManagers = cssEngine.getValueManagers();
//                int idx = cssEngine.getPropertyIndex(attributeLocalName); // nazev property
//                if (idx == -1) System.out.println("error");
//                ValueManager vm = valueManagers[idx];
//
//                //pokud neni css atribut validni tak nastane vyjimka
//                try {
//                    Value val2 = vm.createValue(lu, cssEngine);
//                } catch (Exception ex) {
//                    input.setBackground(invalidColor);
//                    return raiseEvent(false, "CSS hodnota neni validni", vm.getDefaultValue().toString());
//                }
//            }
//
//            if (val == null) {
//                //System.out.println("hodnota neni validni");
//                AnimatableValue expecting = currentAnimationEngine.parseAnimatableValue
//                        (animationElement, animationTarget, null, //namespace
//                                attributeLocalName, attributeType == AttributeType.CSS,
//                                animationTarget.getElement().getAttribute(attributeLocalName));
//                if (expecting != null) {
//                    input.setBackground(invalidColor);
//                    return raiseEvent(false, "Hodnota neni validni", expecting.toStringRep());
//                }
//                input.setBackground(invalidColor);
//                return raiseEvent(false, "Hodnota neni validni", "Neznama chyba");
//
//
//            }
//            input.setBackground(validColor);
//            return raiseEvent(val != null, "", "");
//        } catch (Exception ex) {
//            input.setBackground(invalidColor);
//            return raiseEvent(false, "Hodnota neni validni", "Neznama chyba");
//        }
    }

    public interface IValidationCompleted {
        void InputValidated(boolean isValid, String errorMsg, String defaultValidValue);
    }

    private boolean raiseEvent(boolean isValid, String errorMsg, String defaultValidValue) {
        if (eventList != null && !eventList.isEmpty())
            eventList.stream().forEach(aR -> aR.InputValidated(isValid, errorMsg, defaultValidValue));

        return isValid;
    }

    public void addChangeListener(IValidationCompleted validationCompleted) {
        if (eventList == null) {
            eventList = new LinkedHashSet<>();
        }
        eventList.add(validationCompleted);
    }

    public void removeChangeListener(IValidationCompleted validationCompleted) {
        if (eventList != null)
            eventList.remove(validationCompleted);
    }
}
