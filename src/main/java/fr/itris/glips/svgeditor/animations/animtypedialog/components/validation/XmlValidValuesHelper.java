package fr.itris.glips.svgeditor.animations.animtypedialog.components.validation;

import fr.itris.glips.svgeditor.resources.ResourcesManager;

/**
 * Created by Radek Frydrysek on 27.01.2016.
 */
public class XmlValidValuesHelper {
    /**
     * init error message
     * static - initialize only once
     * */
    static {
        integerValueError = ResourcesManager.bundle.getString("ErrorIntegerValue");
        numberValueError = ResourcesManager.bundle.getString("ErrorNumberValue");
        lengthValueError = ResourcesManager.bundle.getString("ErrorLengthValue");
        numberListError = ResourcesManager.bundle.getString("ErrorNumberList");
        lengthListError = ResourcesManager.bundle.getString("ErrorLengthList");
        pathDataError = ResourcesManager.bundle.getString("ErrorPathData");
        pointListValueError = ResourcesManager.bundle.getString("ErrorPointListValue");
        preserveAspectRatioValueError = ResourcesManager.bundle.getString("ErrorPreserveAspectRatioValue");
        numberOrPercentageValueError = ResourcesManager.bundle.getString("ErrorNumberOrPercentageValue");
        booleanValueError = ResourcesManager.bundle.getString("ErrorBooleanValue");
        rectValueError = ResourcesManager.bundle.getString("ErrorRectValue");

        integerValueDefaultValue = ResourcesManager.bundle.getString("DefaultValueIntegerValue");
        numberValueDefaultValue = ResourcesManager.bundle.getString("DefaultValueNumberValue");
        lengthValueDefaultValue = ResourcesManager.bundle.getString("DefaultValueLengthValue");
        numberListDefaultValue = ResourcesManager.bundle.getString("DefaultValueNumberList");
        lengthListDefaultValue = ResourcesManager.bundle.getString("DefaultValueLengthList");
        pathDataDefaultValue = ResourcesManager.bundle.getString("DefaultValuePathData");
        pointListValueDefaultValue = ResourcesManager.bundle.getString("DefaultValuePointListValue");
        preserveAspectRatioValueDefaultValue = ResourcesManager.bundle.getString("DefaultValuePreserveAspectRatioValue");
        numberOrPercentageValueDefaultValue = ResourcesManager.bundle.getString("DefaultNumberOrPercentageValue");
        booleanValueDefaultValue = ResourcesManager.bundle.getString("DefaultValueBooleanValue");
        rectValueDefaultValue = ResourcesManager.bundle.getString("DefaultValueRectValue");

        defaultValueMessage = ResourcesManager.bundle.getString("MessageValidValue");
    }

    //error messages
    private static String integerValueError;
    private static String numberValueError;
    private static String lengthValueError;
    private static String numberListError;
    private static String lengthListError;
    private static String pathDataError;
    private static String pointListValueError;
    private static String preserveAspectRatioValueError;
    private static String numberOrPercentageValueError;
    private static String booleanValueError;
    private static String rectValueError;

    //default values
    private static String integerValueDefaultValue;
    private static String numberValueDefaultValue;
    private static String lengthValueDefaultValue;
    private static String numberListDefaultValue;
    private static String lengthListDefaultValue;
    private static String pathDataDefaultValue;
    private static String pointListValueDefaultValue;
    private static String preserveAspectRatioValueDefaultValue;
    private static String numberOrPercentageValueDefaultValue;
    private static String booleanValueDefaultValue;
    private static String rectValueDefaultValue;

    private static String defaultValueMessage;

    /**
     * @param attributeType type of attribute (XML attribute)
     * @return message error corresponding to type of attribute
     */
    public static String getErrorMessage(int attributeType) {
        String message = "";
        switch (attributeType) {
            case 1://SVGAnimationEngine.AnimatableIntegerValueFactory
                message = integerValueError;
                break;
            case 2://SVGAnimationEngine.AnimatableNumberValueFactory
                message = numberValueError;
                break;
            case 3://SVGAnimationEngine.AnimatableLengthValueFactory
                message = lengthValueError;
                break;
            case 13://SVGAnimationEngine.AnimatableNumberListValueFactory
                message = numberListError;
                break;
            case 14://SVGAnimationEngine.AnimatableLengthListValueFactory
                message = lengthListError;
                break;
            case 22://SVGAnimationEngine.AnimatablePathDataFactory
                message = pathDataError;
                break;
            case 31://SVGAnimationEngine.AnimatablePointListValueFactory
                message = pointListValueError;
                break;
            case 32://SVGAnimationEngine.AnimatablePreserveAspectRatioValueFactory
                message = preserveAspectRatioValueError;
                break;
            case 47://SVGAnimationEngine.AnimatableNumberOrPercentageValueFactory
                message = numberOrPercentageValueError;
                break;
            case 49://SVGAnimationEngine.AnimatableBooleanValueFactory
                message = booleanValueError;
                break;
            case 50://SVGAnimationEngine.AnimatableRectValueFactory
                message = rectValueError;
                break;
        }
        return message;
    }

    /**
     * @param attributeType type of attribute (XML attribute)
     * @return default value corresponding to type of attribute
     */
    public static String getDefaultValue(int attributeType) {
        String defaultValue = "";
        switch (attributeType) {
            case 1://SVGAnimationEngine.AnimatableIntegerValueFactory
                defaultValue = integerValueDefaultValue;
                break;
            case 2://SVGAnimationEngine.AnimatableNumberValueFactory
                defaultValue = numberValueDefaultValue;
                break;
            case 3://SVGAnimationEngine.AnimatableLengthValueFactory
                defaultValue = lengthValueDefaultValue;
                break;
            case 13://SVGAnimationEngine.AnimatableNumberListValueFactory
                defaultValue = numberListDefaultValue;
                break;
            case 14://SVGAnimationEngine.AnimatableLengthListValueFactory
                defaultValue = lengthListDefaultValue;
                break;
            case 22://SVGAnimationEngine.AnimatablePathDataFactory
                defaultValue = pathDataDefaultValue;
                break;
            case 31://SVGAnimationEngine.AnimatablePointListValueFactory
                defaultValue = pointListValueDefaultValue;
                break;
            case 32://SVGAnimationEngine.AnimatablePreserveAspectRatioValueFactory
                defaultValue = preserveAspectRatioValueDefaultValue;
                break;
            case 47://SVGAnimationEngine.AnimatableNumberOrPercentageValueFactory
                defaultValue = numberOrPercentageValueDefaultValue;
                break;
            case 49://SVGAnimationEngine.AnimatableBooleanValueFactory
                defaultValue = booleanValueDefaultValue;
                break;
            case 50://SVGAnimationEngine.AnimatableRectValueFactory
                defaultValue = rectValueDefaultValue;
                break;
        }
        if (!defaultValue.trim().isEmpty()) {
            defaultValue = defaultValueMessage + " " + defaultValue;
        }
        return defaultValue;
    }
}
