package fr.itris.glips.svgeditor.animations.animtypedialog.components;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.AttributeType;
import fr.itris.glips.svgeditor.resources.ResourcesManager;
import org.apache.batik.dom.svg.SVGOMSetElement;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

/**
 * Created by Radek Frydrysek on 06.12.2015.
 */
public class FromToSettings extends JPanel implements IComponent {
    private static String titleLbl, fromLbl, toLbl;

    static {
        titleLbl = ResourcesManager.bundle.getString("FromToSettingsTitle");
        fromLbl = ResourcesManager.bundle.getString("FromToSettingsFromLbl");
        toLbl = ResourcesManager.bundle.getString("FromToSettingsToLbl");
    }


    private JTextField from;
    private JTextField to;

    private JLabel fromLabel;
    private JLabel toLabel;
    private JLabel errorMsgLabel;

    private IAnimated currentAnimation;
    private AttributeSettings attributeSettings;
    //private ValidationResultComponent validationResultComponent;

    private FromToVerifier inputVerifier;

    public FromToSettings(IAnimated currentAnimation, AttributeSettings attributeSettings) {
        //this(currentAnimation);
        this.attributeSettings = attributeSettings;

        this.currentAnimation = currentAnimation;

        initComponents();

        if (currentAnimation.getAnimationElement() instanceof SVGOMSetElement) { //without from attr
            from.setEnabled(false);
            fromLabel.setEnabled(false);
        }
        from.setText(String.valueOf(currentAnimation.getFromAttribute()));
        to.setText(String.valueOf(currentAnimation.getToAttribute()));

        compose();
        attributeSettings.addChangeListener(new AttributeSettings.IAttributeSettingsChanged() {
            @Override
            public void TypeChanged(AttributeType attributeType, String attributeName) {
                from.setText("");
                to.setText("");
            }

            @Override
            public void NameChanged(AttributeType attributeType, String attributeName) {
                from.setText("");
                to.setText("");
            }
        });


    }

    public FromToSettings(IAnimated currentAnimation) {
        this.currentAnimation = currentAnimation;

        initComponents();

        if (currentAnimation.getAnimationElement() instanceof SVGOMSetElement) { //without from attr
            from.setEnabled(false);
            fromLabel.setEnabled(false);
        }
        from.setText(String.valueOf(currentAnimation.getFromAttribute()));
        to.setText(String.valueOf(currentAnimation.getToAttribute()));

        compose();
    }

    private void verifyAndUpdateFrom(InputVerifier inputVerifier) {
        if (inputVerifier.verify(from)) {
            if (attributeSettings != null) {
                if (inputVerifier.verify(to)) { //pokud jsou oba validni
                    errorMsgLabel.setText("");
                    //pokud neni stejna jako predchozi
                    if (!from.getText().equals(currentAnimation.getFromAttribute()))
                        currentAnimation.updateAttributeSettings(from.getText(), to.getText(), attributeSettings.getAttributeType(), attributeSettings.getAttributeName());
                }
            } else {

                currentAnimation.updateFromAttribute(from.getText());
            }
        }
    }

    private void verifyAndUpdateTo(InputVerifier inputVerifier) {
        if (inputVerifier.verify(to)) {
            if (attributeSettings != null) {
                if ((currentAnimation.getAnimationElement() instanceof SVGOMSetElement) || inputVerifier.verify(from)) {//pokud jsou oba validni
                    errorMsgLabel.setText("");
                    if (!to.getText().equals(currentAnimation.getToAttribute()))
                        currentAnimation.updateAttributeSettings(from.getText(), to.getText(), attributeSettings.getAttributeType(), attributeSettings.getAttributeName());
                }

            } else {

                currentAnimation.updateToAttribute(to.getText());
            }
        }
    }

    @Override
    public void initComponents() {
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(titleLbl));
        this.setPreferredSize(new Dimension(250, 85));

        fromLabel = new JLabel(fromLbl);
        toLabel = new JLabel(toLbl);
        errorMsgLabel = new JLabel();


        from = new JTextField();
        //from.setInputVerifier(new AttributeVerifier());
        inputVerifier = new FromToVerifier(this, attributeSettings);
//        validationResultComponent = new ValidationResultComponent((FromToVerifier) inputVerifier);
        from.setPreferredSize(new Dimension(100, 25));
        if (!(currentAnimation.getAnimationElement() instanceof SVGOMSetElement)) {
            from.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    verifyAndUpdateFrom(inputVerifier);
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    verifyAndUpdateFrom(inputVerifier);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    verifyAndUpdateFrom(inputVerifier);
                }
            });
        }

        to = new JTextField();
        to.setPreferredSize(new Dimension(100, 25));
        to.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                verifyAndUpdateTo(inputVerifier);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                verifyAndUpdateTo(inputVerifier);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                verifyAndUpdateTo(inputVerifier);
            }
        });
    }

    @Override
    public void compose() {
        //AttributeType
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.EAST;
        cons.weighty = cons.weightx = 0.1;
        cons.insets = new Insets(0, 10, 0, 10);
        add(fromLabel, cons);

        cons.gridx = 1;
        cons.gridy = 0;
        cons.anchor = GridBagConstraints.WEST;
        cons.weighty = cons.weightx = 0.1;
        add(from, cons);

        //AttributeName
        cons.gridx = 0;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.EAST;
        cons.weighty = cons.weightx = 0.1;
        cons.insets = new Insets(0, 10, 0, 10);
        add(toLabel, cons);

        cons.gridx = 1;
        cons.gridy = 1;
        cons.anchor = GridBagConstraints.WEST;
        cons.weighty = cons.weightx = 0.1;
        add(to, cons);

        cons.gridx = 0;
        cons.gridy = 2;
        cons.gridwidth = 2;
        cons.anchor = GridBagConstraints.WEST;
        cons.weighty = cons.weightx = 0.1;
        add(errorMsgLabel, cons);

//        cons.gridx = 0;
//        cons.gridy = 3;
//        cons.gridwidth = 4;
//        cons.anchor = GridBagConstraints.WEST;
//        cons.weighty = cons.weightx = 0.1;
//        add(validationResultComponent, cons);
    }

    @Override
    public IAnimated getCurrentAnimation() {
        return currentAnimation;
    }

    public void updateAnimation(IAnimated animation) {
        //nechat vse inicializovane a pouze upravit hodnoty
        this.currentAnimation = animation;

        if (currentAnimation.getAnimationElement() instanceof SVGOMSetElement) { //without from attr
            from.setEnabled(false);
            fromLabel.setEnabled(false);
        }
        from.setText(String.valueOf(currentAnimation.getFromAttribute()));
        to.setText(String.valueOf(currentAnimation.getToAttribute()));
    }
//    test(
//            ((Element) ((Element) svgCanvas.getSVGDocument().getRootElement().getChildNodes().item(3)).getChildNodes().item(1)), //element animace
//            "x", //attributeName z aniamce
//            svgCanvas.getUpdateManager().getBridgeContext().getAnimationEngine(),
//    (SVGOMRectElement) ((Element) svgCanvas.getSVGDocument().getRootElement().getChildNodes().item(3)), //animovany element
//            "x",//attributeName z aniamce
//    SVGAnimationEngine.ANIM_TYPE_XML
//    );
//
//    private boolean isValid(IAnimated currentAnimation, String atrName, String validatingValue, SVGAnimationEngine currentAnimationEngine, String attributeLocalName, AttributeType attributeType) {
//        Element animationElement = currentAnimation.getAnimationElement();
//        AnimationTarget animationTarget = (AnimationTarget) currentAnimation.getAnimatedElement();
//
//        //pokud jde o animate transform
//        //at org.apache.batik.bridge.SVGAnimateTransformElementBridge.parseValue(SVGAnimateTransformElementBridge.java:124)
//        if(animationElement instanceof SVGOMAnimateTransformElement){
//            CustomAnimateTransform customAnimateTransform = new CustomAnimateTransform((SVGOMAnimateTransformElement) animationElement);
//
//                boolean res = customAnimateTransform.isValid(animationTarget);
//                if(res){
//                    System.out.println("animateTransform je validni");
//                }else{
//                    System.out.println("animateTransform neni validni");
//                }
//            return res;
//
//        }
//
//
//        //pokud element nema atribut
//        if (!animationTarget.getElement().hasAttributeNS(null, atrName)) {
//            //pak zkusim css - style
//            if (attributeType == AttributeType.CSS || attributeType == AttributeType.Auto) {
//                //ma-li style, tak jej zkusime prohledat zda neobsahuje vybrany atribut
//                if (animationTarget.getElement().hasAttributeNS(null, "style")) {
//                    if (!animationTarget.getElement().getAttribute("style").contains(attributeLocalName)) {
//                        System.out.println("atribut " + atrName + " neexistuje");
//                        return false;
//                    }
//                }
//            } else {
//                System.out.println("atribut " + atrName + " neexistuje");
//                return false;
//            }
//        }
//        try {
//
//            /*
//            * zkusime zparsovat atribut
//            * u xml atributu je vracen null pokud hodnota neni validni
//            * u css se vrati obecna textova hodnota, kterou je nutne overit pomoc CSSEngine
//            * */
//            AnimatableValue val = null;
//            try {
//                val = currentAnimationEngine.parseAnimatableValue
//                        (animationElement, animationTarget, null, //namespace
//                                attributeLocalName, attributeType == AttributeType.CSS,
//                                validatingValue);
//
//            } catch (BridgeException ex) {
//                System.out.println("Atribut neni typu XML");
//            }
//
////            if (attributeType == AttributeType.XML && val == null) {
////                System.out.println("atribut neni validni");
////                return false;
////            }
//
//            //------------------------------------------------------
//            //CSS nebo AUTO
//            //------------------------------------------------------
//            if (attributeType == AttributeType.Auto || attributeType == AttributeType.CSS) {
//                ExtendedParser cssParser = new Parser();
//                CSSEngine cssEngine = CSSUtilities.getCSSEngine(animationTarget.getElement());
//
//            /*pokud je AttributeType.css (nebo auto a vime ze se jedna o css), tak zkusit jeste overit pomoci CSSEngine*/
////                Value cssValue = null;
////                boolean test = val instanceof AnimatablePaintValue ? ((AnimatablePaintValue) val).getPaintType() == 2 : val instanceof AnimatableColorValue;
////                if (attributeType == AttributeType.CSS) {
////                    CSSStylableElement stylableElement = (CSSStylableElement) animationTarget.getElement();
////                    cssValue = CSSUtilities.getCSSEngine(animationTarget.getElement()).parsePropertyValue(null, atrName, validatingValue);
////                    if (cssValue.getStringValue().equals("none")) return false;
////                }
//
//                LexicalUnit lu = cssParser.parsePropertyValue(validatingValue);
//                ValueManager[] valueManagers = cssEngine.getValueManagers();
//                int idx = cssEngine.getPropertyIndex(attributeLocalName); // nazev property
//                if (idx == -1) System.out.println("error");
//                ValueManager vm = valueManagers[idx];
//
//                //pokud neni css atribut validni tak nastane vyjimka
//                try {
//                    Value val2 = vm.createValue(lu, cssEngine);
//                } catch (Exception ex) {
//                    //System.out.println("CSS hodnota neni validni, eg.:" + vm.getDefaultValue().toString());
//                    errorMsgLabel.setText("CSS hodnota neni validni, eg.:" + vm.getDefaultValue().toString());
//                    return false;
//                }
//            }
//
//            if (val == null) {
//                //System.out.println("hodnota neni validni");
//                AnimatableValue expecting = currentAnimationEngine.parseAnimatableValue
//                        (animationElement, animationTarget, null, //namespace
//                                attributeLocalName, attributeType == AttributeType.CSS,
//                                animationTarget.getElement().getAttribute(attributeLocalName));
//                if (expecting != null)
//                    errorMsgLabel.setText("Hodnota neni validni, eg.:" + expecting.toStringRep());
//                //System.out.println("Expecting: " + expecting.toStringRep());
//            }
//            return val != null;
//        } catch (Exception ex) {
//            //System.out.println(ex.getLocalizedMessage());
//            //System.out.println("neocekavana chyba pri validaci");
//            errorMsgLabel.setText("Hodnota neni validni");
//            //vypsat default hodnotu
//            return false;
//        }
//    }
//
//    public class AttributeVerifier extends InputVerifier {
//        @Override
//        public boolean verify(JComponent input) {
//            String text = ((JTextField) input).getText();
//            try {
//                SVGAnimationEngine animationEngine = (SVGAnimationEngine) Editor.getEditor().getHandlesManager().getCurrentHandle().getCanvas().getBridgeContext().getAnimationEngine();
//                if (attributeSettings == null) {
//                    return isValid(currentAnimation, currentAnimation.getAttributeName(), text, animationEngine, currentAnimation.getAttributeName(), currentAnimation.getAttributeType());
//
//                } else {
//                    return isValid(currentAnimation, attributeSettings.getAttributeName(), text, animationEngine, attributeSettings.getAttributeName(), attributeSettings.getAttributeType());
//                }
//            } catch (Exception e) {
//                return false;
//            }
//        }
//    }
//    private class CustomAnimateTransform extends SVGAnimateTransformElementBridge {
//
//        public CustomAnimateTransform(SVGOMAnimateTransformElement element){
//            this.element = element;
//        }
//
//        public boolean isValid(AnimationTarget target){
//            //return this.createAnimation(target);
//            AnimatableValue fromTransformVal = null;
//            AnimatableValue toTransformVal = null;
//            AnimatableValue byTransformVal = null;
//            try {
//                short type = this.parseType();
//
//
//                if (super.element.hasAttributeNS((String) null, "from")) {
//                    fromTransformVal = this.parseValue(super.element.getAttributeNS((String) null, "from"), type, target);
//                }
//
//                if (super.element.hasAttributeNS((String) null, "to")) {
//                    toTransformVal = this.parseValue(super.element.getAttributeNS((String) null, "to"), type, target);
//                }
//
//                if (super.element.hasAttributeNS((String) null, "by")) {
//                    byTransformVal = this.parseValue(super.element.getAttributeNS((String) null, "by"), type, target);
//                }
//            }catch (Exception ex){
//                return false;
//            }
//
//            return (fromTransformVal!=null && toTransformVal!=null);
//        }
//
//    }

    @Override
    public FromToVerifier getInputVerifier() {
        return inputVerifier;
    }
}
