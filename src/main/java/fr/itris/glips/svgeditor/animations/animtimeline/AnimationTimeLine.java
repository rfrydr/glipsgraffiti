package fr.itris.glips.svgeditor.animations.animtimeline;

import fr.itris.glips.svgeditor.animations.animtimeline.components.AnimationRuler;
import fr.itris.glips.svgeditor.animations.animtimeline.components.AnimationTimeLineComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Třída časové osy
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public class AnimationTimeLine extends AbstractAnimationTimeLine {
    private static final long serialVersionUID = -4837000437121818293L;

    private AnimationTimeLineComponent timeLineComponent;
    private JPanel animationPanel;
    private JScrollPane scrollableTimeLine;

    public static int animationHeight = 25;
    public static int animationShapeWidth = 150;
    public static final int defaultFrameWidth = 5;

    private JComponent parent;
    private AnimationRuler ruler;

    public AnimationTimeLine(JComponent parent) {
        initComponents();
        this.parent = parent;
        //this.setPreferredSize(new Dimension(200, 500));
    }

    public void update(Graphics g) {
        super.update(g);
        refresh();
        //update minimum size
        this.setPreferredSize(new Dimension(timeLineComponent.getWidth(), timeLineComponent.getHeight() + ruler.getHeight()));
//        this.setSize(this.getPreferredSize());
//        animationPanel.setSize(this.getPreferredSize());
//        scrollableTimeLine.setSize(this.getPreferredSize());
    }

    private void initComponents() {
        GridBagConstraints gridBagConstraints;
        animationPanel = new JPanel();
        timeLineComponent = new AnimationTimeLineComponent(this);
        scrollableTimeLine = new JScrollPane();

        setLayout(new GridBagLayout());

        animationPanel.setMinimumSize(new Dimension(300, 100));
        animationPanel.setPreferredSize(new Dimension(300, 100));
        animationPanel.setLayout(new GridBagLayout());

        ruler = new AnimationRuler(scrollableTimeLine);
        ruler.setBackground(Color.WHITE);
        ruler.setOpaque(true);
        ruler.setMaximumSize(new Dimension(2048, 25));

        JPanel timelineWrapper = new JPanel();
        timelineWrapper.setLayout(new GridBagLayout());
        timelineWrapper.setBackground(new Color(238, 248, 248));

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.ipady = 0;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        timelineWrapper.add(ruler, constraints);

        constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 0;
        constraints.gridy = 1;
//        constraints.weightx = 0.1;
//        constraints.weighty = 0.1;
        constraints.ipady = 0;
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.fill = GridBagConstraints.BOTH;
        timelineWrapper.add(timeLineComponent, constraints);

        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = constraints.weightx = 1.0;
        timelineWrapper.add(new JPanel(), constraints);

        scrollableTimeLine.setViewportView(timelineWrapper);

        GridBagConstraints cons = new GridBagConstraints();
        cons.fill = GridBagConstraints.BOTH;
        cons.weightx = 1.0;
        cons.weighty = 1.0;
        animationPanel.add(scrollableTimeLine, cons);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(animationPanel, gridBagConstraints);
    }

    @Override
    public void refresh() {
        if (timeLineComponent != null) {
            timeLineComponent.repaint();
            Dimension timelineDim = timeLineComponent.getSize();
            this.setPreferredSize(new Dimension(((int) timelineDim.getWidth()) + 12, (int) timelineDim.getHeight() + ruler.getHeight() + 12));
        }
    }

    public JComponent getParent() {
        return parent;
    }

    @Override
    public void setZoom(int zoom) {
        ruler.setZoomLevel(zoom);
        timeLineComponent.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
