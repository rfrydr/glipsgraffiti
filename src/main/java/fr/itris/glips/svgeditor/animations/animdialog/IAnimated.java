package fr.itris.glips.svgeditor.animations.animdialog;

import fr.itris.glips.svgeditor.animations.animtypedialog.AnimateMotionRotate;
import fr.itris.glips.svgeditor.animations.animtypedialog.AttributeType;
import fr.itris.glips.svgeditor.animations.animtypedialog.RepeatType;
import fr.itris.glips.svgeditor.animations.animtypedialog.TransformType;
import org.apache.batik.anim.timing.TimedElement;
import org.apache.batik.dom.svg.SVGOMAnimationElement;
import org.w3c.dom.Element;

/**
 * Created by Radek Frydrysek on 4.5.2015.
 */
public interface IAnimated {
    Element getAnimatedElement();

    SVGOMAnimationElement getAnimationElement();

    double getDuration();

    void updateDuration(double duration);

    double getDurationOffset();

    double getTotalTime();

    void updateDurationOffset(double durationOffset);

    String getAnimationLabel();

    TimedElement getTimedElement();

    int getRepeatCount();

    void updateRepeatCount(final int repeatCount);

    RepeatType getRepeatType();

    void updateRepeatType(final RepeatType repeatType);

    double getRepeatDur();

    void updateRepeatDur(final double repeatDur);

    RepeatType getRepeatDurType();

    void updateRepeatDurType(final RepeatType repeatDurType);

    AttributeType getAttributeType();

    void updateAttributeType(final AttributeType attributeType);

    String getAttributeName();

    void updateAttributeName(final String attributeName);

    String getFromAttribute();

    void updateFromAttribute(final String from);

    String getToAttribute();

    void updateToAttribute(final String to);

    TransformType getTransformType();

    void removeFromToAttributes();

    void insertFromToAttributes(final String fromVal, final String toVal);

    void updateAttributeSettings(final String fromVal, final String toVal, final AttributeType attributeType, final String attributeName);

    String getAnimateMotionPath();

    AnimateMotionRotate getAnimateMotionRotate();

    void updateAnimateMotionRotate(AnimateMotionRotate rotateValue);

    void updateAnimateMotionPath(String pathValue);

    String getPredefinedLabel();

    void updateTransformType(TransformType transformType);
}
