package fr.itris.glips.svgeditor.animations.animtimeline;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Třída reprezentující časovou osu animací
 *
 * @author Zdeněk Gold, Radek Frydryšek
 */
public abstract class AbstractAnimationTimeLine extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1554888122167L;

    protected AbstractAnimationTimeLine() {
    }

    /**
     * Nastavení zoomu časové osy
     *
     * @param zoom
     */
    public abstract void setZoom(int zoom);

    /**
     * Metoda pro překreslení komponenty podle aktuálního stavu
     */
    public abstract void refresh();

    /**
     * Získání nadřazeného prvku komponenty
     *
     * @return
     */
    public abstract JComponent getParent();
}

