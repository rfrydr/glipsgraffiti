package fr.itris.glips.svgeditor.animations.animtypedialog.predefined;

import fr.itris.glips.svgeditor.animations.animdialog.IAnimated;
import fr.itris.glips.svgeditor.animations.animtypedialog.*;
import fr.itris.glips.svgeditor.display.selection.Selection;
import org.apache.batik.dom.svg.*;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGAnimationElement;

import javax.swing.*;

/**
 * Created by Radek Frydrysek on 03.10.2015.
 */
public class DialogFactory {
    public static AbstractAnimationDialog getDialog(JMenuItem selectedItem, Selection selection) {
        //TODO pouzit hodnoty z prekladu
        String selectedText = selectedItem.getText();
        Element selectedElement = null;
        if (!selection.getSelectedElements().isEmpty())
            selectedElement = selection.getSelectedElements().iterator().next();

        AbstractAnimationDialog dialog = null;
        if (selectedElement != null) {
            switch (selectedText) {
                case "Fade in/out":
                    dialog = new FadeInOut(selectedElement);
                    break;
                case "Change of scale":
                    dialog = new ChangeOfScale(selectedElement);
                    break;
                case "Rotate":
                    dialog = new RotateObject(selectedElement);
                    break;
                case "Skew":
                    dialog = new Skew(selectedElement);
                    break;
                case "Set":
                    dialog = new SetDialog(selectedElement);
                    break;
                case "Animate":
                    dialog = new AnimateDialog(selectedElement);
                    break;
                case "AnimateColor":
                    dialog = new AnimateDialog(selectedElement);
                    break;
                case "AnimateTransform":
                    dialog = new AnimateTransformDialog(selectedElement);
                    break;
                case "AnimateMotion":
                    dialog = new AnimateMotionDialog(selectedElement);
                    break;
            }
        }
        return dialog;
    }

    public static AbstractAnimationDialog getDialog(IAnimated currentAnimationObject) {
        if (currentAnimationObject == null) {
            return null;
        }
        //predefined
        String predefinedAnim = currentAnimationObject.getAnimationElement().getAttribute("predefined");
        if (predefinedAnim.trim().isEmpty()) {
            SVGAnimationElement animationElement = currentAnimationObject.getAnimationElement();
            if (animationElement.getClass().equals(SVGOMAnimateElement.class) || animationElement.getClass().equals(SVGOMAnimateColorElement.class)) {
                return new AnimateDialog(currentAnimationObject);
            } else if (animationElement.getClass().equals(SVGOMAnimateMotionElement.class)) {
                return new AnimateMotionDialog(currentAnimationObject);
            } else if (animationElement.getClass().equals(SVGOMAnimateTransformElement.class)) {
                if (currentAnimationObject.getTransformType() == TransformType.rotate) {
                    return new RotateObject(currentAnimationObject);
                } else if (currentAnimationObject.getTransformType() == TransformType.scale) {
                    return new ChangeOfScale(currentAnimationObject);
                } else if (currentAnimationObject.getTransformType() == TransformType.skewX || currentAnimationObject.getTransformType() == TransformType.skewY) {
                    return new Skew(currentAnimationObject);
                } else {
                    return new AnimateTransformDialog(currentAnimationObject);
                }
            } else if (animationElement.getClass().equals(SVGOMSetElement.class)) {
                return new SetDialog(currentAnimationObject);
            }
        } else {
            switch (predefinedAnim) {
                case "FadeInOut":
                    return new FadeInOut(currentAnimationObject);
                case "ChangeOfScale":
                    return new ChangeOfScale(currentAnimationObject);
                case "Skew":
                    return new Skew(currentAnimationObject);
                case "Rotation":
                    return new RotateObject(currentAnimationObject);
            }
        }
        return null;
    }
}
