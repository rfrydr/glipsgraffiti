package fr.itris.glips.rtdaeditor.test.display;

import javax.swing.*;

/**
 * Created by Radek Frydrysek on 7.4.2015.
 */
public interface DialogTest {
    void refreshDialogState(boolean visible);

    void refreshSimulationValuesPanel();

    JFrame getFrame();
}
