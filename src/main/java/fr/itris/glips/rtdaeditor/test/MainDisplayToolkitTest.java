package fr.itris.glips.rtdaeditor.test;

import java.awt.*;
import fr.itris.glips.rtda.*;
import fr.itris.glips.rtda.components.picture.*;
import fr.itris.glips.rtdaeditor.test.display.*;

/**
 * the class of the toolkit for the main display test
 * @author ITRIS, Jordi SUC
 */
public class MainDisplayToolkitTest extends MainDisplayToolkit{

	/**
	 * the dialog test
	 */
	private DialogTest dialogTestImpl;
	
	/**
	 * the constructor of the class
	 * @param dialogTestImpl the dialog for the dialog
	 */
	public MainDisplayToolkitTest(DialogTest dialogTestImpl) {
		
		this.dialogTestImpl = dialogTestImpl;
	}
	
	@Override
	public void quitProgram() {

		dialogTestImpl.refreshDialogState(false);
	}

	@Override
	public void refresh(SVGPicture currentPicture) {

		dialogTestImpl.refreshSimulationValuesPanel();
	}

	@Override
	public Frame getTopLevelFrame() {

		return dialogTestImpl.getFrame();
	}

	/**
	 * @return the dialogTest
	 */
	public DialogTest getDialogTestImpl() {
		return dialogTestImpl;
	}
}
